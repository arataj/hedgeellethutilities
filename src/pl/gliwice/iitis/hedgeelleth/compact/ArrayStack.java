/*
 * ArrayStack.java
 *
 * Created on Jan 16, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compact;

import java.util.*;

/**
 * An array--based stack for efficient storage of large quantities
 * of byte arrays.
 * 
 * @author Artur Rataj
 */
public class ArrayStack implements Collection<byte[]> {
    final static boolean VERBOSE = false;
    /**
     * Size of a single element array, stored in this queue.
     */
    final int ARRAY_SIZE;
    /**
     * Maximum number of element arrays stored in a single chunk,
     * determines the chunk's size.
     */
    final int CHUNK_SIZE;
    /**
     * Chunks, into which the storage space is divided. The order
     * is from head to tail. Elements are pushed to the tail, popped from
     * the tail.
     */
    LinkedList<byte[]> chunks;
    /**
     * To avoid instantation overhead, the latest removed chunk is
     * cached in this field. Null if none or already reused.
     */
    byte[] cachedRemovedChunk = null;
    /**
     * <p>Index of the next element to push, that is, to
     * append to the tail chunk, or <code>CHUNK_SIZE</code>, if a
     * new chunk would need to be added in order to add the next
     * element.</p>
     * 
     * <p>Also, one entry past the element to pop</p>
     */
    int index;
    
    /**
     * Creates a new instance of the stack.
     * 
     * @param arraySize size of an element array, in bytes
     * @param chunkSize maximum number of arrays stored in a single
     * chunk, determines the chunk's size, -1 for the default, defined
     * by <code>ArrayCompact.getDefaultChunkSize(arraySize, 64)</code>.
     */
    public ArrayStack(int arraySize, int chunkSize) {
        ARRAY_SIZE = arraySize;
        if(chunkSize == -1)
            chunkSize = ArrayCompact.getDefaultChunkSize(ARRAY_SIZE, 64);
        CHUNK_SIZE = chunkSize;
        cachedRemovedChunk = null;
        clear();
    }
    /**
     * Appends an empty tail chunk. Resets <code>index</code>.
     */
    protected void appendChunk() {
        if(cachedRemovedChunk != null) {
            chunks.add(cachedRemovedChunk);
            cachedRemovedChunk = null;
        } else
            chunks.add(new byte[ARRAY_SIZE*CHUNK_SIZE]);
        if(VERBOSE)
            System.out.println("stack: bulk direct: chunks = " + chunks.size() + ", total memory = " +
                    (int)Math.round((1.0*chunks.size()*ARRAY_SIZE*CHUNK_SIZE)/(1024*1024)) + "m");
        index = 0;
    }
    /**
     * Removes an empty tail chunk. Resets <code>index</code>.
     */
    protected void removeChunk() {
        cachedRemovedChunk = chunks.removeLast();
        if(VERBOSE)
            System.out.println("stack: bulk direct: chunks = " + chunks.size() + ", total memory = " +
                    (int)Math.round((1.0*chunks.size()*ARRAY_SIZE*CHUNK_SIZE)/(1024*1024)) + "m");
        index = CHUNK_SIZE;
    }
    /**
     * Push an element to this stack.
     * 
     * @param element array of the size <code>ARRAY_SIZE</code>
     * @return if modified; always true
     */
    @Override
    public boolean add(byte[] element) {
//System.out.print("stack: size=" + size() + ", adding");
        if(element.length != ARRAY_SIZE)
            throw new RuntimeException("invalid element size");
        if(index == CHUNK_SIZE)
            appendChunk();
        byte[] chunk = chunks.getLast();
        System.arraycopy(element, 0, chunk, index*ARRAY_SIZE,
                ARRAY_SIZE);
        ++index;
//System.out.println(", new size=" + size());
        return true;
    }
    /**
     * Pops an element from this stack.
     * 
     * @param element array of the size <code>ARRAY_SIZE</code>,
     * to be filled by this method if the queue is not empty
     * @return reference to <code>element</code>
     */
    public byte[] poll(byte[] element) {
//System.out.print("stack: size=" + size() + ", popping");
        if(element.length != ARRAY_SIZE)
            throw new RuntimeException("invalid element size");
        if(isEmpty())
            throw new RuntimeException("stack is already empty");
        byte[] chunk = chunks.getLast();
        --index;
        System.arraycopy(chunk, index*ARRAY_SIZE, element, 0, 
                ARRAY_SIZE);
        if(index == 0)
            // the current tail chunk is no more needed
            removeChunk();
//System.out.println(", new size=" + size());
        return element;
    }
    @Override
    public int size() {
        int n = chunks.size();
        if(n == 0)
            return 0;
        else
            return (n - 1)*CHUNK_SIZE + index;
    }
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public Iterator<byte[]> iterator() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean addAll(Collection<? extends byte[]> c) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public void clear() {
        chunks = new LinkedList<>();
        index = CHUNK_SIZE;
    }
}
