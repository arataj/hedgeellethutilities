/*
 * ByteArray.java
 *
 * Created on Jan 24, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compact;

import java.util.Arrays;

/**
 * Based on Jon Skeet's example at
 * http://stackoverflow.com/questions/1058149/using-a-byte-array-as-hashmap-key-java
 */
public final class ByteArray
{
    public final byte[] data;

    public ByteArray(int size)
    {
        data = new byte[size];
    }
    @Override
    public boolean equals(Object other)
    {
        if (!(other instanceof ByteArray))
        {
            return false;
        }
        return Arrays.equals(data, ((ByteArray)other).data);
    }
    @Override
    public int hashCode()
    {
        return Arrays.hashCode(data);
    }
}