/*
 * ArrayFIFO.java
 *
 * Created on Jan 16, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compact;

import java.util.*;

/**
 * An array--based fifo queue for efficient storage of large quantities
 * of byte arrays.
 * 
 * @author Artur Rataj
 */
public class ArrayFIFO implements Collection<byte[]> {
    final static boolean VERBOSE = false;
    /**
     * Size of a single element array, stored in this queue.
     */
    final int ARRAY_SIZE;
    /**
     * Maximum number of element arrays stored in a single chunk,
     * determines the chunk's size.
     */
    final int CHUNK_SIZE;
    /**
     * Chunks, into which the storage space is divided. The order
     * is from head to tail. Elements are added to the tail, polled from
     * the head.
     */
    LinkedList<byte[]> chunks;
    /**
     * Index of the next element to poll from the head chunk, or -1
     * for an empty queue.
     */
    int headIndex;
    /**
     * <p>Index of the next element to append to the tail chunk,
     * or <code>CHUNK_SIZE</code>, if a new chunk would need to
     * be added in order to add the next element.</p>
     * 
     * <p>For a single chunk, this value must always be greater than
     * or equal to <code>headIndex</code>.</p>
     */
    int tailIndex;
    
    /**
     * Creates a new instance of the FIFO queue.
     * 
     * @param arraySize size of an element array, in bytes
     * @param chunkSize maximum number of arrays stored in a single
     * chunk, determines the chunk's size, -1 for the default, defined
     * by <code>ArrayCompact.getDefaultChunkSize(arraySize, 64)</code>.
     */
    public ArrayFIFO(int arraySize, int chunkSize) {
        ARRAY_SIZE = arraySize;
        if(chunkSize == -1)
            chunkSize = ArrayCompact.getDefaultChunkSize(ARRAY_SIZE, 64);
        CHUNK_SIZE = chunkSize;
        clear();
    }
    /**
     * Appends an empty tail chunk. Resets <code>tailIndex</code>
     * and, if the queue is currently empty, <code>headIndex</code>.
     */
    protected void appendChunk() {
        chunks.add(new byte[ARRAY_SIZE*CHUNK_SIZE]);
        if(VERBOSE)
            System.out.println("fifo: bulk direct: chunks = " + chunks.size() + ", total memory = " +
                    (int)Math.round((1.0*chunks.size()*ARRAY_SIZE*CHUNK_SIZE)/(1024*1024)) + "m");
        tailIndex = 0;
        if(headIndex == -1) {
            if(chunks.size() != 1)
                throw new RuntimeException("unexpected number of chunks");
            headIndex = 0;
        }
    }
    /**
     * Removes a used head chunk. Resets <code>headIndex</code>.
     */
    protected void removeChunk() {
        chunks.removeFirst();
        if(VERBOSE)
            System.out.println("fifo: bulk direct: chunks = " + chunks.size() + ", total memory = " +
                    (int)Math.round((1.0*chunks.size()*ARRAY_SIZE*CHUNK_SIZE)/(1024*1024)) + "m");
        if(chunks.isEmpty()) {
            headIndex = -1;
            if(tailIndex != CHUNK_SIZE)
                throw new RuntimeException("unexpected value of tail index");
        } else
            headIndex = 0;
    }
    /**
     * Adds an element to this queue.
     * 
     * @param element array of the size <code>ARRAY_SIZE</code>
     * @return if modified; always true
     */
    @Override
    public boolean add(byte[] element) {
// System.out.print("fifo: size=" + size() + ", adding");
        if(element.length != ARRAY_SIZE)
            throw new RuntimeException("invalid element size");
        if(tailIndex == CHUNK_SIZE)
            appendChunk();
        byte[] chunk = chunks.getLast();
        System.arraycopy(element, 0, chunk, tailIndex*ARRAY_SIZE,
                ARRAY_SIZE);
        ++tailIndex;
// System.out.println(", new size=" + size());
        return true;
    }
    /**
     * Polls an element from the head of this queue.
     * 
     * @param element array of the size <code>ARRAY_SIZE</code>,
     * to be filled by this method if the queue is not empty
     * @return reference to <code>element</code>
     */
    public byte[] poll(byte[] element) {
// System.out.print("fifo: size=" + size() + ", polling");
        if(element.length != ARRAY_SIZE)
            throw new RuntimeException("invalid element size");
        if(isEmpty())
            throw new RuntimeException("queue is already empty");
        byte[] chunk = chunks.getFirst();
        System.arraycopy(chunk, headIndex*ARRAY_SIZE, element, 0, 
                ARRAY_SIZE);
        if(++headIndex == CHUNK_SIZE)
            // the current head chunk is no more needed
            removeChunk();
// System.out.println(", new size=" + size());
        return element;
    }
    @Override
    public int size() {
        int n = chunks.size();
        if(n == 0)
            return 0;
        else if(n == 1)
            return tailIndex - headIndex;
        else {
            int sum;
            if(n > 2)
                sum = (n - 2)*CHUNK_SIZE;
            else
                sum = 0;
            sum += CHUNK_SIZE - headIndex;
            sum += tailIndex;
            return sum;
        }
    }
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public Iterator<byte[]> iterator() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean addAll(Collection<? extends byte[]> c) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public void clear() {
        chunks = new LinkedList<>();
        headIndex = -1;
        tailIndex = CHUNK_SIZE;
    }
}
