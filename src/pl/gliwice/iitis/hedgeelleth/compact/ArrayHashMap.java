/*
 * ArrayHashMap.java
 *
 * Created on Jan 16, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compact;

import java.util.*;

/**
 * <p>A hashmap for efficient storage of large quantities
 * of byte arrays. A byte array is divided into the key/value parts.
 * Removal of entries is possible, yet the contraction of allocated
 * memory can be only partial due to fragmentation of data. Only
 * if all elements are removed it is guaranteed, that all chunks
 * are freed.</p>
 * 
 * <p>Deeply copies any source key or value.</code>
 * 
 * <p>Provided that there are at least millions of entries, up to about
 * 50% -- 60% of the allocated memory was effectively used
 * in tests, where key was 10 bytes, value 4 bytes.</p>
 * 
 * @author Artur Rataj
 */
public class ArrayHashMap {
    /**
     * Verbosity level, 0, 1, or 2.
     */
    static int VERBOSE = 0;
    /**
     * Size of a single array, stored in this map.
     */
    final int ARRAY_SIZE;
    /**
     * Number of bytes within <code>ARRAY_SIZE</code>,
     * which is the key part; equal to
     * <code>ARRAY_SIZE - VALUE_SIZE</code>
     */
    final int KEY_SIZE;
    /**
     * Number of bytes within <code>ARRAY_SIZE</code>,
     * which is the value part; the rest is the key part.
     * Equal to <code>ARRAY_SIZE - KEY_SIZE</code>
     */
    final int VALUE_SIZE;
    /**
     * Maximum number of arrays stored in a single chunk, determines
     * the chunk's size.
     */
    final int CHUNK_SIZE;
    /**
     * If two neigboring chunks have at least that value of entries, loosening
     * becomes possible.
     */
    final int LOOSE_ENTRY_NUM;
    /**
     * If putting of a value, whose key is already present in the map, is
     * allowed.
     */
    final boolean ALLOW_DUPLICATE_PUT;
    /**
     * <p>If false, <code>ArrayHashMap</code> is used as a cache of this
     * <code>ArrayHashMap</code>. If true, <code>HashMap</code>
     * is used as that cache. The latter uses less space in general, but is slower.</p>
     * 
     * If the cache is not enabled, this field is meaningless.
     */
    static boolean COMPACT_CACHE;
    
    /**
     * A separate storage place for a zero key.
     */
    byte[] zero;
    /**
     * If a value is stored in <code>zero</code>.
     */
    boolean zeroFilled;
    /**
     * Chunks, into which the storage space is divided.
     */
    List<byte[]> chunks;
    /**
     * Number of entries in each chunk.
     */
    List<int[]> chunkEntries;
    /**
     * Number of conflicts + empty entries, indexed with the
     * cell index.
     */
    short[] sizes;
    /**
     * Entry computation/testing class.
     */
    Finder finder;
    /**
     * If not null, plain a cache that wraps a plain <code>HashMap</code>.
     */
    Cache cache;
    /**
     * If not null, an array cache.
     */
    ArrayCache arrayCache;
    long statShiftSum = 0;
    long statShiftCount = 0;
    long statCacheAccess = 0;
    long statCacheHits = 0;
    long statCacheAccessLocal = 0;
    long statCacheHitsLocal = 0;
    
    /**
     * Computes subsequent cells to access, depending on
     * an array's key part hash code.
     */
    class Finder {
        /**
         * Hash code of a key.
         */
        int hash;
        /**
         * If the key is the special zero key, stored separately.
         */
        public boolean zeroKey;
        /**
         * If a matching entry has been found in the latest
         * <code>compare()</code>. If a free or conflicting
         * entry has been found, then this field is false.
         */
        boolean matching;
        /**
         * Chunk, where <code>cell</code> is located. Meaningless if
         * <code>zeroKey</code> is true. Null if no chunk exist yet.
         */
        public byte[] chunk;
        /**
         * Index of <code>chunk</code> within <code>chunks</code>.
         */
        int chunkIndex;
        /**
         * Value of <code>cell</code> for the first test. Meaningless if
         * <code>zeroKey</code> is true. Indexes <code>sizes</code>.
         * Set only by <code>reset()</code>.
         */
        public int baseCell;
        /**
         * Index of a cell within a given chunk. Meaningless if
         * <code>zeroKey</code> is true.
         */
        public int cell;
        /**
         * Number of entries, conflicting with the current hash value.
         */
        public int size;

        public Finder() {
        }
        /**
         * Resets this class according to a given buffer. After this
         * call, next() computes subsequent cells.
         * 
         * @param array array, whose key part's hash code is to be computed,
         * or the key part itself
         */
        public void reset(byte[] array) {
            zeroKey = true;
            for(int i = 0; i < KEY_SIZE; ++i) {
                if(array[i] != 0)
                    zeroKey = false;
            }
            if(array.length != KEY_SIZE)
                throw new RuntimeException("not implemented");
            hash = MurMur2.hash(array, 12345678);
            if(chunks.isEmpty())
                chunk = null;
            else
                chunk = chunks.get(0);
            cell = baseCell = Math.abs(hash)%CHUNK_SIZE;
            size = sizes[baseCell];
        }
        /**
         * Computes the initial (if <code>test</code> is 0) or the subsequent
         * chunk / cell combination, unless <code>zeroKey</code>
         * is true. In the latter case, this method can not be called, as
         * in that special case tests are not needed.
         * 
         * @param test test number; zero for the first chunk / cell combination
         * to be tested; if equal to <code>size</code>, a new chunk is created
         * if needed
         */
        public void find(int test) {
            if(zeroKey) {
                throw new RuntimeException("zero key need no tests");
            } else {
                cell = baseCell;
                chunkIndex = test;
                if(chunkIndex == chunks.size()) {
                    chunks.add(new byte[CHUNK_SIZE*ARRAY_SIZE]);
                    chunkEntries.add(new int[1]);
                    if(VERBOSE >= 1)
                        System.out.println("map: adding chunk " + chunkIndex +
                                "\n" + getStats());
                }
                chunk = chunks.get(chunkIndex);
            }
        }
        /**
         * Compares a key with the combination selected by the latest
         * <code>find()</code>. Computes <code>matching</code>.
         * 
         * @param array array, whose key part to be compared, or alternatively
         * the key part alone; the key part should be the same as one passed
         * to the latest <code>reset()</code>
         * @return 0 for match or a free cell; -1 for <code>array</code> smaller;
         * 1 for <code>array</code> greater
         */
        public int compare(byte[] array) {
            int diff = 0;
            boolean free = true;
            int base = cell*ARRAY_SIZE;
            for(int i = 0; i < KEY_SIZE; ++i) {
                byte a = array[i];
                byte e = chunk[base + i];
                if(e != 0)
                    free = false;
                if(diff == 0) {
                    if(a < e)
                        diff = -1;
                    else if(a > e)
                        diff = 1;
                }
            }
            if(free) {
                matching = false;
                return 0;
            } else {
                matching = diff == 0;
                return diff;
            }
        }
    }
    /**
     * <p>An optional <code>HashMap</code> cache in front of
     * <code>ArrayHashMap</code>. This cache never allows
     * duplicates, so <code>ALLOW_DUPLICATE_PUT</code> must
     * be false if this cache is used.</p>
     * 
     * <p>Deeply copies any source key or value.</code>
     */
    class Cache {
        final int MAX_SIZE;
        ByteArray[] keyPool;
        ByteArray[] valuePool;
        ByteArray keyWrapper;
        int poolPos;
        Map<ByteArray, ByteArray> entries;
        
        public Cache(int maxSize) {
            MAX_SIZE = maxSize;
            keyPool = new ByteArray[MAX_SIZE];
            valuePool = new ByteArray[MAX_SIZE];
            for(int i = 0; i < MAX_SIZE; ++i) {
                keyPool[i] = new ByteArray(KEY_SIZE);
                valuePool[i] = new ByteArray(VALUE_SIZE);
            }
            entries = new HashMap<>();
            poolPos = 0;
            keyWrapper = new ByteArray(KEY_SIZE);
        }
        public void put(byte[] key, byte[] value) {
            ByteArray keyBuffer = keyPool[poolPos];
            ByteArray valueBuffer = valuePool[poolPos];
            poolPos = (poolPos + 1)%MAX_SIZE;
            if(entries.size() == MAX_SIZE)
                if(entries.remove(keyBuffer) == null)
                    throw new RuntimeException("unexpected missing entry");
            System.arraycopy(key, 0, keyBuffer.data, 0, KEY_SIZE);
            System.arraycopy(value, 0, valueBuffer.data, 0, VALUE_SIZE);
            entries.put(keyBuffer, valueBuffer);
        }
        public boolean get(byte[] key, byte[] value) {
            System.arraycopy(key, 0, keyWrapper.data, 0, KEY_SIZE);
            if(value == null)
                return entries.containsKey(keyWrapper);
            else {
                ByteArray valueBuffer = entries.get(keyWrapper);
                if(valueBuffer != null) {
                    System.arraycopy(valueBuffer.data, 0, value, 0, VALUE_SIZE);
                    return true;
                } else
                    return false;
            }
        }
    };
    /**
     * An optional <code>ArrayHashMap</code> cache in front of
     * <code>ArrayHashMap</code>. This cache never allows
     * duplicates, so <code>ALLOW_DUPLICATE_PUT</code> must
     * be false if this cache is used.
     * 
     * <p>Deeply copies any source key or value.</code>
     */
    class ArrayCache {
        final int MAX_SIZE;
        byte[][] keyPool;
        byte[][] valuePool;
        byte[] keyWrapper;
        byte[] valueWrapper;
        int poolPos;
        ArrayHashMap entries;
        int entriesNum;
        
        public ArrayCache(int maxSize) {
            MAX_SIZE = maxSize;
            keyPool = new byte[MAX_SIZE][KEY_SIZE];
            valuePool = new byte[MAX_SIZE][VALUE_SIZE];
            entries = new ArrayHashMap(KEY_SIZE, VALUE_SIZE,
                    Math.max(1024, maxSize*2), false, 0,
                    COMPACT_CACHE);
            entriesNum = 0;
            poolPos = 0;
            keyWrapper = new byte[KEY_SIZE];
            valueWrapper = new byte[VALUE_SIZE];
        }
        public void put(byte[] key, byte[] value) {
            byte[] keyBuffer = keyPool[poolPos];
            byte[] valueBuffer = valuePool[poolPos];
            poolPos = (poolPos + 1)%MAX_SIZE;
            if(entriesNum == MAX_SIZE) {
                if(!entries.remove(keyBuffer)) {
// entries.remove(keyBuffer);
                    throw new RuntimeException("unexpected missing entry");
                }
                --entriesNum;
            }
            System.arraycopy(key, 0, keyBuffer, 0, KEY_SIZE);
            System.arraycopy(value, 0, valueBuffer, 0, VALUE_SIZE);
            entries.put(keyBuffer, valueBuffer);
            ++entriesNum;
            // System.out.println("entries num = " + entriesNum);
        }
        public boolean get(byte[] key, byte[] value) {
            System.arraycopy(key, 0, keyWrapper, 0, KEY_SIZE);
            if(value == null)
                return entries.get(keyWrapper, null);
            else {
                if(entries.get(keyWrapper, valueWrapper)) {
                    System.arraycopy(valueWrapper, 0, value, 0, VALUE_SIZE);
                    return true;
                } else
                    return false;
            }
        }
    };
    
    /**
     * Creates a new hash map. It stores byte arrays of the size
     * <code>keySize + valueSize</code>. Head part is a key,
     * tail part is a value.
     * 
     * @param keySize number of bytes, which is the key part;
     * the rest is the key part
     * @param valueSize number of bytes, which is the value part;
     * the rest is the key part; <code>valueSize</code> can be 0,
     * this map would act as a set in that case
     * @param chunkSize maximum number of arrays stored in a single
     * chunk, determines the chunk's size, -1 for the default, defined
     * by <code>ArrayCompact.getDefaultChunkSize(keySize + valueSize, 32)</code>.
     * @param allowDuplicatePut if putting of a value, whose key is already
     * present in the map, is allowed
     * @param cacheSize number of entries in a cache; 0 for no cache
     * @param useCompactCache if cache is enabled, then this parameter specifies,
     * if a slower, but taking less memory, <code>HashMap</code>--based cache
     * should be used, as opposed to <code>ArrayHashMap</code>
     */
    public ArrayHashMap(int keySize, int valueSize, int chunkSize,
            boolean allowDuplicatePut, int cacheSize, boolean useCompactCache) {
        KEY_SIZE = keySize;
        VALUE_SIZE = valueSize;
        ARRAY_SIZE = keySize + valueSize;
        if(chunkSize == -1)
            chunkSize = ArrayCompact.getDefaultChunkSize(ARRAY_SIZE, 32);
        // chunkSize = 1003;
        CHUNK_SIZE = chunkSize;
        LOOSE_ENTRY_NUM = 19*CHUNK_SIZE/20;
        chunks = new ArrayList<>();
        chunkEntries = new ArrayList<>();
        zero = new byte[ARRAY_SIZE];
        zeroFilled = false;
        sizes = new short[CHUNK_SIZE];
        finder = new Finder();
        ALLOW_DUPLICATE_PUT = allowDuplicatePut;
        COMPACT_CACHE = useCompactCache;
        if(cacheSize == 0) {
            cache = null;
            arrayCache = null;
        } else {
            if(ALLOW_DUPLICATE_PUT)
                throw new RuntimeException("cache not supported if duplicate put allowed");
            if(COMPACT_CACHE) {
                cache = new Cache(cacheSize);
            } else {
                arrayCache = new ArrayCache(cacheSize);
            }
        }
    }
    /**
     * Adds a key/value pair into this map.
     * 
     * @param key, of the size <code>KEY_SIZE</code>; deeply copied
     * @param value, of the size <code>VALUE_SIZE</code>; deeply
     * copied
     */
    public void put(byte[] key, byte[] value) {
        if(key.length != KEY_SIZE)
            throw new RuntimeException("invalid key size");
        if(value.length != VALUE_SIZE)
            throw new RuntimeException("invalid value size");
        if(cache != null)
            cache.put(key, value);
        else if(arrayCache != null)
            arrayCache.put(key, value);
        finder.reset(key);
/*String before = "[";
for(int i = 0; i < finder.size; ++i) {
    finder.find(i);
    before += ":";
    for(int j = 0; j < KEY_SIZE; ++j)
        before += finder.chunk[ARRAY_SIZE*finder.cell + j] + " ";
}
before += "]";
if(arrayCache == null &&
        key[0] == 12 &&
        key[1] == 0 &&
        key[2] == 3 &&
        key[3] == 0 &&
        key[4] == 1 &&
        key[5] == 14 &&
        key[6] == 0) {
    System.out.println(before);
}*/
        if(finder.zeroKey) {
            // a zero key has a fixed storage in a separate place
            System.arraycopy(key, 0, zero, 0, KEY_SIZE);
            System.arraycopy(value, 0, zero, KEY_SIZE, VALUE_SIZE);
            zeroFilled = true;
        } else {
            int test = finder.size/2;
            int step = Math.max(test/2, 1);
            int prevStep = step;
            int diff;
            int prevDiff = Integer.MAX_VALUE;
            finder.find(test);
            boolean wentFromEmpty = false;
            while((diff = finder.compare(key)) != 0 ||
                    (!finder.matching &&
                        // a free entry not and the tail -- it is not known yet,
                        // what range of values fits here
                        test < finder.size - 1)) {
                if(finder.matching)
                    throw new RuntimeException("unexpected match");
                if(diff < 0)
                    test -= step;
                else if(diff > 0)
                    test += step;
                else if(test < finder.size - 1) {
                    // inertially move further through empty entries
                    if(prevDiff < 0) {
                        if(test > 0)
                            --test;
                        else
                            // bounce back
                            prevDiff = 1;
                    } else
                        ++test;
                    finder.find(test);
                    wentFromEmpty = true;
                    continue;
                } else
                    throw new RuntimeException("unexpected free entry at tail");
                if(test >= finder.size) {
                    if(step == 1) {
                        // new entry larger than any other
                        test = finder.size;
                        break;
                    } else
                        test = finder.size - 1;
                } else if(test < 0) {
                    if(step == 1) {
                        // new entry smaller than any other
                        test = 0;
                        break;
                    } else
                        test = 0;
                }
                if(step == 1 && prevStep == 1 && prevDiff != Integer.MAX_VALUE &&
                        prevDiff*diff == -1) {
                    if(wentFromEmpty) {
                        finder.find(test);
                        if((diff = finder.compare(key)) != 0)
                            throw new RuntimeException("empty entry expected");
                    } else
                        if(diff == -1) {
// finder.find(test);
// if(finder.compare(key) == 0)
//     finder = finder;
                            // point to the larger entry, the previous one is
                            // smaller
                            ++test;
                        }
                    break;
                }
                prevStep = step;
                step = Math.max(step/2 + ((step&3) == 3 ? 1 : 0), 1);
                prevDiff = diff;
                finder.find(test);
                wentFromEmpty = false;
            }
            if(diff != 0)
                finder.find(test);
            byte[] currChunk = finder.chunk;
            int currChunkIndex = finder.chunkIndex;
            int currCell = finder.cell;
            boolean currMatching = finder.matching;
            // where the shift ends
            int shiftTail;
            if(diff != 0 && test < finder.size) {
                // no matching or free at tail entry found, shift is needed
                if(test < finder.size - 1 && chunkEntries.get(test)[0] >= LOOSE_ENTRY_NUM &&
                        chunkEntries.get(test + 1)[0] >= LOOSE_ENTRY_NUM) {
                    // two neighboring chunks full, add some space in between
                    chunks.add(test + 1, new byte[CHUNK_SIZE*ARRAY_SIZE]);
                    chunkEntries.add(test + 1, new int[1]);
                    if(VERBOSE >= 1)
                        System.out.print("map: loosening at " + (test + 1));
                    if(VERBOSE >= 2)
                        System.out.print(" [");
                    else if(VERBOSE == 1)
                        System.out.println();
                    for(int i = 0; i < CHUNK_SIZE; ++i)
                        // the current cell will have its size updated later
                        if(true || i != finder.baseCell) {
                            if(sizes[i] > test + 1) {
                                ++sizes[i];
                                if(VERBOSE >= 2)
                                    System.out.print(" " + sizes[i]);
                            } else {
                                if(VERBOSE >= 2)
                                    System.out.print(" s" + sizes[i]);
                            }
                        } else
                            System.out.print(" c" + sizes[i]);
                    if(VERBOSE >= 2)
                        System.out.println("]\n" + getStats());
                    ++finder.size;
                }
                for(shiftTail = test + 1; shiftTail < finder.size; ++shiftTail) {
                    byte[] c = chunks.get(shiftTail);
                    int base = finder.cell*ARRAY_SIZE;
                    boolean free = true;
                    for(int j = 0; j < KEY_SIZE; ++j)
                        if(c[base + j] != 0) {
                            free = false;
                            break;
                        }
                    if(free) {
                        if(chunkEntries.get(shiftTail)[0] == CHUNK_SIZE)
                            throw new RuntimeException("full chunk with free entry");
                        break;
                    }
                }
                finder.find(shiftTail);
                byte[] targetChunk = finder.chunk;
                int targetCell = finder.cell;
                for(int i = shiftTail - 1; i >= test; --i) {
                    finder.find(i);
                    byte[] sourceChunk = finder.chunk;
                    int sourceCell = finder.cell;
                    System.arraycopy(sourceChunk, sourceCell*ARRAY_SIZE,
                            targetChunk, targetCell*ARRAY_SIZE, ARRAY_SIZE);
                    targetChunk = sourceChunk;
                    targetCell = sourceCell;
                }
                statShiftSum += shiftTail - test;
                ++statShiftCount;
                if(targetChunk != currChunk || targetCell != currCell)
                    throw new RuntimeException("unexpected cell mismatch");
            } else
                // there was not shift
                shiftTail = -1;
            if(currMatching) {
                if(!ALLOW_DUPLICATE_PUT)
                    throw new RuntimeException("duplicate put not allowed");
            } else {
                int base = currCell*ARRAY_SIZE;
                System.arraycopy(key, 0, currChunk, base, KEY_SIZE);
                System.arraycopy(value, 0, currChunk, base + KEY_SIZE, VALUE_SIZE);
                if(shiftTail == finder.size || currChunkIndex == finder.size)
                    ++sizes[finder.baseCell];
                if(shiftTail == -1) {
                    if(chunkEntries.get(currChunkIndex)[0]++ == CHUNK_SIZE)
                        throw new RuntimeException("chunk already full");
                } else {
                    if(chunkEntries.get(shiftTail)[0]++ == CHUNK_SIZE)
                        throw new RuntimeException("chunk already full");
                }
            }
        }
/*String after = "[";
for(int i = 0; i < finder.size; ++i) {
    finder.find(i);
    after += ":";
    boolean lower;
    if(i == 0)
        lower = true;
    else
        lower =false;
    boolean zero = true;
    int vio = -1;
    for(int j = 0; j < KEY_SIZE; ++j) {
        if(finder.chunk[ARRAY_SIZE*finder.cell + j] != 0)
            zero = false;
        if(!lower) {
            if(chunks.get(i - 1)[ARRAY_SIZE*finder.cell + j] <
                    finder.chunk[ARRAY_SIZE*finder.cell + j])
                lower = true;
            else if(chunks.get(i - 1)[ARRAY_SIZE*finder.cell + j] >
                    finder.chunk[ARRAY_SIZE*finder.cell + j]) {
                vio = j;
            }
        }
        after += finder.chunk[ARRAY_SIZE*finder.cell + j] + " ";
    }
    if(!zero && vio != -1) {
                System.out.println("ERROR before = " + before);
                System.out.println("after = " + after);
                i = i;
    }
}
after += "]";*/
    }
    /**
     * Searches a sorted list represented by <code>finder</code>
     * for <code>key</code>.
     * 
     * @param finder finder
     * @param key key
     * @return if found; if yes, <code>finder.chunk</code> and
     * <code>finder.chunkIndex</code> are set resectively
     */
    protected boolean searchByDivide(Finder finder, byte[] key) {
        int test = finder.size/2;
        int step = Math.max(test/2, 1);
        int prevStep = step;
        int diff;
        int prevDiff = Integer.MAX_VALUE;
        if(VERBOSE >= 2) {
            System.out.print("[");
            for(int i = 0; i < finder.size; ++i) {
                finder.find(i);
                System.out.print(":");
                for(int j = 0; j < KEY_SIZE; ++j)
                    System.out.print(finder.chunk[ARRAY_SIZE*finder.cell + j] + " ");
            }
            System.out.println("]");
        }
        finder.find(test);
        while((diff = finder.compare(key)) != 0 || !finder.matching) {
            if(diff < 0)
                test -= step;
            else if(diff > 0)
                test += step;
            else {
                // inertially move further through empty entries
                if(prevDiff < 0) {
                    if(test > 0)
                        --test;
                    else {
                        // bounce back
                        prevDiff = 1;
                    }
                } else {
                    if(test < finder.size - 1)
                        ++test;
                    else {
                        // tested key larger than any other
                        return false;
                    }
                }
                finder.find(test);
                continue;
            }
            if(test >= finder.size) {
                if(step == 1) {
                    // tested key larger than any other
                    return false;
                } else
                    test = finder.size - 1;
            } else if(test < 0) {
                if(step == 1) {
                    // tested key smaller than any other
                    return false;
                } else
                    test = 0;
            }
            if(step == 1 && prevStep == 1 && prevDiff != Integer.MAX_VALUE &&
                    prevDiff*diff == -1) {
                // tested key not found
                return false;
            }
            prevStep = step;
            step = Math.max(step/2 + ((step&3) == 3 ? 1 : 0), 1);
            prevDiff = diff;
            finder.find(test);
        }
        return true;
    }
    /**
     * Looks up a value, given a key.
     * 
     * @param key key, of the size <code>KEY_SIZE</code>
     * @param value preallocated value array, of the size
     * <code>VALUE_SIZE</code>, filled by this method;
     * null if only presence is to be determined
     * @return if the value has been found; if false, <code>value</code>'s
     * contents is unaffected
     */
    public boolean get(byte[] key, byte[] value) {
if(VERBOSE >= 1 && Math.random() < 0.0000001)
    System.out.println(getStats());
        if(key.length != KEY_SIZE)
            throw new RuntimeException("invalid key size");
        if(value != null && value.length != VALUE_SIZE)
            throw new RuntimeException("invalid value size");
        if(statCacheAccessLocal == 1000000) {
            statCacheAccessLocal /= 2;
            statCacheHitsLocal /= 2;
        }
        ++statCacheAccess;
        ++statCacheAccessLocal;
        if(cache != null && cache.get(key, value)) {
            ++statCacheHits;
            ++statCacheHitsLocal;
            return true;
        } else if(arrayCache != null && arrayCache.get(key, value)) {
            ++statCacheHits;
            ++statCacheHitsLocal;
            return true;
        } else {
            boolean found = false;
            finder.reset(key);
            if(finder.zeroKey) {
                // a zero key has a fixed storage in a separate place
                if(zeroFilled) {
                    if(value != null)
                        System.arraycopy(zero, KEY_SIZE, value, 0, VALUE_SIZE);
                    found = true;
                }
            } else if(finder.size != 0) {
                if(!searchByDivide(finder, key))
                    return false;
                if(!finder.matching)
                    throw new RuntimeException("unexpected mismatch");
                found = true;
                if(value != null) {
                    System.arraycopy(finder.chunk, finder.cell*ARRAY_SIZE + KEY_SIZE,
                        value, 0, VALUE_SIZE);
                    if(cache != null)
                        cache.put(key, value);
                    else if(arrayCache != null)
                        arrayCache.put(key, value);
                }
            }
            return found;
        }
    }
    /**
     * Removes an entry. Can be used only if no cache is present.
     * Never contracts the memory used by this array hash map.
     * 
     * @param key key of the entry to remove
     * @return if the entry to remove has been present; if false, this
     * method did nothing
     */
    public boolean remove(byte[] key) {
        if(cache != null || arrayCache != null)
            throw new RuntimeException("can not remove if cache is present");
        finder.reset(key);
        if(finder.zeroKey) {
            // a zero key has a fixed storage in a separate place
            if(zeroFilled) {
                zeroFilled = false;
                return true;
            } else
                return false;
        } else if(finder.size != 0) {
            if(!searchByDivide(finder, key))
                return false;
            if(!finder.matching)
                throw new RuntimeException("unexpected mismatch");
            int base = finder.cell*ARRAY_SIZE;
            for(int i = 0; i < KEY_SIZE; ++i) {
                if(finder.chunk[base + i] != key[i])
                    throw new RuntimeException("wrong key");
                finder.chunk[base + i] = 0;
            }
            if(finder.chunkIndex == sizes[finder.cell] - 1)
                --sizes[finder.cell];
            --chunkEntries.get(finder.chunkIndex)[0];
            // remove new trailing empty entries, if any
            while(sizes[finder.cell] > 0) {
                byte[] c = chunks.get(sizes[finder.cell] - 1);
                boolean zero = true;
                for(int i = 0; i < KEY_SIZE; ++i)
                    if(c[base + i] != 0) {
                        zero = false;
                        break;
                    }
                if(zero)
                    --sizes[finder.cell];
                else
                    break;
            }
            if(finder.chunkIndex == chunks.size() - 1 &&
                    chunkEntries.get(finder.chunkIndex)[0] == 0) {
                // the last chunk is empty, remove it
                //
                // no sizes need to be modified, as an empty entry
                // is not allowed at a lists's tail
                if(VERBOSE >= 1)
                    System.out.println("REMOVAL OF EMPTY TAIL CHUNK\n\t" + getStats());
                chunks.remove(finder.chunkIndex);
                chunkEntries.remove(finder.chunkIndex);
            }
            return true;
        } else
            return false;
    }
    /**
     * Returns the total ratio of cache hits to all cache reads
     * so far.
     * 
     * @return ratio of cache hits, 0.0 ... 1.0; <code>Double.NaN</code> if
     * cache is not enabled
     */
    public double getCacheHits() {
        if(cache != null || arrayCache != null)
            return statCacheHits*1.0/statCacheAccess;
        else
            return Double.NaN;
    }
    /**
     * Return statistics.
     * 
     * @return a textual description
     */
    public String getStats() {
        String out = "map stats\n\t% filled";
        int size = 0;
        // Iterator<byte[]> chunksIt = chunks.iterator();
        for(int[] entries : chunkEntries) {
            int num = entries[0];
            /*
            byte[] c = chunksIt.next();
            int hits = 0;
            for(int i = 0; i < CHUNK_SIZE; ++i) {
                int base = i*ARRAY_SIZE;
                boolean free = true;
                for(int j = 0; j < KEY_SIZE; ++j)
                    if(c[base + j] != 0) {
                        free = false;
                        break;
                    }
                if(!free)
                    ++hits;
            }
            if(hits != num)
                throw new RuntimeException("bad number of entries");
                */
            out += " " + (float)(Math.round(num*1000.0/(CHUNK_SIZE))/10.0);
            size += num;
        }
        out += "\n\tentries";
        for(int[] entries : chunkEntries) {
            int num = entries[0];
            out += " " + num;
        }
        out += "\n\ttotal filled = " + (float)(Math.round(size*1000.0/
                (chunks.size()*CHUNK_SIZE))/10.0) + "%" +
                "\n\ttotal entries = " + size + "\n\ttotal memory = " +
                (int)Math.round((1.0*chunks.size()*ARRAY_SIZE*CHUNK_SIZE)/(1024*1024)) + "m";
        out += "\n\ttotal shifts = " + statShiftCount +
                "\n\tmean shift length = " + (float)(statShiftSum*1.0/statShiftCount);
        if(cache != null || arrayCache != null) {
            out += "\n\tcache hits = total " + (float)(Math.round(statCacheHits*1000.0/
                    statCacheAccess)/10.0) + "%, recent " +
                    (float)(Math.round(statCacheHitsLocal*1000.0/
                    statCacheAccessLocal)/10.0) + "%";
        }
        return out;
    }
}
