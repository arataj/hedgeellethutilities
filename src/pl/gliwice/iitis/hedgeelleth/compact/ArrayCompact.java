/*
 * ArrayCompact.java
 *
 * Created on Jan 17, 2013
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compact;

/**
 * An utility class for the compact byte array storage data types in this
 * package.
 * 
 * @author Artur Rataj
 */
public class ArrayCompact {
    /**
     * <p>Returns the default chunk size defined in the number of element.
     * The returned size is equal to
     * <code>max(scale*1024, ceil(scale*1024*512/arraySize))</code>.</p>
     * 
     * <p>The returned size is relatively large, and thus optimised for storing
     * a very large amount of elements.</p>
     * 
     * @param arraySize size of an element to be stored in a chunk of
     * the returned size
     * @return a chunk size, in the numbetr of elements of the size
     * <code>arraySize</code>
     */
    public static int getDefaultChunkSize(int arraySize, int scale) {
        return Math.max(scale*1024,
                (int)Math.ceil(scale*1024*512/arraySize));
    }

}
