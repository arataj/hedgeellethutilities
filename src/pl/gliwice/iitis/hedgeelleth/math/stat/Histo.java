/*
 * Histo.java
 *
 * Created on Jul 25, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.stat;


import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;
import pl.gliwice.iitis.hedgeelleth.math.rng.ExpDist;

/**
 * A histogram.
 * 
 * @author Artur Rataj
 */
public class Histo {
    public final double MIN;
    public final double MAX;
    protected final double LOG_MIN;
    protected final double LOG_MAX;
    public final boolean LOG;
    public final long[] BUCKET;
    /**
     * If the histogram stores directly longs, this field is null.
     */
    public final Histo[] BUCKET_HISTO;
    public final double[] BUCKET_WIDTH;
    public final int BUCKET_NUM;
    /**
     * Sum of buckets.
     */
    public long sum;
    
    /**
     * Creates a new histogram.
     * 
     * @param min minimum value
     * @param max maximum value
     * @param log false for linear, true for logarithmic
     * @param bucketsNum number of bars/buckets spanning from <code>min</code> to <code>max</code>
     * @param factory a factory of bucket histograms, null if this histogram stores
     * directly long values
     */
    public Histo(double min, double max, boolean log, int bucketNum,
            HistoFactory factory) {
        MIN = min;
        MAX = max;
        if(MIN >= MAX)
            throw new RuntimeException("histogram's maximum must be larger than its minimum, found " +
                    MIN + ", " + MAX);
        LOG = log;
        if(LOG) {
            if(MIN <= 0)
                throw new RuntimeException("log scale must begin with a positive value, found " + MIN);
            LOG_MIN = Math.log(MIN);
            LOG_MAX = Math.log(MAX);
        } else {
            LOG_MIN = Double.NaN;
            LOG_MAX = Double.NaN;
        }
        BUCKET_NUM = bucketNum;
        if(factory == null) {
            BUCKET = new long[BUCKET_NUM];
            BUCKET_HISTO = null;
        } else {
            BUCKET = null;
            BUCKET_HISTO = new Histo[BUCKET_NUM];
            for(int i = 0; i < BUCKET_NUM; ++i)
                BUCKET_HISTO[i] = factory.newHisto();
        }
        BUCKET_WIDTH = new double[BUCKET_NUM];
        for(int i = 0; i < BUCKET_NUM; ++i)
            BUCKET_WIDTH[i] = getMid(i + 0.5) - getMid(i - 0.5);
        sum = 0;
    }
    /**
     * Creates a new histogram, whcih stores longs directly. This is a convenience
     * constructor.
     * 
     * @param min minimum value
     * @param max maximum value
     * @param log false for linear, true for logarithmic
     * @param bucketsNum number of bars/buckets spanning from <code>min</code> to <code>max</code>
     */
    public Histo(double min, double max, boolean log, int bucketNum) {
        this(min, max, log, bucketNum, null);
    }
    /**
     * Reads a histogram from a file, a reverse of <code>toString()</code>.
     * 
     * @param fileName name of the file to read
     */
    public Histo(String fileName) throws IOException {
        try(InputStream in = new FileInputStream(fileName)) {
            double[][] a = NumStream.toArray(in);
            BUCKET_NUM = a.length;
            BUCKET = new long[BUCKET_NUM];
            BUCKET_HISTO = null;
            BUCKET_WIDTH = new double[BUCKET_NUM];
            double startDiff = a[1][0] - a[0][0];
            double stopDiff = a[BUCKET_NUM - 1][0] - a[BUCKET_NUM - 2][0];
            if(!(LOG = (stopDiff/startDiff > 1 + 1e-6))) {
                MIN = a[0][0] - startDiff/2.0;
                MAX = a[BUCKET_NUM - 1][0] + stopDiff/2.0;
                LOG_MIN = Double.NaN;
                LOG_MAX = Double.NaN;
            } else {
                startDiff = Math.log(a[1][0]) - Math.log(a[0][0]);
                stopDiff = Math.log(a[BUCKET_NUM - 1][0]) - Math.log(a[BUCKET_NUM - 2][0]);
                if(Math.abs(stopDiff - startDiff) > 1e-6)
                    throw new RuntimeException("unrecognized bucket widths");
                double diff = (startDiff + stopDiff)/2.0;
                MIN = Math.exp(Math.log(a[0][0]) - diff/2.0);
                MAX = Math.exp(Math.log(a[BUCKET_NUM - 1][0]) + diff/2.0);
                LOG_MIN = Math.log(MIN);
                LOG_MAX = Math.log(MAX);
            }
            for(int i = 0; i < BUCKET_NUM; ++i) {
                long n = Math.round(a[i][1]);
                if(Math.abs(n - a[i][1]) > 1e-6)
                    throw new RuntimeException("a problem with bucket precision");
                BUCKET[i] = n;
                sum += n;
            }
            for(int i = 0; i < BUCKET_NUM; ++i)
                BUCKET_WIDTH[i] = getMid(i + 0.5) - getMid(i - 0.5);
        }
    }
    /**
     * Empties this histogram, like a constructor re-run.
     */
    public void clear() {
        if(BUCKET != null) {
            for(int i = 0; i < BUCKET_NUM; ++i)
                BUCKET[i] = 0;
        } else {
            for(int i = 0; i < BUCKET_NUM; ++i)
                BUCKET_HISTO[i].clear();
        }
        sum = 0;
    }
    /**
     * Returns an index of a bucket, which would be incremented by a given
     * value.
     * 
     * @param value value
     * @return bucket index, -1 if out of range
     */
    public int getBucketIndex(double value) {
        if(value >= MIN && value <= MAX) {
            int num;
            if(LOG)
                num = Math.min(BUCKET_NUM - 1,
                    (int)((Math.log(value) - LOG_MIN)*BUCKET_NUM/(LOG_MAX - LOG_MIN)));
            else
                num = Math.min(BUCKET_NUM - 1,
                    (int)((value - MIN)*BUCKET_NUM/(MAX - MIN)));
            return num;
        } else
            return -1;
    }
    /**
     * Increments an according bucket. Only for histograms that directly store
     * longs.
     * 
     * @param value value to add to the histogram
     */
    public void add(double value) {
        int index = getBucketIndex(value);
//if(index == -1)
//    index = index;
        if(index != -1)
            ++BUCKET[index];
        ++sum;
    }
    /**
     * Increments an according bucket by a given value. Only for histograms
     * that directly store longs.
     * 
     * @param value value to add to the histogram
     * @param num how many times that value should be added
     */
    public void add(double value, long num) {
        int index = getBucketIndex(value);
        if(index != -1)
            BUCKET[index] += num;
        sum += num;
    }
    /**
     * Adds two histograms, their parameters must be identical as the buckets
     * are merged directly. If the parameters differ, a runtime exception is thrown.
     * 
     * @param other a histogram to add to this one; it is not changed
     */
    public void add(Histo other) {
        if(
                (LOG || (MIN == other.MIN &&
                MAX == other.MAX)) &&
                LOG == other.LOG &&
                (!LOG || (LOG_MIN == other.LOG_MIN &&
                LOG_MAX == other.LOG_MAX)) &&
                BUCKET_NUM == other.BUCKET_NUM
                ) {
            if(BUCKET != null) {
                for(int i = 0; i < BUCKET_NUM; ++i)
                    BUCKET[i] += other.BUCKET[i];
            } else {
                for(int i = 0; i < BUCKET_NUM; ++i)
                    BUCKET_HISTO[i].add(other.BUCKET_HISTO[i]);
            }
            sum += other.sum;
        } else
            throw new RuntimeException("parameter mismatch");
    }
    /**
     * Returns a bucket's value, typically the middle value.
     * 
     * @param bucketNum number of the bucket; integer values for the middle value
     * @return x value at a center of the bucket
     */
    public double getMid(double bucketNum) {
        if(LOG)
            return Math.exp(LOG_MIN + (bucketNum + 0.5)*(LOG_MAX - LOG_MIN)/BUCKET_NUM);
        else
            return MIN + (bucketNum + 0.5)*(MAX - MIN)/BUCKET_NUM;
    }
    /**
     * Returns a normalised value from a bucket. Only for histograms which store
     * directly longs.
     * 
     * @param bucketNum number of the bucket
     * @return bucket / <code>sum</code> / width
     */
    public double getNorm(int bucketNum) {
        return BUCKET[bucketNum]*1.0/sum/
                BUCKET_WIDTH[bucketNum];
    }
    /**
     * Returns the weighted mean. Only for histograms which store
     * directly longs.
     * 
     * @return mean value, per added value
     */
    public double getMean() {
        double sum = 0.0;
        long count = 0;
        for(int i = 0; i < BUCKET_NUM; ++i) {
            sum += getMid(i)*BUCKET[i];
            count += BUCKET[i];
        }
        return sum/count;
    }
    /**
     * Returns the variance mean. Only for histograms which store
     * directly longs.
     * 
     * @return variance
     */
    public double getVariance() {
        double mean = getMean();
        double variance = 0.0;
        long count = 0;
        for(int i = 0; i < BUCKET_NUM; ++i) {
            variance += Math.pow(getMid(i) - getMean(), 2.0)*BUCKET[i];
            count += BUCKET[i];
        }
        return variance/count;
    }
    /**
     * If everything has been fit into this histogram, i.e.
     * <code>sum</code> is equal to the actual sum of counts in
     * the buckets.
     * 
     * @return if this histogram's range is wide enough to put all
     * samples in the buckets
     */
    public boolean isComplete() {
        long count = 0;
        for(int i = 0; i < BUCKET_NUM; ++i)
            count += BUCKET[i];
        return count == sum;
    }
    /**
     * Returns a confidence interval, that a sample is smaller/larger
     * at a given probability. A linear interpolation is used within a single
     * bucket.
     * 
     * @param p probability
     * @param lower true: a CI between the lowest value and the returned value,
     * false: a CI between the returned value and the highest value; for false,
     * the method returns values of the quantile function
     * @return a respective limit of the CI or NaN if outside the histogram's
     * range
     */
    public double getEdgeCI(double p, boolean lower) {
        if(p == 0) {
            if(lower)
                return getMid(-0.5);
            else
                return getMid(BUCKET_NUM - 1  + 0.5);
        } else {
            long count = 0;
            long prevCount = 0;
            for(int i = 0; i < BUCKET_NUM; ++i) {
if(i == BUCKET_NUM - 1)
    i = i;
                int num;
                if(lower)
                    num = i;
                else
                    num = BUCKET_NUM - 1 - i;
                prevCount = count;
                count += BUCKET[num];
                double rightP = count*1.0/sum;
                if(rightP >= p) {
                    double leftP = prevCount*1.0/sum;
                    int prevNum;
                    if(lower)
                        prevNum = num - 1;
                    else
                        prevNum = num + 1;
                    double ratio = (p - leftP)/(rightP - leftP);
                    return getMid(0.5 + prevNum + ratio*(num - prevNum));
                }
            }
            return Double.NaN;
        }
    }
    /**
     * Returns a minimum (mean) value found within the buckets.
     * 
     * @return contents of a bucket with a smallest number of additions
     * (long buckets) or with a smallest mean (histo buckets)
     */
    public double getBucketMin() {
        if(BUCKET != null) {
            long min = Long.MAX_VALUE;
            for(long l : BUCKET)
                if(min > l)
                    min = l;
            return min;
        } else {
            double min = Double.MAX_VALUE;
            for(Histo h : BUCKET_HISTO) {
                double m = h.getMean();
                if(min > m)
                    min = m;
            }
            return min;
        }
    }
    public double getBucketMax() {
        if(BUCKET != null) {
            long max = Long.MIN_VALUE;
            for(long l : BUCKET)
                if(max < l)
                    max = l;
            return max;
        } else {
            double max = -Double.MAX_VALUE;
            for(Histo h : BUCKET_HISTO) {
                double m = h.getMean();
                if(max < m)
                    max = m;
            }
            return max;
        }
    }
    @Override
    /**
     * Prints the contents of this histogram to a file: 1st column bucket mid,
     * 2nd column bucket hits, 3nd column probability density.
     */
    public String toString() {
        StringBuilder out = new StringBuilder();
        if(BUCKET != null) {
            for(int i = 0; i < BUCKET_NUM; ++i)
                out.append(getMid(i) + "\t" + BUCKET[i] + "\t" + getNorm(i) + "\n");
        } else
            throw new RuntimeException("can only display 1-dim. histograms");
        return out.toString();
    }
    public static void main(String[] args) {
        Histo h = new Histo(0.01, 10, true, 100);
//        for(double x = 1.0; x <= 10.0; x += 0.001)
//            h.add(x);
//        h.getMean();
//        h.getNorm(0);
        ExpDist d = new ExpDist(1, 47328979);
        for(int i = 0; i < 1e6; ++i)
            h.add(Math.max(0.01, Math.min(10, d.next())));
        h.getEdgeCI(0.5, true);
        System.out.println(h.toString());
        System.out.println(h.getMean());
    }
}
