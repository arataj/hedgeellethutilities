/*
 * Histo2.java
 *
 * Created on Dec 8, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.stat;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.text.DecimalFormat;
import java.util.*;
import java.util.function.IntFunction;
import java.util.function.LongFunction;
import pl.gliwice.iitis.hedgeelleth.math.rng.ExpDist;

/**
 * A two-dimensional histogram.
 * 
 * @author Artur Rataj
 */
public class Histo2 {
    static class BucketFactory implements HistoFactory {
        final double MIN_X;
        final double MAX_X;
        final boolean LOG_X;
        final int BUCKET_NUM_X;
        public BucketFactory(double minX, double maxX, boolean logX, int bucketNumX) {
            MIN_X = minX;
            MAX_X = maxX;
            LOG_X = logX;
            BUCKET_NUM_X = bucketNumX;
        }
        @Override
        public Histo newHisto() {
            return new Histo(MIN_X, MAX_X, LOG_X, BUCKET_NUM_X);
        }
    };
    /**
     * Number of buckets in the X dimension.
     */
    public final int BUCKET_NUM_X;
    /**
     * Number of buckets in the Y dimension.
     */
    public final int BUCKET_NUM_Y;
    /**
     * Y histogram, contains X histograms.
     */
    final Histo HISTO;
    public Histo2(double minX, double maxX, boolean logX, int bucketNumX,
            double minY, double maxY, boolean logY, int bucketNumY) {
        BUCKET_NUM_X = bucketNumX;
        BUCKET_NUM_Y = bucketNumY;
        BucketFactory bf = new BucketFactory(
                minX, maxX, logX, bucketNumX);
        HISTO = new Histo(minY, maxY, logY, bucketNumY, bf);
    }
    /**
     * Empties this histogram, like a constructor re-run.
     */
    public void clear() {
        HISTO.clear();
    }
    /**
     * Increments an according bucket.
     * 
     * @param x x value to add to the histogram
     * @param y y value to add to the histogram
     */
    public void add(double x, double y) {
        int index = HISTO.getBucketIndex(y);
        if(index != -1) {
            Histo histoY = HISTO.BUCKET_HISTO[index];
            histoY.add(x);
        }
        ++HISTO.sum;
    }
    /**
     * Adds two histograms, their parameters must be identical as the buckets
     * are merged directly. If the parameters differ, a runtime exception is thrown.
     * 
     * @param other a histogram to add to this one; it is not changed
     */
    public void add(Histo2 other) {
        HISTO.add(other.HISTO);
    }
    /**
     * Returns a bucket's value X, typically the middle value.
     * 
     * @param bucketNumX x index of the buckets; integer values for the middle value
     * @return x x value at a center of the bucket
     */
    public double getMidX(double bucketNumX) {
        return HISTO.BUCKET_HISTO[0].getMid(bucketNumX);
    }
    /**
     * Returns a bucket's value Y, typically the middle value.
     * 
     * @param bucketNumX y index of the buckets; integer values for the middle value
     * @return y y value at a center of the bucket
     */
    public double getMidY(double bucketNumY) {
        return HISTO.getMid(bucketNumY);
    }
    /**
     * Returns a normalised value from a bucket. Only for histograms which store
     * directly longs.
     * 
     * @param bucketNumX x coord of the bucket
     * @param bucketNumY y coord of the bucket
     * @return bucket / sum / width
     */
    public double getNorm(int bucketNumX, int bucketNumY) {
        Histo histoY = HISTO.BUCKET_HISTO[bucketNumY];
        return histoY.BUCKET[bucketNumX]*1.0/
                (histoY.BUCKET_WIDTH[bucketNumX]*HISTO.BUCKET_WIDTH[bucketNumY])/
                HISTO.sum;
    }
    /**
     * Returns the weighted x mean.
     * 
     * @return mean x value, per added value
     */
    public double getMeanX() {
        double sum = 0.0;
        long count = 0;
        for(int y = 0; y < HISTO.BUCKET_NUM; ++y) {
            Histo histoY = HISTO.BUCKET_HISTO[y];
            for(int x = 0; x < histoY.BUCKET_NUM; ++x) {
                sum += histoY.getMid(x)*histoY.BUCKET[x];
                count += histoY.BUCKET[x];
            }
        }
        return sum/count;
    }
    /**
     * Returns the weighted y mean.
     * 
     * @return mean y value, per added value
     */
    public double getMeanY() {
        double sum = 0.0;
        long count = 0;
        for(int y = 0; y < HISTO.BUCKET_NUM; ++y) {
            Histo histoY = HISTO.BUCKET_HISTO[y];
            for(int x = 0; x < histoY.BUCKET_NUM; ++x) {
                sum += HISTO.getMid(y)*histoY.BUCKET[x];
                count += histoY.BUCKET[x];
            }
        }
        return sum/count;
    }
    /**
     * Returns the weighted x mean for a row.
     * 
     * @param bucketY index of an y bucket (a row)
     * @return weighted y
     */
    public double getMeanRow(int bucketY) {
        return HISTO.BUCKET_HISTO[bucketY].getMean();
    }
    /**
     * Returns the weighted y mean for a column.
     * 
     * @param bucketX index of an x bucket (a column)
     * @return weighted y
     */
    public double getMeanCol(int bucketX) {
        double sum = 0.0;
        long count = 0;
        for(int y = 0; y < HISTO.BUCKET_NUM; ++y) {
            Histo histoY = HISTO.BUCKET_HISTO[y];
            sum += HISTO.getMid(y)*histoY.BUCKET[bucketX];
            count += histoY.BUCKET[bucketX];
        }
        return sum/count;
    }
    public String toStringHeatmap() {
        DecimalFormat f = new DecimalFormat("######0");
        StringBuilder out = new StringBuilder();
        for(int y = 0; y < HISTO.BUCKET_NUM; ++y) {
            Histo histoY = HISTO.BUCKET_HISTO[y];
            for(int x = 0; x < histoY.BUCKET_NUM; ++x)
                out.append(y + "\t" + x + "\t" + "\t" + f.format(histoY.BUCKET[x]) + "\n");
            out.append('\n');
        }
        return out.toString();
    }
    /**
     * Stores as a 16--bit grayscale PNG.
     * 
     * @param fun transfer function, null for 1:1
     * @param clamp if to clamp; if a multiplied bucket exceedes 65535
     * and this parameter is false, a runtime exception is thrown
     * @return a writable image
     */
    public BufferedImage toImage(LongFunction<Integer> fun, boolean clamp) {
        int width = HISTO.BUCKET_HISTO[0].BUCKET_NUM;
        int height = HISTO.BUCKET_NUM;
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_USHORT_GRAY);
        WritableRaster raster = image.getRaster();
        for(int y = 0; y < height; ++y) {
            Histo histoY = HISTO.BUCKET_HISTO[y];
            for(int x = 0; x < width; ++x) {
                long v = histoY.BUCKET[x];
                if(fun != null)
                    v = fun.apply(v);
                if(v > 65535)
                    if(clamp)
                        v = 65535;
                    else
                        throw new RuntimeException("overflow, bucket value = " + histoY.BUCKET[x]);
                if(v > 10)
                    v = v;
                raster.setSample(x, y, 0, v); 
            }
        }
        return image;
    }
    public String toString() {
        DecimalFormat f = new DecimalFormat("######0");
        StringBuilder out = new StringBuilder();
        for(int y = 0; y < HISTO.BUCKET_NUM; ++y) {
            Histo histoY = HISTO.BUCKET_HISTO[y];
            for(int x = 0; x < histoY.BUCKET_NUM; ++x)
                out.append(" " + f.format(histoY.BUCKET[x]));
            out.append('\n');
        }
        return out.toString();
    }
    public static void main(String[] args) {
        Histo2 h2 = new Histo2(
                -5, 5, false, 15,
                1, 1000, true, 20);
        ExpDist dX = new ExpDist(1, 47328979);
        ExpDist dY = new ExpDist(0.01, 47328979);
        for(int i = 0; i < 1e6; ++i)
            h2.add(dX.next() - 4, dY.next() + 10);
        System.out.println(h2.toString());
    }
}
