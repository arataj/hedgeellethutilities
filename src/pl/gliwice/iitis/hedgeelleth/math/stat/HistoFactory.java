/*
 * HistoFactory.java
 *
 * Created on Dec 8, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.stat;

import java.util.*;

/**
 * An abstract  factory of histograms.
 * 
 * @author Artur Rataj
 */
public interface HistoFactory {
    public Histo newHisto();
}
