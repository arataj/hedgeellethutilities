/*
 * Node.java
 *
 * Created on Apr 5, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.eptd;

/**
 *
 * @author Artur Rataj
 */
public class Node {
    Op op;
    
    /**
     * Creates a new instance of <code>Node</code>.
     */
    public Node() {
    }
}
