/*
 * Genotype.java
 *
 * Created on Apr 5, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.eptd;

import java.util.*;

/**
 * A genotype.
 * 
 * @author Artur Rataj
 */
public class Genotype {
    /**
     * Dimensionality of the phenotype.
     */
    public final int dim;
    
    /**
     * Subsequent genes in this genotype.
     */
    List<Gene> genes;
    /**
     * Creates a new instance of <code>Genotype</code>.
     * 
     * @param dim dimensionality of the phenotype
     */
    public Genotype(int dim) {
        this.dim = dim;
        genes = new ArrayList<>();
    }
}
