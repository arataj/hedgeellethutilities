/*
 * Gene.java
 *
 * Created on Apr 5, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.eptd;

import java.util.*;

/**
 * A gene.
 * 
 * @author Artur Rataj
 */
public class Gene {
    /**
     * Genotype, to which this gene belongs.
     */
    Genotype gt;
    /**
     * Minimum coordinates of the active region.
     */
    int[] activeMin;
    /**
     * Maximum coordinates of the active region.
     */
    int[] activeMax;
    /**
     * List of targets.
     */
    List<Target> targets;
    
    /**
     * Creates a new, random <code>Gene</code>.
     * 
     * @param gt genotype, to which this gene belongs
     */
    public Gene(Genotype gt) {
        this.gt = gt;
        initActive();
    }
    /**
     * Initialises the active region.
     */
    private void initActive() {
        activeMin = new int[gt.dim];
        activeMax = new int[gt.dim];
        
    }
}
