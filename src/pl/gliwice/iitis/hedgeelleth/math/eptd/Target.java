/*
 * Target.java
 *
 * Created on Apr 5, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.eptd;

/**
 * A target.
 * 
 * @author Artur Rataj
 */
public class Target {
    /**
     * Coordinates of the target.
     */
    Node[] target;
    /**
     * Rate.
     */
    Node rate;
    
    /**
     * Creates a new instance of <code>Target</code>.
     * 
     * @param gt genotype, to which belongs the gene, that contains this target
     */
    public Target(Genotype gt) {
        target = new Node[gt.dim];
        rate = new Node();
    }
}
