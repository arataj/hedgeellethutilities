/*
 * MathUtil.java
 *
 * Created on Jan 10, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math;

import java.util.*;

/**
 * Different math functions and utilities.
 * 
 * @author Artur Rataj
 */
public class MathUtil {
    /**
     * A binomial, n over k.
     * 
     * @param n n
     * @param k k
     * @return value of the binomial
     */
    public static int binomial(int n, int k)
    {
        if (k>n-k)
            k=n-k;
        long b=1;
        for (int i=1, m=n; i<=k; i++, m--)
            b=b*m/i;
        if(b > Integer.MAX_VALUE)
            throw new RuntimeException("does not fit in integer");
        return (int)b;
    }
    /**
     * For a given number of variables, finds a respective multivariate polynomial
     * and returns subsequent powers of the variables (2nd index) for each
     * coefficient required (1st index). For example, for order = 2 and
     * numVar = 2 the output array might be {{2, 0}, {1, 1}, {0, 2}, {1, 0}, {0, 1}, {0, 0}}
     * which translates to a polynomial of the form a0v1^2 + a1v1v2 + a2v2^2 + a3v1 + a4v2 + a5.
     * 
     * @param order order of the polynomial
     * @param numVar number of variables
     * @return 
     */
    public static int[][] findMultivariatePolyPowers(int order, int numVar) {
        int numCoeff = binomial(order + numVar, order);
        int[][] coeff = new int[numCoeff][];
        int coeffCount = 0;
        int[] counter = new int[numVar];
        // number of possible values of the counter
        int N = (int)Math.round(Math.pow(1 + order, numVar));
        // count from 0 to N - 1
        for(int i = 0; i < N; ++i) {
            int sum = 0;
            for(int pos = 0; pos < numVar; ++pos)
                sum += counter[pos];
            if(sum <= order) {
                // a valid component
//            int[] powers = new int[counter.length];
//            for(int pos = 0; pos < counter.length; ++pos)
//                powers[pos] = counter[pos];
                coeff[coeffCount++] = Arrays.copyOf(counter, counter.length);
                Arrays.copyOf(counter, counter.length);
            }
            // tick
            for(int pos = 0; pos < numVar; ++pos) {
                if(++counter[pos] > order)
                    counter[pos] = 0;
                else
                    break;
            }
        }
        if(coeffCount != numCoeff)
            throw new RuntimeException("unexpected");
        return coeff;
    }
    /**
     * tanh^-1
     */
    public static double tanhr(double x) {
        return 0.5*Math.log((1.0 + Math.abs(x))/(1.0 - Math.abs(x)))*Math.signum(x);
    }
    /**
     * AQ soft threshold, 0 .. 1 -> 0 .. 1
     * 
     * S = -1 .. 1; 0 for linear, S = 1.5, 3, 6 for weakly, moderately, strongly thresholded;
     * negative S for an inverse function, e.g. S = -3 would moderately flatten middle
     * arguments 0.2 .. 0.8 to a narrow range of values around 0.5
     * @return 
     */
    public static double soft(double x, double S) {
        if(S == 0)
            return x;
        else if(S > 0)
            return (Math.tanh((x*2 - 1)*S) - Math.tanh(-S))/
                    (2*Math.tanh(S));
        else {
            S *= -1;
            return (tanhr((x*2 - 1)*Math.tanh(S)) + S)/(2*S);
        }
    }
    public static void main(String[] args) {
        for(double x = 0; x <= 1.0001; x += 0.01) {
            System.out.println(x + "\t" +
                    soft(x, -3) + "\t" +
                    soft(x, -1.5) + "\t" +
                    soft(x, 0) + "\t" +
                    soft(x, 1.5) + "\t" +
                    soft(x, 3));
        }
    }
}
