/*
 * AbstractOptSpaceDescr.java
 *
 * Created on Jan 22, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math;

import java.util.*;

/**
 * A description of the optimisation space: bounds, initial point and
 * step.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractOptSpaceDescr {
    /**
     * Minimum possible coordinates of the optimisation space.
     */
    public double[] lower;
    /**
     * Maximum possible coordinates of the optimisation space.
     */
    public double[] upper;
    /**
     * Starting point.
     */
    public double[] initial;
    /**
     * Initial step.
     */
    public double[] initialStep;
}
