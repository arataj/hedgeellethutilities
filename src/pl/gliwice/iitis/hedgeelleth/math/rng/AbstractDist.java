/*
 * AbstractDist.java
 *
 * Created on Apr 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

/**
 * An abstract probability distribution.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractDist {
    /**
     * Creates a new instance of <code>AbstractDist</code>.
     */
    public AbstractDist() {
    }
    /**
     * Draws a random value from this distribution.
     * 
     * @return the value drawn
     */
    public abstract double next();
    /**
     * Returns the inverse cumulative probability. Implementation is optional.<br>
     * 
     * The default implementation returns <code>Double.NaN</code>.
     * 
     * @param x argument, 0 ... 1
     * @return inverse cumulative probability for <code>x</code> or
     * <code>Double.NaN</code> meaning not implemented
     */
    public double inverseCumulativePr(double x) {
        return Double.NaN;
    }
    /**
     * Returns the density at a given value.
     * 
     * @param x value
     * @return density
     */
    public abstract double getDensity(double x);
    /**
     * Returns a moment: 1st -- mean == expected value, 2nd == variance,
     * 3rd == skewness.
     * 
     * @param num number of the moment to return.
     */
    public abstract double getMoment(int num);
    /**
     * Returns the expected value == getMoment(1).
     * 
     * @return expected value
     */
    public double getMean() {
        return getMoment(1);
    }
    /**
     * Returns the variance == getMoment(2).
     * 
     * @return variance
     */
    public double getVariance() {
        return getMoment(2);
    }
    /**
     * Returns the standarised skewness == getMoment(3).
     * 
     * @return variance
     */
    public double getSkewness() {
        return getMoment(3);
    }
    /**
     * Returns the standard deviation == sqrt(getMoment(2)).
     * 
     * @return variance
     */
    public double getSigma() {
        return Math.sqrt(getMoment(2));
    }
}
