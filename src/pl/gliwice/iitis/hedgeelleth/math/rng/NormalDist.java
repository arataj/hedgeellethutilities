/*
 * NormalDist.java
 *
 * Created on Apr 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import java.util.Random;

/**
 * Normal distribution.
 * 
 * @author Artur Rataj
 */
public class NormalDist extends AbstractDist {
    /**
     * Expectation.
     */
    protected final double mean;
    /**
     * Standard deviation.
     */
    protected final double sigma;
    /**
     * Variance = <code>sigma</code>^2.
     */
    protected final double variance;
    /**
     * A generator of random numbers.
     */
    static Random random = new Random();
    /**
     * A helper coefficient for computing density.
     */
    private double densityMult;
    /**
     * A helper coefficient for computing density.
     */
    private double densityEMult;
    
    /**
     * Creates a new instance of <code>NormalDist</code>.
     * 
     * @param mean mean value
     * @param sigma standard deviation
     */
    public NormalDist(double mean, double sigma) {
        this.mean = mean;
        this.sigma = sigma;
        variance = this.sigma*this.sigma;
        densityMult = 1.0/(this.sigma*Math.sqrt(2.0*Math.PI));
        densityEMult = -1.0/(2.0*variance);
    }
    @Override
    public double next() {
        return random.nextGaussian()*sigma + mean;
    }
    @Override
    public double getDensity(double x) {
        double sqr = (x - mean);
        sqr = sqr*sqr;
        return densityMult*Math.exp(densityEMult*sqr);
    }
    @Override
    public double getMoment(int num) {
        switch(num) {
            case 1:
                return mean;
                
            case 2:
                return variance;

            case 3:
                return 0.0;
                
            default:
                throw new RuntimeException("invalid moment number");
        }
    }
    @Override
    public double getSigma() {
        return sigma;
    }
}
