/*
 * SampledDist.java
 *
 * Created on Apr 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import pl.gliwice.iitis.hedgeelleth.math.*;

/**
 * A tabularised distribution, built from sampling another distribution.
 * 
 * @author Artur Rataj
 */
public class SampledDist extends ArrayDist {
    /**
     * Creates a new instance of <code>SampledDist</code>, by approximating
     * another distribution.<br>
     * 
     * The approximating data is normalised.
     * 
     * @param origin the distribution to sample
     * @param xMin minimum x of samples
     * @param xMax maximum x of samples
     * @param barsNum length of the mass array
     * @param oversampling samples per element of the mass array
     */
    public SampledDist(AbstractDist origin, double xMin, double xMax,
            int barsNum, int oversampling) {
        super(sampleMass(origin, xMin, xMax, barsNum, oversampling));
    }
    /**
     * Samples a given distribution, returning an approximating mass array.
     * The approximating data is normalised.
     * 
     * @param origin the distribution to sample
     * @param xMin minimum x of samples
     * @param xMax maximum x of samples
     * @param barsNum length of the mass array
     * @param oversampling samples per element of the mass array
     */
    public static Xxy[] sampleMass(AbstractDist origin, double xMin, double xMax,
            int barsNum, int oversampling) {
        Xxy[] out = new Xxy[barsNum];
        for(int bar = 0; bar < barsNum; ++bar) {
            double xLeft = xMin + (xMax - xMin)*bar/barsNum;
            double xRight = xMin + (xMax - xMin)*(bar + 1)/barsNum;
            double integral = 0.0;
            for(int sample = 0; sample < oversampling; ++sample) {
                double x = xLeft + (xRight - xLeft)*(sample + 0.5)/oversampling;
                integral += origin.getDensity(x);
            }
            integral = integral/oversampling;
            out[bar] = new Xxy(xLeft, xRight, integral);
        }
        normalise(out);
        return out;
    }
}
