/*
 * WeibullDist.java
 *
 * Created on Apr 4, 2012
 *
 * Copyright (c) 2012 Artur Rataj, Apache Software Foundation
 *
 * The method <code>inverseCumulativePr</code> based on code from
 * Apache Commons Math version 3.
 * 
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import org.apache.commons.math3.special.Gamma;

/**
 * A Weibull distribution.
 * 
 * @author Artur Rataj
 */
public class WeibullDist extends AbstractDist {
    /**
     * Scale parameter.
     */
    protected final double lambda;
    /**
     * Shape parameter.
     */
    protected final double k;
    /**
     * Expectation.
     */
    protected final double mean;
    /**
     * Variance.
     */
    protected final double variance;
    /**
     * Skewness.
     */
    protected final double skewness;
    /**
     * A helper coefficient.
     */
    private final double scaledK;

    /**
     * Default inverse cumulative probability accuracy.
     */
    public static final double DEFAULT_INVERSE_ABSOLUTE_ACCURACY = 1e-9;
    
    /**
     * Creates a new instance of <code>WeibullDist</code>.
     * 
     * @param lambda scale parameter
     * @param k shape parameter
     */
    public WeibullDist(double lambda, double k) {
        this.lambda = lambda;
        this.k = k;
        // median = this.lambda*Math.pow(Math.log(2.0), 1.0/this.k);
        mean = this.lambda*gamma(1.0 + 1.0/this.k);
        variance = Math.pow(this.lambda, 2.0)*
                gamma(1.0 + 2.0/this.k) -
                Math.pow(mean, 2.0);
        skewness = (gamma(1.0 + 3.0/this.k)*Math.pow(this.lambda, 3.0) -
                3.0*mean*variance - Math.pow(mean, 3.0))/
                Math.pow(variance, 3.0/2.0);
        scaledK = k/lambda;
    }
    /**
     * Returns the Gamma function for real numbers.
     * 
     * @param x argument
     * @return Γ(x)
     */
    private double gamma(double x) {
        return Math.exp(Gamma.logGamma(x));
    }
    /**
     * Returns the scale parameter.
     * 
     * @return lambda
     */
    public double getLambda() {
        return lambda;
    }
    /**
     * Returns the shape parameter.
     * 
     * @return k
     */
    public double getK() {
        return k;
    }
    @Override
    public double next() {
        return ICPSampling.next(this);
    }
    @Override
    public double inverseCumulativePr(double x) {
        if (x < 0.0 || x > 1.0)
            throw new RuntimeException("argument out of range");
        else if (x == 0)
            return 0.0;
        else  if (x == 1)
            return Double.POSITIVE_INFINITY;
        else
            return lambda*Math.pow(-Math.log(1.0 - x), 1.0/k);
    }
    @Override
    public double getDensity(double x) {
        if(x < 0.0)
            return 0.0;
        else {
            double scaledX = x/lambda;
            return scaledK*Math.pow(scaledX, k - 1.0)*
                    Math.exp(-Math.pow(scaledX, k));
        }
    }
    @Override
    public double getMoment(int num) {
        switch(num) {
            case 1:
                return mean;
                
            case 2:
                return variance;

            case 3:
                return skewness;
                
            default:
                throw new RuntimeException("invalid moment number");
        }
    }
}
