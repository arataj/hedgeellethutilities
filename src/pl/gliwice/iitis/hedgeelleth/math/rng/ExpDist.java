/*
 * ExpDist.java
 *
 * Created on Apr 11, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import java.util.SplittableRandom;

/**
 * Exponential distribution.
 * 
 * @author Artur Rataj
 */
public class ExpDist extends AbstractDist {
    /**
     * Rate.
     */
    protected final double lambda;
    /**
     * Expectation.
     */
    protected final double mean;
    /**
     * Variance.
     */
    protected final double variance;
    /**
     * A generator of random numbers.
     */
    static SplittableRandom random;
    /**
     * Creates a new instance of <code>ExpDist</code>.
     * 
     * @param lambda rate
     * @param sigma standard deviation
     * @param seed seed, -1 for random
     */
    public ExpDist(double lambda, long seed) {
        this.lambda = lambda;
        mean = 1.0/this.lambda;
        variance = mean*mean;
        if(seed == -1)
            random = new SplittableRandom();
        else
            random = new SplittableRandom(seed);
    }
    /**
     * Creates a new instance of <code>ExpDist</code>, with a random seed.
     * This is a convenience constructor.
     * 
     * @param lambda rate
     * @param sigma standard deviation
     */
    public ExpDist(double lambda) {
        this(lambda, -1);
    }
    @Override
    public double next() {
        return -mean*Math.log(1.0 - random.nextDouble());
    }
    @Override
    public double getDensity(double x) {
        if(x < 0)
            return 0.0;
        else
            return lambda*Math.exp(-lambda*x);
    }
    @Override
    public double getMoment(int num) {
        switch(num) {
            case 1:
                return mean;
                
            case 2:
                return variance;

            case 3:
                return 2.0;
                
            default:
                throw new RuntimeException("invalid moment number");
        }
    }
    /**
     * Returns the rate.
     * 
     * @return rate
     */
    public double getLambda() {
        return lambda;
    }
}
