/*
 * OlssonEM.java
 *
 * Created on May 22, 2012
 *
 * Original C code (c) Maria Olsson. Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import java.io.*;
import static java.lang.Math.*;

import org.jblas.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;
import pl.gliwice.iitis.hedgeelleth.math.Xxy;

/**
 * This is a Java port of S. Asmussen, O. Nerman & M. Olsson, Fitting phase-type
 * distribution via the EM algorithm, Scand. J. Statist. 23, 419-441 (1996).<br>
 * 
 * It is a conversion of portions of EMpht.c.
 * 
 * @author Artur Rataj
 */
public class OlssonEM extends GeneralPTDist {
    final boolean DEBUG = false;
    /**
     * If to create an intermediate matrix file "phases.txt" within
     * chosen iterations.
     */
    final boolean SAVE_PHASES = false;
    
    public enum PHType {
        GENERAL,
        HYPEREXP,
        SUM_OF_EXP,
        COX,
        COX_GENERAL,
    };
    
    double[] obs, weight, censur, cweight, lower, upper, intweight;
    double SumOfWeights, SumOfCensored, SumOfInt;
    int[] partner;
    int NoOfObs, NoOfCensored, NoOfInt;
    
    /**
     * Creates a phase--type distribution of a given type, that fits to given
     * probability density.
     * 
     * @param density density to fit to
     * @param phType type of the phase--type distribution
     * @param phases number of phases of the distribution
     * @param iterations number of fitting iterations
     */
    public OlssonEM(Xxy[] density, PHType phType, int phases, int iterations) {
        phasesNum = phases;
        fit(density, phType, phases, iterations);
    }
    void init_vector(double[] vector, int NoOfElements) {
        int i;
        for (i = 0; i < NoOfElements; i++) {
            vector[i] = 0.0;
        }
    }
    void init_integervector(int[] vector, int dim) {
        int i;
        for (i = 0; i < dim; i++) {
            vector[i] = 0;
        }
    }
    void init_matrix(double[][] matrix, int dim1, int dim2) {
        int i, j;
        for (i = 0; i < dim1; i++) {
            for (j = 0; j < dim2; j++) {
                matrix[i][j] = 0.0;
            }
        }
    }
    void init_integermatrix(int[][] matrix, int dim1, int dim2) {
        int i, j;
        for (i = 0; i < dim1; i++) {
            for (j = 0; j < dim2; j++) {
                matrix[i][j] = 0;
            }
        }
    }
    void init_3dimmatrix(double[][][] matrix, int dim1, int dim2, int dim3) {
        int i, j, k;
        for (i = 0; i < dim1; i++) {
            for (j = 0; j < dim2; j++) {
                for (k = 0; k < dim3; k++) {
                    matrix[i][j][k] = 0;
                }
            }
        }
    }
    void a_rungekutta(int p, double[] avector, double[][] ka, double dt, double h,
            double[][] T) {
        int i, j;
        double eps, h2, sum;

        i = (int) (dt / h);
        h2 = dt / (i + 1);
        init_matrix(ka, 4, p);
        for (eps = 0; eps <= dt - h2 / 2; eps += h2) {
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[j][i] * avector[j];
                }
                ka[0][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[j][i] * (avector[j] + ka[0][j] / 2);
                }
                ka[1][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[j][i] * (avector[j] + ka[1][j] / 2);
                }
                ka[2][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[j][i] * (avector[j] + ka[2][j]);
                }
                ka[3][i] = h2 * sum;
            }

            for (i = 0; i < p; i++) {
                avector[i] += (ka[0][i] + 2 * ka[1][i] + 2 * ka[2][i] + ka[3][i]) / 6;
            }
        }
    }
    void rungekutta(int p, double[] avector, double[] gvector, double[] bvector,
            double[][] cmatrix, double dt, double h, double[][] T, double[] t,
            double[][] ka, double[][] kg, double[][] kb, double[][][] kc) {
        int i, j, k, m;
        double eps, h2, sum;

        i = (int) (dt / h);
        h2 = dt / (i + 1);
        init_matrix(ka, 4, p);
        init_matrix(kb, 4, p);
        init_3dimmatrix(kc, 4, p, p);
        if (kg != null) {
            init_matrix(kg, 4, p);
        }
        for (eps = 0; eps <= dt - h2 / 2; eps += h2) {
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[j][i] * avector[j];
                }
                ka[0][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[j][i] * (avector[j] + ka[0][j] / 2);
                }
                ka[1][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[j][i] * (avector[j] + ka[1][j] / 2);
                }
                ka[2][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[j][i] * (avector[j] + ka[2][j]);
                }
                ka[3][i] = h2 * sum;
            }
            if (gvector != null) {
                for (i = 0; i < p; i++) {
                    kg[0][i] = h2 * avector[i];
                }
                for (i = 0; i < p; i++) {
                    kg[1][i] = h2 * (avector[i] + ka[0][i] / 2);
                }
                for (i = 0; i < p; i++) {
                    kg[2][i] = h2 * (avector[i] + ka[1][i] / 2);
                }
                for (i = 0; i < p; i++) {
                    kg[3][i] = h2 * (avector[i] + ka[2][i]);
                }
                for (i = 0; i < p; i++) {
                    gvector[i] += (kg[0][i] + 2 * kg[1][i] + 2 * kg[2][i] + kg[3][i]) / 6;
                }
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[i][j] * bvector[j];
                }
                kb[0][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[i][j] * (bvector[j] + kb[0][j] / 2);
                }
                kb[1][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[i][j] * (bvector[j] + kb[1][j] / 2);
                }
                kb[2][i] = h2 * sum;
            }
            for (i = 0; i < p; i++) {
                sum = 0;
                for (j = 0; j < p; j++) {
                    sum += T[i][j] * (bvector[j] + kb[2][j]);
                }
                kb[3][i] = h2 * sum;
            }
            for (m = 0; m < p; m++) {
                for (i = 0; i < p; i++) {
                    sum = t[m] * avector[i];
                    for (j = 0; j < p; j++) {
                        sum += T[m][j] * cmatrix[j][i];
                    }
                    kc[0][m][i] = h2 * sum;
                }
            }
            for (m = 0; m < p; m++) {
                for (i = 0; i < p; i++) {
                    sum = t[m] * (avector[i] + ka[0][i] / 2);
                    for (j = 0; j < p; j++) {
                        sum += T[m][j] * (cmatrix[j][i] + kc[0][j][i] / 2);
                    }
                    kc[1][m][i] = h2 * sum;
                }
            }
            for (m = 0; m < p; m++) {
                for (i = 0; i < p; i++) {
                    sum = t[m] * (avector[i] + ka[1][i] / 2);
                    for (j = 0; j < p; j++) {
                        sum += T[m][j] * (cmatrix[j][i] + kc[1][j][i] / 2);
                    }
                    kc[2][m][i] = h2 * sum;
                }
            }
            for (m = 0; m < p; m++) {
                for (i = 0; i < p; i++) {
                    sum = t[m] * (avector[i] + ka[2][i]);
                    for (j = 0; j < p; j++) {
                        sum += T[m][j] * (cmatrix[j][i] + kc[2][j][i]);
                    }
                    kc[3][m][i] = h2 * sum;
                }
            }
            for (i = 0; i < p; i++) {
                avector[i] += (ka[0][i] + 2 * ka[1][i] + 2 * ka[2][i] + ka[3][i]) / 6;
                bvector[i] += (kb[0][i] + 2 * kb[1][i] + 2 * kb[2][i] + kb[3][i]) / 6;
                for (j = 0; j < p; j++) {
                    cmatrix[i][j] += (kc[0][i][j] + 2 * kc[1][i][j] + 2 * kc[2][i][j] + kc[3][i][j]) / 6;
                }
            }
        }
    }
    double density(double t, int type, double[] par) {
        if (t <= 0) {
            return (0);
        } else {
            switch (type) {
                case 1:
                    if ((t >= par[0]) && (t <= par[1])) {
                        return (1 / (par[1] - par[0]));
                    } else {
                        return (0);
                    }

                case 2:
                    return (1 / (sqrt(2 * Math.PI) * par[1] * exp((t - par[0]) * (t - par[0])
                            / (2 * par[1] * par[1]))));

                case 3:
                    return (1 / (sqrt(2 * Math.PI) * t * par[1] * exp((log(t) - par[0]) * (log(t) - par[0])
                            / (2 * par[1] * par[1]))));

                case 4:
                    return (par[0] * pow(par[1], par[0]) * pow(t, par[0] - 1) / exp(pow((par[1] * t),
                            par[0])));

                case 5:
                    return (par[0] / (sqrt(2 * Math.PI)) * pow(t, -1.5)
                            * exp(par[1] * par[0] - 0.5 * (par[0] * par[0] / t + par[1] * par[1] * t)));

                case 7:
                    throw new UnsupportedOperationException("not implemented");

                default:
                    return 0.0;
            }
        }
    }
    void ExportToMatlab_Phasetype(int dim, double h, double dt, double truncpoint,
            double[] pie, double[][] q, double[] exit) {
        int i, j;
        double t, deltat;
        double density, cumulative, intensity;
        double[] avector;
        double[][] ka;
        FileOutputStream utfil2;
        try {
            utfil2 = new FileOutputStream("inputdistr.txt");
            PrintWriter out = new PrintWriter(utfil2);
            deltat = truncpoint * 1.1 / 400;
            i = (int) (deltat / h);
            h = deltat / (i + 1);
            avector = new double[dim];
            ka = new double[4][dim];
            for (i = 0; i < dim; i++) {
                avector[i] = pie[i];
            }

            for (t = 0; t <= truncpoint * 1.3; t += deltat) {
                density = 0;
                for (j = 0; j < dim; j++) {
                    density += avector[j] * exit[j];
                }

                cumulative = 1;
                for (j = 0; j < dim; j++) {
                    cumulative -= avector[j];
                }

                if (cumulative < 0.9999) {
                    intensity = density / (1 - cumulative);
                } else {
                    intensity = 0;
                }
                if(DEBUG)
                    out.println(t + " " + cumulative + " " + density + " " + intensity);
                a_rungekutta(dim, avector, ka, deltat, h, q);
            }
            if (out.checkError()) {
                throw new IOException("error writing stream");
            }
            utfil2.close();
        } catch (IOException e) {
            throw new RuntimeException("could not write output file: "
                    + e.toString());
        }
    }
    void ExportToEMPHTmain_Phasetype(int dim, double h, double dt, double truncpoint,
            double[] pie, double[][] q, double[] exit) {
        int i, j;
        double t, tcenterofmass, deltat;
        double density, density1, density2, density3;
        double[] avector;
        double[][] ka;

        FileOutputStream utfil2;
        try {
            utfil2 = new FileOutputStream("sample.txt");
            PrintWriter out = new PrintWriter(utfil2);
            deltat = dt;
            i = (int) (deltat / h);
            h = deltat / (i + 1);
            avector = new double[dim];
            ka = new double[4][dim];
            for (i = 0; i < dim; i++) {
                avector[i] = pie[i];
            }

            for (t = deltat; t <= truncpoint + deltat / 2; t += deltat) {
                density1 = 0;
                for (j = 0; j < dim; j++) {
                    density1 += avector[j] * exit[j];
                }
                a_rungekutta(dim, avector, ka, deltat / 2, h, q);
                density2 = 0;
                for (j = 0; j < dim; j++) {
                    density2 += avector[j] * exit[j];
                }
                a_rungekutta(dim, avector, ka, deltat / 2, h, q);
                density3 = 0;
                for (j = 0; j < dim; j++) {
                    density3 += avector[j] * exit[j];
                }
                density = (density1 + 4 * density2 + density3) / 6;
                tcenterofmass = (density1 * t + 4 * density2 * (t + deltat / 2) + density3 * (t + deltat))
                        / (6 * density);
                if(DEBUG)
                    out.println(tcenterofmass + " " + density);
            }
            if(DEBUG)
                out.println("-1");
            utfil2.close();
        } catch (IOException e) {
            throw new RuntimeException("could not write output file: "
                    + e.toString());
        }
    }
    void ExportToMatlab(int k, double[] parameter, double truncpoint) {
        double t, deltat;
        double cumulative, intensity;
        FileOutputStream utfil2;
        try {
            utfil2 = new FileOutputStream("inputdistr.txt");
            PrintWriter out = new PrintWriter(utfil2);
            cumulative = 0;
            deltat = truncpoint / 400;
            for (t = deltat; t <= truncpoint + deltat / 2; t += deltat) {
                if (t > 0) {
                    cumulative += (density(t - deltat, k, parameter)
                            + 4 * density(t - deltat / 2, k, parameter)
                            + density(t, k, parameter)) * deltat / 6;
                }
                if (cumulative < 0.9999) {
                    intensity = density(t, k, parameter) / (1 - cumulative);
                } else {
                    intensity = 0;
                }
                if(DEBUG)
                    out.println(t + " " + cumulative + " " + density(t, k, parameter)
                        + " " + intensity);
            }
            utfil2.close();
        } catch (IOException e) {
            throw new RuntimeException("could not write output file: "
                    + e.toString());
        }
    }
    void show_pi_T(int p, double[] pi, double[][] T, double[] t) {
        int i, j;

        if(DEBUG) {
            for (i = 0; i < p; i++) {
                System.out.print("\n" + pi[i] + "    ");
                for (j = 0; j < p; j++) {
                    System.out.print(T[i][j] + " ");
                }
                System.out.print("   " + t[i]);
            }
            System.out.println();
        }
    }
    double set_steplength(int p, double[][] T) {
        int i;
        double h;

        h = -0.1 / T[0][0];
        for (i = 1; i < p; i++) {
            if (h > -0.1 / T[i][i]) {
                h = -0.1 / T[i][i];
            }
        }
        return (h);
    }
    void assign_Cvectors(Xxy[] samples) {
        int i;
        double Obs, Weight, Low, Upp;

        i = 0;
        for (i = 0; i < samples.length; ++i) {
            Xxy p = samples[i];
            lower[i] = p.xL;
            upper[i] = p.xR;
            intweight[i] = p.y;
            SumOfInt += intweight[i];
        }
    }
    int sort_observations(int size, double[] vec1, double[] vec2) {
        int i, j, newsize;
        double tempweight;
        double tempobs;

        newsize = size;
        for (i = 0; i < size - 1; i++) {
            for (j = i + 1; j < size; j++) {
                if (vec1[i] > vec1[j]) {
                    tempobs = vec1[i];
                    vec1[i] = vec1[j];
                    vec1[j] = tempobs;
                    tempweight = vec2[i];
                    vec2[i] = vec2[j];
                    vec2[j] = tempweight;
                }
            }
        }
        for (i = size - 2; i >= 0; i--) {
            if (vec1[i] == vec1[i + 1]) {
                vec2[i] += vec2[i + 1];
                newsize--;
                for (j = i + 1; j < newsize; j++) {
                    vec1[j] = vec1[j + 1];
                    vec2[j] = vec2[j + 1];
                }
            }
        }
        return newsize;
    }
    int sort_interval(int NoOfPairs) {
        double temp;
        int tempI;
        int i, j, v, newsize;

        for (i = 0; i < NoOfPairs - 1; i++) {
            for (j = i + 1; j < NoOfPairs; j++) {
                if (lower[i] > lower[j]) {
                    temp = lower[i];
                    lower[i] = lower[j];
                    lower[j] = temp;
                    temp = upper[i];
                    upper[i] = upper[j];
                    upper[j] = temp;
                    temp = intweight[i];
                    intweight[i] = intweight[j];
                    intweight[j] = temp;
                }
            }
        }

        newsize = NoOfPairs;
        if(false)
            for (i = NoOfPairs - 2; i >= 0; i--) {
                if (lower[i] == lower[i + 1] && upper[i] == upper[i + 1]) {
                    intweight[i] += intweight[i + 1];
                    newsize--;
                    for (j = i + 1; j < newsize; j++) {
                        lower[j] = lower[j + 1];
                        upper[j] = upper[j + 1];
                        intweight[j] = intweight[j + 1];
                    }
                }
            }

        partner[0] = 0;
        v = 0;
        for (i = 1; i < newsize; i++) {
            if (lower[i] > lower[i - 1]) {
                v++;
            }
            partner[i] = v;
        }

        for (i = 0; i < newsize - 1; i++) {
            for (j = i + 1; j < newsize; j++) {
                if (upper[i] > upper[j]) {
                    temp = upper[i];
                    upper[i] = upper[j];
                    upper[j] = temp;
                    tempI = partner[i];
                    partner[i] = partner[j];
                    partner[j] = tempI;
                    temp = intweight[i];
                    intweight[i] = intweight[j];
                    intweight[j] = temp;
                }
            }
        }
        return (newsize);
    }
    void AskForInput(Xxy[] samples) {
        int num = samples.length;
        int input, sampletype;

        lower = new double[num];
       upper = new double[num];
        intweight = new double[num];
        partner = new int[num];
        assign_Cvectors(samples);
        NoOfInt = sort_interval(samples.length);
    }
    double random() {
        return 0.1 + 0.9*Math.random();
    }
    void randomphase(int p, double[] pi, double[][] T, double[] exitvector,
            int[] pilegal, int[][] Tlegal) {
        int i, j;
        double r, sum, scalefactor;

        if ((NoOfObs > NoOfCensored) || (NoOfObs > NoOfInt)) {
            scalefactor = obs[(NoOfObs - 1) / 2];
        } else if (NoOfInt > NoOfCensored) {
            scalefactor = upper[NoOfInt / 2];
        } else {
            scalefactor = censur[NoOfCensored / 2];
        }

        sum = 0;
        for (i = 0; i < p; i++) {
            if (pilegal[i] == 1) {
                pi[i] = random();
                sum += pi[i];
            }
        }
        for (i = 0; i < p; i++) {
            pi[i] = pi[i] / sum;
        }
        for (i = 0; i < p; i++) {
            for (j = 0; j < p; j++) {
                if ((i != j) && (Tlegal[i][j] == 1)) {
                    T[i][j] = random();
                    T[i][i] -= T[i][j];
                }
            }
        }
        for (i = 0; i < p; i++) {
            if (Tlegal[i][i] == 1) {
                r = random();
                T[i][i] -= r;
                exitvector[i] = r;
            }
        }
        for (i = 0; i < p; i++) {
            exitvector[i] = exitvector[i] * p / scalefactor;
            for (j = 0; j < p; j++) {
                T[i][j] = T[i][j] * p / scalefactor;
            }
        }
    }
    void selectstructure(PHType structure,
            int p, double[] pi, double[][] T, double[] t, int[] pilegal,
            int[][] Tlegal) {
        int i, j;

        /*
         * printf("Select distribution type: \n"); printf(" 1. General
         * phase-type \n"); printf(" 2. Hyperexponential \n"); printf(" 3. Sum
         * of exponentials \n"); printf(" 4. Coxian \n"); printf(" 5. Coxian
         * general \n"); printf(" 6. User specified structure (from file
         * 'distrtype') \n"); printf(" 7. User specified starting values (from
         * file 'phases') \n"); printf(" 8. User specified starting values (from
         * keyboard) \n");
         */
        switch (structure) {
            case GENERAL:
                for (i = 0; i < p; i++) {
                    pilegal[i] = 1;
                    for (j = 0; j < p; j++) {
                        Tlegal[i][j] = 1;
                    }
                }
                break;
            case HYPEREXP:
                for (i = 0; i < p; i++) {
                    pilegal[i] = 1;
                    Tlegal[i][i] = 1;
                }
                break;
            case SUM_OF_EXP:
                pilegal[0] = 1;
                for (i = 0; i < p - 1; i++) {
                    Tlegal[i][i + 1] = 1;
                }
                Tlegal[p - 1][p - 1] = 1;
                break;
            case COX:
                pilegal[0] = 1;
                for (i = 0; i < p - 1; i++) {
                    Tlegal[i][i] = 1;
                    Tlegal[i][i + 1] = 1;
                }
                Tlegal[p - 1][p - 1] = 1;
                break;
            case COX_GENERAL:
                for (i = 0; i < p - 1; i++) {
                    pilegal[i] = 1;
                    Tlegal[i][i] = 1;
                    Tlegal[i][i + 1] = 1;
                }
                Tlegal[p - 1][p - 1] = 1;
                pilegal[p - 1] = 1;
                break;
            /*
             * case 6: infil=fopen("distrtype", "r"); for (i=0; i < p; i++) {
             * fscanf(infil,"%d", &pilegal[i]); for (j=0; j < p; j++)
             * fscanf(infil,"%d", &Tlegal[i][j]); } printf("Number of
             * phases:%d\n",p); fclose(infil); break; case 7:
             * infil=fopen("phases", "r"); for (i=0; i < p; i++) { fscanf(infil,
             * "%le", &pi[i]); for (j=0; j < p; j++) fscanf(infil, "%le",
             * &T[i][j]); } fclose(infil); break; case 8: for (i=0; i < p; i++)
             * { printf("pi[%d]: ", i+1); scanf("%le", &pi[i]); for (j=0; j < p;
             * j++) { printf("T[%d][%d]: ", i+1, j+1); scanf("%le", &T[i][j]); }
             * } break;
             */
        }
        if(DEBUG) {
            System.out.println("\n Phase-type structure:");
            for (i = 0; i < p; i++) {
                System.out.print("\n" + pilegal[i] + "     ");
                for (j = 0; j < p; j++) {
                    System.out.print(Tlegal[i][j] + " ");
                }
            }
        }
        randomphase(p, pi, T, t, pilegal, Tlegal);
    }
    void EMstep(int p, double h, double[] pi, double[][] T, double[] t, double[] gvector, double[] avector, double[] bvector, double[][] cmatrix,
            double[] Bmean, double[] Zmean, double[][] Nmean, double[][] kg,
            double[][] ka, double[][] kb, double[][][] kc, double[] ett, double[][] g_left, double[][] a_left, double[][] b_left, double[][][] c_left) {
        int i, j, k, n, v, m, mate;
        double pitimesb, dt, adiffsum, prev;

        init_vector(Bmean, p);
        init_vector(Zmean, p);
        init_matrix(Nmean, p, p + 1);

        if (NoOfObs > 0) {
            for (i = 0; i < p; i++) {
                avector[i] = pi[i];
                bvector[i] = t[i];
            }
            init_matrix(cmatrix, p, p);
            dt = obs[0];
            for (k = 0; k < NoOfObs; k++) {
                if (dt > 0) {
                    rungekutta(p, avector, null, bvector, cmatrix, dt, h, T, t, ka, null,
                            kb, kc);
                }
                pitimesb = 0;
                for (i = 0; i < p; i++) {
                    pitimesb += pi[i] * bvector[i];
                }
                for (i = 0; i < p; i++) {
                    Bmean[i] += pi[i] * bvector[i] * weight[k] / pitimesb;
                    Nmean[i][p] += avector[i] * t[i] * weight[k] / pitimesb;
                    Zmean[i] += cmatrix[i][i] * weight[k] / pitimesb;
                    for (j = 0; j < p; j++) {
                        Nmean[i][j] += T[i][j] * cmatrix[j][i] * weight[k] / pitimesb;
                    }
                }
                dt = obs[k + 1] - obs[k];
            }
        }

        if (NoOfCensored > 0) {
            for (i = 0; i < p; i++) {
                avector[i] = pi[i];
                bvector[i] = 1;
            }
            init_matrix(cmatrix, p, p);
            dt = censur[0];
            for (k = 0; k < NoOfCensored; k++) {
                if (dt > 0) {
                    rungekutta(p, avector, null, bvector, cmatrix, dt, h, T, ett, ka, null,
                            kb, kc);
                }
                pitimesb = 0;
                for (i = 0; i < p; i++) {
                    pitimesb += pi[i] * bvector[i];
                }
                for (i = 0; i < p; i++) {
                    Bmean[i] += pi[i] * bvector[i] * cweight[k] / pitimesb;
                    Zmean[i] += cmatrix[i][i] * cweight[k] / pitimesb;
                    for (j = 0; j < p; j++) {
                        Nmean[i][j] += T[i][j] * cmatrix[j][i] * cweight[k] / pitimesb;
                    }
                }
                dt = censur[k + 1] - censur[k];
            }
        }

        if (NoOfInt > 0) {
            for (i = 0; i < p; i++) {
                avector[i] = pi[i];
                bvector[i] = 1;
            }
            init_vector(gvector, p);
            init_matrix(cmatrix, p, p);
            dt = lower[0];
            if (dt > 0) {
                rungekutta(p, avector, gvector, bvector, cmatrix, dt, h, T, ett, ka, kg,
                        kb, kc);
            }
            for (i = 0; i < p; i++) {
                a_left[0][i] = avector[i];
                g_left[0][i] = gvector[i];
                b_left[0][i] = bvector[i];
                for (j = 0; j < p; j++) {
                    c_left[0][i][j] = cmatrix[i][j];
                }
            }
            m = 1;
            prev = lower[0];
            v = 1;
            for (k = 0; k < NoOfInt; k++) {

                while ((v < NoOfInt) && (lower[v] < upper[k])) {
                    dt = lower[v] - prev;
                    if (dt > 0) {
                        rungekutta(p, avector, gvector, bvector, cmatrix, dt, h, T, ett, ka,
                                kg, kb, kc);
                    }
                    if (lower[v] > lower[v - 1]) {
                        for (i = 0; i < p; i++) {
                            a_left[m][i] = avector[i];
                            g_left[m][i] = gvector[i];
                            b_left[m][i] = bvector[i];
                            for (j = 0; j < p; j++) {
                                c_left[m][i][j] = cmatrix[i][j];
                            }
                        }
                        m++;
                    }
                    prev = lower[v];
                    v++;
                }
                dt = upper[k] - prev;
                if (dt > 0) {
                    rungekutta(p, avector, gvector, bvector, cmatrix, dt, h, T, ett, ka, kg, kb, kc);
                }
                mate = partner[k];

                adiffsum = 0;
                for (i = 0; i < p; i++) {
                    adiffsum += a_left[mate][i] - avector[i];
                }
                for (i = 0; i < p; i++) {
                    Bmean[i] += pi[i] * (b_left[mate][i] - bvector[i]) * intweight[k]
                            / adiffsum;
                    Nmean[i][p] += t[i] * (gvector[i] - g_left[mate][i]) * intweight[k]
                            / adiffsum;
                    Zmean[i] += (gvector[i] - g_left[mate][i] + c_left[mate][i][i]
                            - cmatrix[i][i]) * intweight[k] / adiffsum;
                    for (j = 0; j < p; j++) {
                        Nmean[i][j] += T[i][j] * (gvector[i] - g_left[mate][i]
                                + c_left[mate][j][i] - cmatrix[j][i])
                                * intweight[k] / adiffsum;
                    }
                }

                prev = upper[k];

            }
        }

        for (i = 0; i < p; i++) {
            pi[i] = Bmean[i] / (SumOfCensored + SumOfWeights + SumOfInt);
            if (pi[i] < 0) {
                pi[i] = 0;
            }
            t[i] = Nmean[i][p] / Zmean[i];
            if (t[i] < 0) {
                t[i] = 0;
            }
            T[i][i] = -t[i];
            for (j = 0; j < p; j++) {
                if (i != j) {
                    T[i][j] = Nmean[i][j] / Zmean[i];
                    if (T[i][j] < 0) {
                        T[i][j] = 0;
                    }
                    T[i][i] -= T[i][j];
                }
            }
        }
    }
    void compute_loglikelihood(double h, int p, double[] pi, double[][] T, double[] t,
            int stepindicator, double[] avector, double[][] ka,
            double[][] a_left) {
        int i, j, k, n, m, v, mate;
        double loglikelihood, asum, atimesexit, dt, adiffsum, prev;

        if (stepindicator == 1) {
            h = set_steplength(p, T);
        }
        loglikelihood = 0;

        if (NoOfObs > 0) {
            for (i = 0; i < p; i++) {
                avector[i] = pi[i];
            }
            dt = obs[0];
            for (i = 0; i < NoOfObs; i++) {
                atimesexit = 0.0;
                if (dt > 0) {
                    a_rungekutta(p, avector, ka, dt, h, T);
                }
                for (j = 0; j < p; j++) {
                    atimesexit += avector[j] * t[j];
                }
                loglikelihood += weight[i] * log(atimesexit);
                dt = obs[i + 1] - obs[i];
            }
        }

        if (NoOfCensored > 0) {
            for (i = 0; i < p; i++) {
                avector[i] = pi[i];
            }
            dt = censur[0];
            for (i = 0; i < NoOfCensored; i++) {
                asum = 0.0;
                if (dt > 0) {
                    a_rungekutta(p, avector, ka, dt, h, T);
                }
                for (j = 0; j < p; j++) {
                    asum += avector[j];
                }
                loglikelihood += cweight[i] * log(asum);
                dt = censur[i + 1] - censur[i];
            }
        }

        if (NoOfInt > 0) {
            for (i = 0; i < p; i++) {
                avector[i] = pi[i];
            }
            dt = lower[0];
            if (dt > 0) {
                a_rungekutta(p, avector, ka, dt, h, T);
            }
            for (i = 0; i < p; i++) {
                a_left[0][i] = avector[i];
            }
            m = 1;
            prev = lower[0];
            v = 1;
            for (k = 0; k < NoOfInt; k++) {
                while ((v < NoOfInt) && (lower[v] < upper[k])) {
                    dt = lower[v] - prev;
                    if (dt > 0) {
                        a_rungekutta(p, avector, ka, dt, h, T);
                    }
                    if (lower[v] > lower[v - 1]) {
                        for (i = 0; i < p; i++) {
                            a_left[m][i] = avector[i];
                        }
                        m++;
                    }
                    prev = lower[v];
                    v++;
                }

                dt = upper[k] - prev;
                if (dt > 0) {
                    a_rungekutta(p, avector, ka, dt, h, T);
                }
                mate = partner[k];

                adiffsum = 0;
                for (i = 0; i < p; i++) {
                    adiffsum += a_left[mate][i] - avector[i];
                }
                loglikelihood += intweight[k] * log(adiffsum);
                prev = upper[k];
            }
        }
        System.out.print("log(likelihood) = " + loglikelihood + " ");
        
        double mse = 0.0;
        double max = 0.0;
        for(i = 0; i < intweight.length; ++i) {
            double x = (lower[i] + upper[i])/2.0;
            double y = intweight[i];
            double yApprox = getDensity(x);
            double diff = Math.abs(yApprox - y);
            mse += diff*diff;
            if(max < diff)
                max = diff;
        }
        mse = mse/intweight.length;
        System.out.println("mse = " + mse  + " max = " + max);
    }
    void SavePhases(int p, double[] pi, double[][] T) {
        int i, j;
        FileOutputStream utfil2;
        try {
            utfil2 = new FileOutputStream("phases.txt");
            PrintWriter out = new PrintWriter(utfil2);
            for (i = 0; i < p; i++) {
                out.print("\n" + pi[i] + "   ");
                for (j = 0; j < p; j++) {
                    out.print(T[i][j] + " ");
                }
            }
            out.println();
            if (out.checkError()) {
                throw new IOException("error writing stream");
            }
            utfil2.close();
        } catch (IOException e) {
            throw new RuntimeException("could not write phases file: "
                    + e.toString());
        }
    }
    public double[][] getMatrix() {
        double[][] out = new double[phasesNum + 1][phasesNum + 1];
        for(int row = 0; row < phasesNum + 1; ++row) {
            if(row < phasesNum)
                for(int col = 0; col < phasesNum + 1; ++col) {
                    if(col < phasesNum)
                        out[row][col] = T[row][col];
                    else
                        out[row][col] = t[row];
                }
            else {
                for(int col = 0; col < phasesNum; ++col)
                    out[row][col] = 0;
                out[row][phasesNum] = 1;
            }
        }
        return out;
    }
    void EMiterate(int NoOfEMsteps, int p, double[] pi, double[][] T, double[] t,
            double[] gvector, double[] avector, double[] bvector,
            double[][] cmatrix, double[] Bmean, double[] Zmean, double[][] Nmean,
            double[][] kg, double[][] ka, double[][] kb, double[][][] kc,
            double[] ett, double[][] g_left, double[][] a_left,
            double[][] b_left, double[][][] c_left) {
        int k, stepindicator, stepchoice;
        double RKstep;

        stepindicator = 1;

        for (k = 1; k <= NoOfEMsteps; k++) {
            if (stepindicator == 1) {
                RKstep = set_steplength(p, T);
            } else
                throw new RuntimeException("unknown step");
            if (NoOfInt == 0) {
                EMstep(p, RKstep, pi, T, t, null, avector, bvector, cmatrix, Bmean,
                        Zmean, Nmean, null, ka, kb, kc, ett, null, null, null, null);
            } else {
                EMstep(p, RKstep, pi, T, t, gvector, avector, bvector, cmatrix, Bmean,
                        Zmean, Nmean, kg, ka, kb, kc, ett, g_left, a_left, b_left,
                        c_left);
            }

            if ((k < 6) || ((k % 25) == 0 || k == NoOfEMsteps)) {
                System.out.println("\nEM(" + k + ")");
                show_pi_T(p, pi, T, t);
                if(SAVE_PHASES)
                    SavePhases(p, pi, T);
                if (NoOfInt == 0) {
                    compute_loglikelihood(RKstep, p, pi, T, t, stepindicator, avector, ka,
                            null);
                } else {
                    compute_loglikelihood(RKstep, p, pi, T, t, stepindicator, avector, ka,
                            a_left);
                }
            }
        }
    }
    int search_partner() {
        int i, max;

        max = partner[0];
        for (i = 1; i < NoOfInt; i++) {
            if (partner[i] > max) {
                max = partner[i];
            }
        }

        return max + 1;
    }
    public void fit(Xxy[] samples, PHType phType, int phases, int iterations) {
        int i, j, sampletype, fitting, choice, newp;
        int[] pilegal;
        int[][] Tlegal;
        double[] avector, gvector, bvector;
        double[][] cmatrix;
        double[] Bmean, Zmean;
        double[][] Nmean, ka, kg, kb;
        double[][][] kc;
        double[][] a_left, g_left, b_left;
        double[][][] c_left;
        double[] ett;

        sampletype = 2;
        fitting = 1;
        NoOfObs = 0;
        NoOfCensored = 0;
        NoOfInt = 0;
        SumOfWeights = 0;
        SumOfCensored = 0;
        SumOfInt = 0;

        AskForInput(samples);
        System.out.println("\nNumber of phases of the PH-distribution to be fitted, (p): " + phases);

        pi = new double[phases];
        T = new double[phases][phases];
        t = new double[phases];
        pilegal = new int[phases];
        Tlegal = new int[phases][phases];
        avector = new double[phases];
        bvector = new double[phases];
        cmatrix = new double[phases][phases];
        Bmean = new double[phases];
        Zmean = new double[phases];
        Nmean = new double[phases][phases + 1];
        ka = new double[4][phases];
        kb = new double[4][phases];
        kc = new double[4][phases][phases];
        if (sampletype == 2) {
            ett = new double[phases];
            for (i = 0; i < phases; i++) {
                ett[i] = 1;
            }
        } else {
            ett = null;
        }
        if (NoOfInt > 0) {
            int NoToSave = search_partner();
            gvector = new double[phases];
            kg = new double[4][phases];
            a_left = new double[NoToSave][phases];
            g_left = new double[NoToSave][phases];
            b_left = new double[NoToSave][phases];
            c_left = new double[NoToSave][phases][phases];
        } else {
            gvector = null;
            kg = null;
            a_left = null;
            g_left = null;
            b_left = null;
            c_left = null;
        }

        selectstructure(phType, phases, pi, T, t, pilegal, Tlegal);
        show_pi_T(phases, pi, T, t);

        EMiterate(iterations, phases, pi, T, t, gvector, avector, bvector, cmatrix,
                Bmean, Zmean, Nmean, kg, ka, kb, kc, ett, g_left, a_left, b_left, c_left);
        if(SAVE_PHASES)
            SavePhases(phases, pi, T);
    }
    public static void main(String[] args) throws IOException {
        if (args.length != 4) {
            System.out.println("<density file> <phases> <iterations> <output file>");
        } else {
            String inFileName = args[0];
            int phases = Integer.parseInt(args[1]);
            int iterations = Integer.parseInt(args[2]);
            String outFileName = args[3];
            FileInputStream in = new FileInputStream(inFileName);
            FileOutputStream out = new FileOutputStream(outFileName);
            double[][] array = NumStream.toArray(in);
            int num = array.length;
            Xxy[] data = new Xxy[num];
            for(int row = 0; row < num; ++row) {
                double[] sample = array[row];
                if(sample.length != 3)
                    throw new RuntimeException("expected <min> <max> <density>");
                data[row] = new Xxy(sample[0], sample[1], sample[2]);
            }
            AbstractDist dist = new OlssonEM(data, PHType.COX, phases, iterations);
            out.close();
            in.close();
        }
    }
}
