/*
 * OsogamiEC.java
 *
 * Created on Apr 4, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

/**
 * A phase--type distribution as described in Osogami, Harchol-Balter
 * "Closed form solutions for mapping general distributions to
 * quasi-minimal PH distributions", 2006.
 * 
 * @author Artur Rataj
 */
public class OsogamiEC extends AbstractDist {
    /**
     * Expectation, that is, the 1st raw moment.
     */
    protected final double mean;
    /**
     * Second raw moment.
     */
    protected final double raw2;
    /**
     * Third raw moment.
     */
    protected final double raw3;
    /**
     * Variance.
     */
    protected final double variance;
    /**
     * Skewness.
     */
    protected final double skewness;
    /**
     * EC coefficient.
     */
    double p;
    /**
     * EC coefficient.
     */
    int n;
    /**
     * EC coefficient.
     */
    double  lambda_y;
    /**
     * EC coefficient.
     */
    double px;
    /**
     * EC coefficient.
     */
    double  lambda_x1;
    /**
     * EC coefficient.
     */
    double  lambda_x2;

    /**
     * Creates a new instance of <code>OsogamiEC</code>, from
     * raw moments.
     * 
     * @param mean mean, i.e. the 1st raw moment
     * @param raw2 2nd raw moment
     * @param raw3 3rd raw moment
     */
    public OsogamiEC(double mean, double raw2, double raw3) {
        this.mean = mean;
        this.raw2 = raw2;
        this.raw3 = raw3;
        variance = this.raw2 - Math.pow(this.mean, 2.0);
        skewness = (this.raw3 - 3.0*this.mean*this.raw2 + 2.0*Math.pow(this.mean, 3.0))
                /Math.pow(variance, 3.0/2.0);
        computeECParameters();
    }
    /**
     * Computes parameters of a Erlang-Coxian distribution, so that it
     * matches the moments.
     */
    private void computeECParameters() {
        final double EPSILON = 1e-6;
        double m2 = raw2/(mean*mean);
        double m3 = raw3/(mean*raw2);
        double t0 = 2.0*m2 - 1.0;
        double t1 = 1.0/(m2 - 1.0);
        if(m3 < t0)
            p = 1.0/(2*m2 - m3);
        else if(t1 == Math.round(t1)) {
            double m22 = m2*m2;
            p = (m22 + 2*m2 - 1.0)/(2*m22);
        } else
            p = 1;
        double muw1 = mean/p;
        double mw2 = p*m2;
        double mw3 = p*m3;
        if(mw3 == 2.0*mw2 - 1.0 && mw2 <= 2.0)
            n = (int)Math.ceil(mw2/(mw2 - 1.0)) - 1;
        else
            n = (int)Math.floor(mw2/(mw2 - 1.0)) + 1;
        if(n == 1) {
            lambda_x1 = 1.0/muw1;
            lambda_x2 = 0;
            px = 0;
            lambda_y = 0;
        } else {
            double mx2 = ((n - 3.0)*mw2 - (n - 2.0))/
                    ((n - 2.0)*mw2 - (n - 1.0));
            double mux1 = muw1/((n - 2.0)*mx2 - (n - 3.0));
            double alpha = (n - 2.0)*(mx2 - 1.0)*(n*(n - 1.0)*mx2*mx2 -
                    n*(2.0*n - 5.0)*mx2 + (n - 1.0)*(n - 3.0));
            double beta = ((n - 1.0)*mx2 - (n - 2.0))*
                    Math.pow((n - 2.0)*mx2 - (n - 3.0), 2.0);
            double mx3 = (beta*mw3 - alpha)/mx2;
            double u;
            double v;
            double t2 = 3.0*mx2 - 2.0*mx3;
            if(Math.abs(t2) < EPSILON) {
                u = 1.0;
                v = 0.0;
            } else {
                u = (6.0 - 2.0*mx3)/t2;
                v = (12 - 6.0*mx2)/(mx2*t2);
            }
            double t3 = 1.0/(2.0*mux1);
            lambda_x1 = (u + Math.sqrt(u*u - 4.0*v))*t3;
            lambda_x2 = (u - Math.sqrt(u*u - 4.0*v))*t3;
            px = lambda_x2*(lambda_x1*mux1 - 1.0)/lambda_x1;
            lambda_y = 1.0/((mx2 - 1.0)*mux1);
        }
    }
    @Override
    public double next() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override
    public double getDensity(double x) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override
    public double getMoment(int num) {
        switch(num) {
            case 1:
                return mean;
                
            case 2:
                return variance;

            case 3:
                return skewness;
                
            default:
                throw new RuntimeException("invalid moment number");
        }
    }
    /**
     * Returns the EC parameter p.
     * 
     * @return p
     */
    public double getP() {
        return p;
    }
    /**
     * Returns the EC parameter n.
     * 
     * @return n
     */
    public int getN() {
        return n;
    }
    /**
     * Returns the EC parameter lambda_y.
     * 
     * @return lambda_y
     */
    public double getLambdaY() {
        return lambda_y;
    }
    /**
     * Returns the EC parameter px.
     * 
     * @return px
     */
    public double getPX() {
        return px;
    }
    /**
     * Returns the EC parameter lambda_x1.
     * 
     * @return lambda_x1
     */
    public double getLambdaX1() {
        return lambda_x1;
    }
    /**
     * Returns the EC parameter lambda_x2.
     * 
     * @return lambda_x2
     */
    public double getLambdaX2() {
        return lambda_x2;
    }
    @Override
    public String toString() {
        return "n = " + n  +
                " p = " + p + " lambda_y = " + lambda_y +
                " px = " + px + " lambda_x1 = " + lambda_x1 +
                " lambda_x2 = " + lambda_x2;
    }
}
