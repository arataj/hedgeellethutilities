/*
 * GeneralPTDist.java
 *
 * Created on Aug 6, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import java.util.*;

import org.jblas.*;

/**
 * A general phase--type distribution.
 * 
 * @author Artur Rataj
 */
public class GeneralPTDist extends AbstractDist {
    /**
     * Initial probability vector.
     */
    public double[] pi;
    /**
     * Phase--type generator; indexed row, column.
     */
    public double[][] T;
    /**
      * Exit--rate vector
      */
    public double[] t;
    /**
     * Number of phases.
     */
    int phasesNum;

    /**
     * Creates a new general phase--type distribution, with no fields
     * initialised.
     */
    public GeneralPTDist() {
        /* empty */
    }
    /**
     * Creates a new general phase--type distribution, given a canonical
     * vector of initial probabilities and a canonical transition matrix.
     * 
     * @param pi initial probabilities; including the absorbing state,
     * whose probability must be zero; deeply copied
     * @param transitions transitions, including the exit vector in the last column,
     * and the absorbing state row, which must be all zeroes; indexed
     * row, column; deeply copied
     */
    public GeneralPTDist(double[] pi, double[][] transitions) {
        phasesNum = transitions.length - 1;
        this.pi = new double[phasesNum];
        for(int i = 0; i < phasesNum; ++i)
            this.pi[i] = pi[i];
        if(pi[phasesNum] != 0)
            throw new RuntimeException("initial probability of the absorbing state " +
                    "must be zero");
        T = new double[phasesNum][phasesNum];
        for(int i = 0; i < phasesNum; ++i)
            for(int j = 0; j < phasesNum; ++j)
                T[i][j] = transitions[i][j];
        t = new double[phasesNum];
        for(int i = 0; i < phasesNum; ++i)
            t[i] = transitions[i][phasesNum];
        for(int i = 0; i < phasesNum; ++i)
            if(transitions[phasesNum][i] != 0)
                throw new RuntimeException("transitions from the absorbing state " +
                        "must be zero");
    }
    @Override
    public double next() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public double getDensity(double x) {
        // double[][] sT = new double[phasesNum][phasesNum];
        DoubleMatrix m = new DoubleMatrix(T);
        m = m.mul(x);
        m = MatrixFunctions.expm(m);
        double[][] expTx = m.toArray2();
        double[] aExpTx = new double[phasesNum];
        for(int col = 0; col < phasesNum; ++col)
            for(int row = 0; row < phasesNum; ++row)
                aExpTx[col] += pi[row]*expTx[row][col];
        double density = 0.0;
        for(int col = 0; col < phasesNum; ++col)
            density += aExpTx[col]*t[col];
        return density;
    }
    @Override
    public double getMoment(int num) {
        double moment;
        switch(num) {
            // expected value
            case 1:
            {
                DoubleMatrix m = new DoubleMatrix(T);
                //DoubleMatrix inv = MatrixFunctions.pow(m, -1);
                DoubleMatrix eye = DoubleMatrix.eye(phasesNum);
                DoubleMatrix one = DoubleMatrix.ones(phasesNum);
                DoubleMatrix inv = Solve.solve(m, eye);
                DoubleMatrix alpha = new DoubleMatrix(pi).transpose();
                
                // -alpha * S^-1 * 1
                DoubleMatrix tmp = alpha.mul(-1);
                tmp.mmuli(inv);
                DoubleMatrix out = tmp.mmul(one);

                moment = out.get(0);
                break;
            }
            // variance
            case 2:
            {
                DoubleMatrix m = new DoubleMatrix(T);
                DoubleMatrix eye = DoubleMatrix.eye(phasesNum);
                DoubleMatrix one = DoubleMatrix.ones(phasesNum);
                DoubleMatrix inv = Solve.solve(m, eye);
                DoubleMatrix inv2 = inv.mmul(inv);
                DoubleMatrix alpha = new DoubleMatrix(pi).transpose();
                
                // 2 alpha * S^-2 * 1
                DoubleMatrix tmp = alpha.mul(2);
                tmp.mmuli(inv2);
                DoubleMatrix comp1 = tmp.mmul(one);

                // alpha * S^-1 * 1
                tmp = alpha; // alpha no more needed
                tmp.mmuli(inv);
                tmp = tmp.mmul(one);
                DoubleMatrix comp2 = MatrixFunctions.pow(tmp, 2.0);
                
                moment = comp1.get(0) - comp2.get(0);
                break;
            }
            // skewness
            case 3:
            {
                throw new RuntimeException("skewness is unsupported");
            }
            default:
                throw new RuntimeException("unknown moment");
        }
        return moment;
    }
}
