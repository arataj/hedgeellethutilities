/*
 * OlssonEM.java
 *
 * Created on May 22, 2012
 *
 * Original C code (c) Maria Olsson. Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import java.io.*;
import static java.lang.Math.*;

import org.jblas.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;
import pl.gliwice.iitis.hedgeelleth.math.Xxy;

/**
 * This is a wrapper to the application HyperStar, as described in
 * P. Reinecke, T. Krauß and K. Wolter, "Cluster-based fitting of phase-type
 * distributions to empirical data", Computers & Mathematics with
 * Applications 64(12), 2012.<br>
 * 
 * @author Artur Rataj
 */
public class HyperStar extends GeneralPTDist {
    /**
     * Creates a phase--type distribution of a given type, that fits to given
     * probability density.
     * 
     * @param density density to fit to
     * @param samplesNum to how many samples convert the density;
     * the samples are generated at random
     */
    public HyperStar(Xxy[] density, int samplesNum) {
        ArrayDist ad = new ArrayDist(density);
        for(int s = 0; s < samplesNum; ++s) {
            double x = ad.next();
            
        }
    }
}
