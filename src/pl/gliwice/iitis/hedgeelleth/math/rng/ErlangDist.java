/*
 * ErlangDist.java
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import java.util.Random;
import org.apache.commons.math3.distribution.GammaDistribution;
import org.apache.commons.math3.special.Gamma;
import org.apache.commons.math3.util.ArithmeticUtils;
import org.apache.commons.math3.util.MathUtils;

/**
 * Erlang distribution.
 * 
 * @author Artur Rataj
 */
public class ErlangDist extends AbstractDist {
    /**
     * Phases = k = shape.
     */
    protected final int phases;
    /**
     * Rate.
     */
    protected final double lambda;
    /**
     * Expectation.
     */
    protected final double mean;
    /**
     * Variance.
     */
    protected final double variance;
    /**
     * A generator of random numbers.
     */
    static Random random = new Random();
//    /**
//     * A helper coefficient for computing density.
//     */
//    private double densityMult;
    protected GammaDistribution gamma;
    
    /**
     * Creates a new instance of <code>ExpDist</code>.
     * 
     * @param lambda rate
     * @param sigma standard deviation
     */
    public ErlangDist(int phases, double lambda) {
        this.phases = phases;
        this.lambda = lambda;
        mean = this.phases/this.lambda;
        variance = this.phases/(this.lambda*this.lambda);
        gamma = new GammaDistribution(phases, 1.0);
    }
    @Override
    public double next() {
        return gamma.sample();
    }
    @Override
    public double getDensity(double x) {
        if(x < 0)
            return 0.0;
        else {
//            double chunk = lambda*x/phases;
//            double densityMult = lambda*Math.exp(-chunk);
//            for(int i = 1; i < phases; ++i) {
//                densityMult *= lambda*x/i*Math.exp(-chunk);
//            }
//            return densityMult;
            return gamma.density(x);
        }
    }
    @Override
    public double getMoment(int num) {
        switch(num) {
            case 1:
                return mean;
                
            case 2:
                return variance;

            case 3:
                return 2.0;
                
            default:
                throw new RuntimeException("invalid moment number");
        }
    }
    /**
     * Returns the rate.
     * 
     * @return rate
     */
    public double getLambda() {
        return lambda;
    }
}
