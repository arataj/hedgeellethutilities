/*
 * ArrayDist.java
 *
 * Created on Apr 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

import java.nio.DoubleBuffer;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.math.*;

/**
 * A probability distribution, represented by an array of probability
 * masses.
 * 
 * @author Artur Rataj
 */
public class ArrayDist extends AbstractDist implements Cloneable {
    /**
     * Number of the moments to cache.
     */
    final int CACHED_MOMENTS_NUM = 3;
    
    /**
     * Probability mass, xx is an interval, y is a density.
     */
    public Xxy[] mass;
    /**
     * Cache of the first <code>CACHED_MOMENTS_NUM</code> moments.
     */
    protected double[] momentCache;
    /**
     * A ladder for computing <code>next()</code> -- probabilities.
     */
    private double[] ladderPr;
    /**
     * A ladder for computing <code>next()</code> -- indices.
     */
    private int[] ladderIndex;
    /**
     * Minimum value, for which this distribution assigns a mass.
     */
    protected double domainMin;
    /**
     * Maximum value, for which this distribution assigns a mass.
     */
    protected double domainMax;
    /**
     * A buffer that contains the normalisation multiplier, if
     * <code>ArrayDist(double[][], double, double)</code>
     * was used. Null otherwise.
     */
    public DoubleBuffer NORMALISATION_MULT;
    /**
     * If the mass is expected to be normalised. If false, unknown.
     */
    protected boolean normalised;
    
    /**
     * <p>Creates a new instance of <code>ArrayDist</code>. 
     * If sections overlap, a runtime exception is thrown.</p>
     * 
     * @param mass probability mass, xx is an interval, y is a density; shallow copy
     * @param normalised if true, the mass must be normalised with a high precision,
     * e.g. using <code>normalise()</code>
     */
    public ArrayDist(Xxy[] mass, boolean normalised) {
        init(mass, normalised);
    }
    /**
     * <p>Creates a new instance of <code>ArrayDist</code>, normalises
     * the mass. If sections overlap, a runtime exception is thrown.
     * This is a convenience constructor.</p>
     * 
     * @param mass probability mass, xx is an interval, y is a density; shallow copy
     */
    public ArrayDist(Xxy[] mass) {
        init(mass, true);
    }
    /**
     * <p>Creates a new instance of <code>ArrayDist</code>,
     * by copying data from a generic array.</p>
     * 
     * <p>The deeply copied data is normalised by this constructor.
     * The normalization multiplier is stored in
     * <code>NORMALISATION_MULT</code>.</p>
     * 
     * @param tab tabularised mass [sample][0 == x, 1 == mass],
     * deeply copied by this constructor
     * @param leftDiff xMin = min(x) - leftDiff, 0 auto
     * @param rightDiff xMax = max(x) + rightDiff, 0 auto
     */
    public ArrayDist(double[][] tab, double leftDiff, double rightDiff) {
        NORMALISATION_MULT = DoubleBuffer.allocate(1);
        init(copyMass(tab, leftDiff, rightDiff, true, NORMALISATION_MULT),
                true);
    }
    /**
     * A common initialisation.
     * 
     * @param mass probability mass
     * @param if the mass is expected to be normalised
     */
    private void init(Xxy[] mass, boolean normalised) {
        this.mass =  mass;
        this.normalised = normalised;
        momentCache = new double[CACHED_MOMENTS_NUM];
        for(int i = 0; i < CACHED_MOMENTS_NUM; ++i)
            momentCache[i] = Double.NaN;
        findLimits();
        cacheNext();
    }
    /**
     * Creates data from an array (xMid, mass) or
     * (xMix, xMax, mass) (if three columns) to a mass array.<br>
     * 
     * The deeply copied data is normalised. If sections overlap,
     * a runtime exception is thrown.
     * 
     * @param tab tabularised mass [sample][0 == x mid, 1 == mass],
     * deeply copied by this method; must be ordered
     * @param leftDiff xMin = x - leftDiff, 0 auto
     * @param rightDiff xMax = x + rightDiff, 0 auto
     * @param normalisationMult if not null, the normalisation multiplier is
     * stored in it
     */
    public static Xxy[] copyMass(double[][] tab, double leftDiff, double rightDiff,
            boolean normalise, DoubleBuffer normalisationMult) {
        // normally should be 0
        final int EXTRA = 0;
        if(tab[0].length == 2 && (leftDiff < 0 || rightDiff < 0))
            throw new RuntimeException("diff required but negative");
        else if(tab[0].length == 3 && (leftDiff >= 0 || rightDiff >= 0))
            throw new RuntimeException("diff not needed but positive");
        double EPSILON = 1e-6*(leftDiff < 0 ? 1 : (rightDiff + leftDiff));
        if(EPSILON == 0)
            EPSILON = 1e-8;
        int barsNum = tab.length;
        boolean auto = leftDiff == 0;
        Xxy[] out = new Xxy[barsNum + EXTRA];
        double prevXRight;
        if(!auto)
            prevXRight = Double.NaN;
        else if(tab.length >= 2)
            prevXRight = tab[0][0] - (tab[1][0] - tab[0][0])/2;
        else
            throw new RuntimeException("can not determine left diff");
        int offset = 0;
        for(int bar = 0; bar < barsNum; ++bar) {
            double xLeft;
            double xRight;
            double v;
            if(tab[bar].length == 2) {
                double x = tab[bar][0];
                if(auto) {
                    leftDiff = x - prevXRight;
                    if(bar == tab.length - 1)
                        rightDiff = leftDiff;
                    else
                        rightDiff = (tab[bar + 1][0] - tab[bar][0])/2;
                }
                xLeft = x - leftDiff;
                xRight = x + rightDiff;
                v = tab[bar][1];
            } else if(tab[bar].length == 3) {
                xLeft = tab[bar][0];
                xRight = tab[bar][1];
                v = tab[bar][2];
            } else
                throw new RuntimeException("unrecognised number of columns");
            out[bar + offset] = new Xxy(xLeft, xRight, v);
            while(bar + offset < EXTRA) {
                ++offset;
                out[bar + offset] = new Xxy(xLeft, xRight, v);
            }
            if(!Double.isNaN(prevXRight)) {
                double overlap = prevXRight - xLeft;
                if(overlap > EPSILON)
                    throw new RuntimeException("overlap");
                else if(overlap > 0)
                    // correct small overlaps
                    xRight -= overlap;
            }
            prevXRight = xRight;
        }
        if(normalise) {
            double mult = normalise(out);
            if(normalisationMult != null)
                normalisationMult.put(mult);
        }
        return out;
    }
    /**
     * Normalises a mass.
     * 
     * @param mass mass to normalise
     * @return normalisation multiplier
     */
    public static double normalise(Xxy[] mass) {
        double prSum = 0.0;
        for(int i = 0; i < mass.length; ++i) {
            Xxy p = mass[i];
            prSum += p.xDiff*p.y;
        }
        double mult = 1.0/prSum;
        for(int i = 0; i < mass.length; ++i)
            mass[i].y *= mult;
        return mult;
    }
    /**
     * Sorts samples NOT.
     * 
     * @param samples samples defined as in <code>ArrayDist(double[][], double, double)</code>
     * @return sorted samples
     */
    public static double[][] sort(double[][] samples) {
        return samples;
    }
    /**
     * Finds <code>domainMin</code> and <code>domainMax</code>.
     */
    private void findLimits() {
        domainMin = Double.MAX_VALUE;
        domainMax = -Double.MAX_VALUE;
        for(Xxy interval : mass) {
            if(domainMin > interval.xL)
                domainMin = interval.xL;
            if(domainMax < interval.xR)
                domainMax = interval.xR;
        }
    }
    /**
     * Caches data for fast computing of <code>next()</code>.
     */
    private void cacheNext() {
        final double EPSILON = 1e-6;
        SortedMap<Double, Set<Integer>>  sorted = new TreeMap<>();
        Xxy prevP = null;
        for(int i = 0; i < mass.length; ++i) {
            Xxy p = mass[i];
            double pr = -p.xDiff*p.y;
            Set<Integer> set = sorted.get(pr);
            if(set == null) {
                set = new HashSet<>();
                sorted.put(pr, set);
            }
            set.add(i);
            if(prevP != null && prevP.xR >= p.xL + EPSILON)
                throw new RuntimeException("mass not ordered");
            prevP = p;
        }
        ladderPr = new double[mass.length];
        ladderIndex = new int[mass.length];
        Iterator<Set<Integer>> it = sorted.values().iterator();
        double prRest = 1.0;
        int count = 0;
        for(double pr : sorted.keySet()) {
            pr *= -1;
            Set<Integer> set = it.next();
            for(int i : set) {
                ladderPr[count] = (count == mass.length - 1 ? 1.0 : pr/prRest);
                prRest -= pr;
                ladderIndex[count] = i;
                ++count;
            }
        }
        if(normalised && Math.abs(prRest) > EPSILON)
            throw new RuntimeException("mass not normalised");
    }
    /**
     * Returns the minimum value, for which the mass is specified.
     * 
     * @return a minimum value
     */
    public double getDomainMin() {
        return domainMin;
    }
    /**
     * Returns the maximum value, for which the mass is specified.
     * 
     * @return a maximum value
     */
    public double getDomainMax() {
        return domainMax;
    }
    /**
     * Draws an index of the mass array, according to this distribution.
     * 
     * @return a random value 0 .. mass.length - 1
     */
    public int nextIndex() {
        final int N = mass.length - 1;
        int count = 0;
        for(int i = 0; i < N; ++i) {
            if(Math.random() < ladderPr[count])
                return ladderIndex[count];
            ++count;
        }
        return ladderIndex[count];
    }
    @Override
    public double next() {
        Xxy p = mass[nextIndex()];
        return p.xL + Math.random()*p.xDiff;
    }
    @Override
    public double getDensity(double x) {
        int index = mass.length/2;
        int step = index;
        int prevIndex = -1;
        int prevPrevIndex = -1;
        while(true) {
            if(index < 0 || index >= mass.length)
                throw new IllegalArgumentException("out of range");
            Xxy p = mass[index];
            if(x < p.xL) {
                step = Math.max(1, step/2);
                index -= Math.max(0, step);
            } else if(x > p.xR) {
                step = Math.max(1, step/2);
                index += Math.min(mass.length - 1, step);
            } else
                return p.y;
            if(index == prevPrevIndex)
                // in a hole beside density bars
                return 0;
            prevPrevIndex = prevIndex;
            prevIndex = index;
        }
    }
    @Override
    public double getMoment(int num) {
        if(num > CACHED_MOMENTS_NUM ||
                Double.isNaN(momentCache[num - 1])) {
            double m = 0;
            switch(num) {
                // expected value
                case 1:
                    for(int i = 0; i < mass.length; ++i) {
                        Xxy p = mass[i];
                        m += p.xMean*p.xDiff*p.y;
                    }
                    break;
                // variance
                case 2:
                // skewness
                case 3:
                    double mean = getMean();
                    for(int i = 0; i < mass.length; ++i) {
                        Xxy p = mass[i];
                        double diff = p.xMean - mean;
                        if(num == 2)
                            diff = diff*diff;
                        else
                            diff = diff*diff*diff;
                        m += diff*p.xDiff*p.y;
                    }
                    if(num == 3) {
                        double v = getSigma();
                        // standarize
                        m = m/(v*v*v);
                    }
                    break;
                    
                default:
                    throw new RuntimeException("invalid moment number");
            }
            if(num <= CACHED_MOMENTS_NUM)
                momentCache[num - 1] = m;
            return m;
        } else
            return momentCache[num - 1];
    }
    /**
     * Returns <i>n</i>th raw moment.
     * 
     * @param num number of the raw moment
     * @return a raw moment
     */
    public double getRawMoment(int num) {
        if(num == 1)
            // faster
            return getMoment(1);
        else {
            double m = 0.0;
            for(int i = 0; i < mass.length; ++i) {
                Xxy p = mass[i];
                m += Math.pow(p.xMean, num)*p.xDiff*p.y;
            }
            return m;
        }
    }
    @Override
    public ArrayDist clone() {
        try {
            ArrayDist copy = (ArrayDist)super.clone();
            Xxy[] massCopy = new Xxy[mass.length];
            for(int i = 0; i < massCopy.length; ++i)
                massCopy[i] = new Xxy(mass[i]);
            copy.init(massCopy, normalised);
            return copy;
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Tests this class using a normal distribution.
     * 
     * @param args 
     */
    public static void main(String[] args) {
        final double MEAN = 1.234;
        final double SIGMA = 5.678;
        AbstractDist n = new NormalDist(MEAN, SIGMA);
        double xMin = MEAN - SIGMA*4;
        double xMax = MEAN + SIGMA*4;
        final int RES = 100;
        final int OVERSAMPLING = 10;
        ArrayDist s = new SampledDist(n, xMin, xMax,
                RES, OVERSAMPLING);
        System.out.println("mean = " + s.getMean() +
                ", sigma = " + s.getSigma() +
                ", skewness = " + s.getSkewness());
        AbstractDist w = new WeibullDist(1.0, 3.0);
        s = new SampledDist(w, 0, 3,
                RES, OVERSAMPLING);
        for(double i = 0.0; i < 2.5; i += 0.01)
            System.out.println(i + "\t" + s.getDensity(i));
        AbstractDist o = new OsogamiEC(
                s.getRawMoment(1), s.getRawMoment(2), s.getRawMoment(3));
        System.out.println("w/s/ec mean = " + w.getMean() + "/" + s.getMean() + "/" + o.getMean() +
                ", variance = " + w.getVariance() + "/" + s.getVariance() + "/" + o.getVariance() +
                ", skewness = " + w.getSkewness() + "/" + s.getSkewness() + "/" + o.getSkewness());
        System.out.println("ec: " + o.toString());
    }
}
