/*
 * ICPSampling.java
 *
 * Created on Apr 4, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.rng;

/**
 * Inverse transform sampling, used by some distributions.
 * 
 * @author Artur Rataj
 */
public class ICPSampling {
    /**
     * Returns a sample for a distribution with defined
     * <code>inverseCumulativePr</code>.
     * 
     * @param dist distribution to sample from; must imlement 
     * <code>inverseCumulativePr</code>
     * @return a random sample
     */
    public static double next(AbstractDist dist) {
        double v = dist.inverseCumulativePr(Math.random());
        if(Double.isNaN(v))
            throw new RuntimeException("distribution does not implement " +
                    "inverse cumulative probability");
        return v;
    }
}
