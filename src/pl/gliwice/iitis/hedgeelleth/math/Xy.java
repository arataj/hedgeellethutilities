/*
 * Xy.java
 *
 * Created on Apr 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math;

/**
 * A point on an R^2 plane.
 * 
 * @author Artur Rataj
 */
public class Xy {
    /**
     * X coordinate.
     */
    final public double x;
    /**
     * Y coordinate.
     */
    final public double y;
    
    /**
     * Creates a new instance of <code>Xy</code>.
     * 
     * @param x x coordinate
     * @param y y coordinate
     */
    public Xy(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
