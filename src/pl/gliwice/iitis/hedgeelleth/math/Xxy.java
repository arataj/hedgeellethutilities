/*
 * Xy.java
 *
 * Created on Apr 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math;

/**
 * Two points on an R^2 plane, the same y coordinate.
 * 
 * @author Artur Rataj
 */
public class Xxy {
    /**
     * Lower x coordinate.
     */
    final public double xL;
    /**
     * Higher x coordinate.
     */
    final public double xR;
    /**
     * Cached middle x coordinate = (xL + xR)/2.
     */
    final public double xMean;
    /**
     * Cached x coordinate difference = (xR - xL).
     */
    final public double xDiff;
    /**
     * Y coordinate, not final to allow normalisation.
     */
    public double y;
    
    /**
     * Creates a new instance of <code>Xy</code>.
     * 
     * @param xL lower x coordinate
     * @param xR higher x coordinate
     * @param y y coordinate
     */
    public Xxy(double xL, double xR, double y) {
        this.xL = xL;
        this.xR = xR;
        this.y = y;
        xMean = (this.xL + this.xR)/2.0;
        xDiff = this.xR  - this.xL;
    }
    /**
     * A copying constructor.
     * 
     * @param xxy the original to depth copy
     */
    public Xxy(Xxy xxy) {
        this(xxy.xL, xxy.xR, xxy.y);
    }
    @Override
    public String toString() {
        return "(" + xL + " ... " + xR + "; " + y + ")";
    }
}
