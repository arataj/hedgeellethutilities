/*
 * Optimizer.java
 *
 * Created on 2 janv. 2018
 *
 * Copyright (c) 2018 Amira Kamli.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math;

import java.util.*;

import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.exception.*;
import org.apache.commons.math3.optim.*;
import org.apache.commons.math3.optim.nonlinear.scalar.*;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.*;
import org.apache.commons.math3.random.Well19937c;
import pl.gliwice.iitis.hedgeelleth.math.BFGSOptimizer;

/**
 *
 * @author Amira Kamli
 */

public class Optimizer {
    /**
     * Optimisation constants.
     */
    public enum Type {
        SIMPLEX_NELDER_MEAD,
        SIMPLEX_MULTIDIRECTIONAL,
        BOBYQA,
        CMAES,
        BFGS,
    };
    public static class Result {
       public double[] bestPoint;
       public double bestQuality;
       
       public Result(double[] bestPoint, double bestQuality){
            this.bestPoint = bestPoint;
            this.bestQuality = bestQuality;
       }
    };
    /**
     * 
     * @param type type of optimisation
     * @param numRestarts number of restarts, 0 for a single run
     * @param spaceDescr description of the optimisation space
     * @param goalFunction goal function
     * @param goalType if to minimize or maximize the goal function
     * @param convRel convergence is reached if the relative difference between
     * point values does not exceed this value; negative for abs convergence 
     * @param convAbs convergence is reached if the absolute difference between
     * point values does not exceed this value; negative for rel convergence 
     * @return the best point and the best goalFunction found
     */
    public static Result optimize(Type type, int numRestarts,
            AbstractOptSpaceDescr spaceDescr, MultivariateFunction goalFunction,
        GoalType goalType, AbstractConvergenceChecker convergenceChecker) {       
        int numCoeff = spaceDescr.initial.length; 
        double[] bestMf = null;
        double bestQ;
        switch(goalType) {
            case MINIMIZE:
                bestQ = Double.MAX_VALUE;
                break;

            case MAXIMIZE:
                bestQ = -Double.MAX_VALUE;
                break;

            default:
                throw new RuntimeException("unknown goal type");
        }
        
        for(int restart = 0; restart < Math.max(1, numRestarts); ++restart) { 
            double[] mf = new double[numCoeff];
            MultivariateOptimizer optimizer;
            String optimiserClass;
            List<OptimizationData> optData = new LinkedList<>();
            // default
            int MAX_ITER = (int)Math.max(10000,
                    Math.round(60000*Math.log((1 + numCoeff)/4.0)));
            InitialGuess initialGuess = new InitialGuess(spaceDescr.initial);
            switch(type) {
                case SIMPLEX_NELDER_MEAD:
                    optimizer = new SimplexOptimizer(convergenceChecker);
                    optData.add(new NelderMeadSimplex(spaceDescr.initialStep));
                    optimiserClass = "Nelder-Mead simplex";
                    break;

                case SIMPLEX_MULTIDIRECTIONAL:
                    optimizer = new SimplexOptimizer(convergenceChecker);
                    optData.add(new MultiDirectionalSimplex(spaceDescr.initialStep));
                    optimiserClass = "multidirectional simplex";
                    break;

                case BOBYQA:
                    optimizer = new BOBYQAOptimizer(numCoeff*2);
                    optData.add(new SimpleBounds(spaceDescr.lower, spaceDescr.upper));
                    optimiserClass = "BOBYQA";
                    break;

                case CMAES:
                    optimizer = new CMAESOptimizer(MAX_ITER, 0, true, 0, 10,
                           new Well19937c(), false,
                           convergenceChecker);
                    optData.add(new SimpleBounds(spaceDescr.lower, spaceDescr.upper));
                    double[] range = new double[numCoeff];
                    for(int i = 0; i < numCoeff; ++i)
                        range[i] = (spaceDescr.upper[i] - spaceDescr.lower[i])/3.0;
                    OptimizationData sigma = new CMAESOptimizer.Sigma(range);
                    OptimizationData popSize = new CMAESOptimizer.PopulationSize((int)(4 + Math.floor(
                            3*Math.log(initialGuess.getInitialGuess().length))));
                    optData.add(sigma);
                    optData.add(popSize);
                    optimiserClass = "CMAES";
                    break;

                case BFGS:
                    optimizer = new BFGSOptimizer();
                    optimiserClass = "BGFSO";
                    throw new RuntimeException("BFGS unimplemented");
                    //break;

                default:
                    throw new RuntimeException("unknown optimiser class");
            }     
            if(numRestarts != 0) {
    //            optimizer = new MultiStartMultivariateOptimizer((MultivariateOptimizer)optimizer,
    //                    OPTIONS.approxRestarts,
    //                    new UncorrelatedRandomVectorGenerator(mf.getNumCoeff(),
    //                            new UniformRandomGenerator(new Well19937c())));
                optimiserClass += " with " + numRestarts + " restarts";
            }
            if(restart == 0)
                System.out.println("number of coeff = " + numCoeff + ", " +
                        "using " + optimiserClass + ", initial step = " + spaceDescr.initialStep +
                        ", max iterations = " + MAX_ITER + "...");
            optData.add(new MaxEval(MAX_ITER));
            optData.add(new ObjectiveFunction(goalFunction));
            optData.add(goalType);
            optData.add(initialGuess);
            if(numRestarts != 0)
                System.out.print("\t#" + restart + ":");
            try {
                final PointValuePair optimum = optimizer.optimize(
                        optData.toArray(new OptimizationData[optData.size()]));
                mf = optimum.getPoint();
                double q = goalFunction.value(mf);
                boolean better;
                switch(goalType) {
                    case MINIMIZE:
                        better = bestQ > q;
                        break;
                        
                    case MAXIMIZE:
                        better = bestQ < q;
                        break;
                        
                    default:
                        throw new RuntimeException("unknown goal type");
                }
                if(better) {
                    bestMf = mf;
                    bestQ = q;
                }
            } catch(TooManyEvaluationsException e) {
                System.out.println("\too many iterations, aborted");
                continue;
            }
        }
        return new Result(bestMf, bestQ);
    }

}