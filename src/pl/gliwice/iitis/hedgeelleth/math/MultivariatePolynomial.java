/*
 * MultivariatePolynomial.java
 *
 * Created on Jan 11, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math;

import java.util.*;

/**
 * A multivariate polynomial. Coefficient order as in
 * 
 * 
 * @author Artur Rataj
 */
public class MultivariatePolynomial {
    /**
     * Format of the textual desciption of this function.
     */
    public static enum OutFormat {
        // plain
        PLAIN,
        // uses Java math functions
        JAVA,
        // order of coefficients MathUtil.findMultivariatePolyPowers()
        FIND_MULTIVARIATE_POLY_POWERS,
    };
    /**
     * Order.
     */
    public final int ORDER;
    /**
     * Number of variables.
     */
    public final int NUM_VAR;
    /**
     * For each coefficient (1st index, length <code>NUM_COEFF</code>), powers of
     * subsequent variables (2nd index, length <code>NUM_VAR</code>)
     */
    public final int[][] POWERS;
    /**
     * Coefficients.
     */
    public final double[] COEFF;
    /**
     * Number of coefficients.
     */
    public final int NUM_COEFF;
    
    public MultivariatePolynomial(int order, int numVar) {
        ORDER = order;
        NUM_VAR = numVar;
        POWERS = MathUtil.findMultivariatePolyPowers(ORDER, NUM_VAR);
        NUM_COEFF = POWERS.length;
        COEFF = new double[NUM_COEFF];
        
    }
    /**
     * Sets values of coefficients.
     * 
     * @param c new values, deeply copied
     */
    public void setCoeff(double[] c) {
        if(c.length != NUM_COEFF)
            throw new RuntimeException("invalid number of coefficients");
        for(int i = 0; i < NUM_COEFF; ++i)
            COEFF[i] = c[i];
    }
    /**
     * Computes the value of this polynomial.
     * 
     * @param variables variables; if no indices, in the natural order, otherwise
     * indices define the order
     * @param indices null or an array with indices to subsequent variables; an
     * element of -1 means ignore, in such a case the array's length must be larger
     * than the number of variables
     * @return value
     */
    public double getValue(int[] variables, int[] indices) {
        if(indices == null && variables.length != NUM_VAR)
            throw new RuntimeException("invalid length of the array of variables");
        double sum = 0.0;
        for(int cNum = 0; cNum < NUM_COEFF; ++cNum) {
            double mult = 1.0;
            int[] p = POWERS[cNum];
            int vCount = 0;
            for(int argIndex = 0; argIndex < NUM_VAR; ++argIndex) {
                int vIndex;
                if(indices != null)
                    vIndex = indices[argIndex];
                else
                    vIndex = argIndex;
                if(vIndex != -1) {
                    // a state variable
                    int power = p[vCount];
                    if(power > 0)
                        if(power == 1)
                            mult *= variables[vIndex];
                        else
                            mult *= Math.pow(variables[vIndex], power);
                    ++vCount;
                }
            }
            if(vCount != NUM_VAR)
                throw new RuntimeException("invalid array of indices");
            sum += COEFF[cNum]*mult;
        }
        return sum;
    }
    /**
     * Print coefficients in the default order, int the style of Java constant
     * array declaration. A helper method for <code>toString()</code>.
     * 
     * @return a textual description
     */
    protected String printCoefficientsArrayStyle() {
        StringBuilder out = new StringBuilder();
        out.append("{\n");
        boolean first = true;
        for(double v : COEFF) {
            if(first)
                first = false;
            else
                out.append(",\n");
            out.append('\t');
            out.append(v);
        }
        out.append("\n}\n");
        return out.toString();
    }
    /**
     * A textual representation of this polynomial.
     * 
     * @param variables see <code>getValue()</code>, but values replaced with
     * strings; if the array is null, values are substituted with a&lt;num&gt;
     * @param indices see <code>getValue()</code>
     * @param outFormat output format, for details see <code>OutFormat</code>
     * @return a textual representation
     */
    public String toString(List<String> variables, int[] indices, OutFormat outFormat) {
        StringBuilder out = new StringBuilder();
        switch(outFormat) {
            case PLAIN:
            case JAVA:
                boolean javaStyle = outFormat == OutFormat.JAVA;
                boolean first = true;
                for(int cNum = 0; cNum < NUM_COEFF; ++cNum) {
                    double mult = COEFF[cNum];
                    if(mult >= 0) {
                        if(!first)
                            out.append(" + ");
                    } else {
                        mult = Math.abs(mult);
                        if(first)
                            out.append("-");
                        else
                            out.append(" - ");
                    }
                    out.append(mult);
                    int[] p = POWERS[cNum];
                    int vCount = 0;
                    for(int argIndex = 0; argIndex < NUM_VAR; ++argIndex) {
                        int vIndex;
                        if(indices != null)
                            vIndex = indices[argIndex];
                        else
                            vIndex = argIndex;
                        if(vIndex != -1) {
                            // a state variable
                            int power = p[vCount];
                            if(power > 0) {
                                if(javaStyle) {
                                    if(power == 1)
                                        out.append("*" + variables.get(vIndex));
                                    else
                                        out.append("*Math.pow(" + variables.get(vIndex) + "," + power + ")");
                                } else {
                                    out.append("*" + variables.get(vIndex));
                                    if(power > 1)
                                       out.append("^" + power);
                                }
                            }
                            ++vCount;
                        }
                    }
                    first = false;
                }
                break;
                
            case FIND_MULTIVARIATE_POLY_POWERS:
                out.append(printCoefficientsArrayStyle());
                break;
                
            default:
                throw new RuntimeException("unknown output format");
        }
        return out.toString();
    }
    public static void main(String[] args) {
        double[] coeff = {
	112.25523748914551,
	389.6048014378894,
	416.126043854807,
	232.62046477380207,
	211.89521702741325,
	107.94644966367203,
	-109.74393432372005,
	66.49244004492141,
	22.711672814940627,
	-51.962684299928966,
	312.8376111555192,
	114.80098393542362, 
	247.43870337402294,
	38.21411730992004,
	78.87853742524426,
	-1592.2473879945194,
	-384.06123315094624,
	-475.45961192568603,
	-19.311196926104518,
	-499.43262335814626,
	634.9476254783709,
	2416.076237216391,
	-386.5886241576488,
	-179.92316167880264,
	103.73903124206396,
	9.252434668704877,
	-73.68999098810599,
	-67.216083770741
        };
        final int N = 6;
        MultivariatePolynomial poly = new MultivariatePolynomial(2, N);
        MultivariatePolynomial anti = new MultivariatePolynomial(2, N);
        poly.setCoeff(coeff);
        int[] state = new int[N];
        double[] ffeoc = new double[coeff.length];
        for(int i = 0; i < 1000000; ++i) {
            double r = -Math.random();
            for(int j = 0; j < coeff.length; ++j)
                ffeoc[j] = r*coeff[j];
            anti.setCoeff(ffeoc);
            for(int j = 0; j < N; ++j)
                state[j] = (int)Math.round(Math.random()*10 - 5);
            double p = poly.getValue(state, null);
            double a = anti.getValue(state, null);
            if(Math.abs(p*r - a) > 1e-9)
                System.out.println("r=" + r + "\tp=" + p + "\ta=" + a);
        }
    }
}
