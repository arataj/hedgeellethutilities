/*
 * MatrixOp.java
 *
 * Created on Dec 9, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.math.matrix.solve;

import org.jblas.DoubleMatrix;

/**
 *
 * @author Artur Rataj
 */
public class MatrixOp {
    public static DoubleMatrix gaussSeidel(DoubleMatrix a, DoubleMatrix b) {
        double[] start = new double[b.getRows()];
        double[] xArray = Matrices.gaussSeidel(a.toArray2(), b.toArray(), start);
        return new DoubleMatrix(b.getRows(), 1, xArray);
    }
}
