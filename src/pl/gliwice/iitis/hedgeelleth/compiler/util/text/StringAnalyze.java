/*
 * StringAnalyze.java
 *
 * Created on Nov 15, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.text;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * Utility methods, that parse a string.
 * 
 * @author Artur Rataj
 */
public class StringAnalyze {
    /**
     * Returns, if a string is enclosed in matching parentheses.
     * 
     * @param s string to analyze
     * @param allowMismatch if parentheses do not match, then:
     * (i) if this parameter is true, return false, (ii) if this parameter is
     * false, throw a runtime exception
     * @return if enclosed in matching parentheses
     */
    public static boolean inParentheses(String s, boolean allowMismatch) {
        boolean enclosed = !s.isEmpty();
        int nest = 0;
        for(int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            switch(c) {
                case '(':
                    ++nest;
                    break;

                case ')':
                    --nest;
                    break;
                    
            }
            if(nest <= 0) {
                if(!allowMismatch && nest < 0)
                    throw new RuntimeException("in \"" + s + "\": unmatched `)'");
                // top level is allowable only before the
                // first character (never tested by this
                // condition) or after the final character
                if(i < s.length() - 1 ||
                        // if the top level is at this final character,
                        // and not only after it
                        c != ')') {
                    if(allowMismatch)
                        return false;
                    else
                        // need to check for matching
                        enclosed = false;
                }
            }
        }
        if(!allowMismatch && nest != 0)
            throw new RuntimeException("in \"" + s + "\": missing `)'");
        return enclosed && nest == 0;
    }
    /**
     * Returns, if a string a negation of an expression in
     * matching parentheses
     * 
     * @param s string to analyze
     * @param allowMismatch if parentheses do not match, then:
     * (i) if this parameter is true, return false, (ii) if this parameter is
     * false, throw a runtime exception
     * @return if negated parenthesized expression
     */
    public static boolean negatedInParentheses(String s, boolean allowMismatch) {
        if(s.length() <= 3 || s.charAt(0) != '!')
            return false;
        else
            return inParentheses(s.substring(1), allowMismatch);
    }
    private static void test(String s) {
        System.out.print("test `" + s + "': ");
        try {
            System.out.println(inParentheses(s, false));
        } catch(RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }
    /**
     * Returns if a string is a number.
     * 
     * @param s string to test
     * @return if a number
     */
    public static boolean isNumber(String s) {
        ParsePosition pos = new ParsePosition(0);
        NumberFormat.getInstance(Locale.ROOT).parse(s, pos);
        return pos.getIndex() == s.length();
    }
    /**
     * If <code>inParentheses(s)</code> returns false, and
     * trimmed <code>s</code> contains non--alphanumeric
     * characters that are not underscored, but <code>s</code>
     * is also not a number, then enclose in parentheses. If
     * <code>s</code> contains unmatched parentheses, a
     * runtime exception is thrown.
     * 
     * @param s string to conditionally enclose in `(' ')'
     * @return a string possibly enclosed in `(' ')'
     */
    public static String parenthesize(String s) {
        if(!isNumber(s.trim()) && !ToAlphanumeric.is(s.trim(), true) &&
                !inParentheses(s, false) && !negatedInParentheses(s, false))
            return "(" + s + ")";
        else
            return s;
    }
    /**
     * Removes leading and trailing empty lines, returns the result.
     * 
     * @param in input string
     * @return a string, without leading and trailing empty lines; if not empty,
     * then ends with a newline
     */
    public static String trimEmptyLines(String in) {
        StringBuilder out = new StringBuilder();
        Scanner sc = CompilerUtils.newScanner(in);
        StringBuilder buffer = new StringBuilder();
        boolean prefix = true;
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            boolean emptyLine = line.trim().isEmpty();
            if(!emptyLine)
                prefix = false;
            if(!prefix) {
                buffer.append(line);
                buffer.append('\n');
            }
            if(!emptyLine) {
                out.append(buffer);
                buffer = new StringBuilder();
            }
        }
        return out.toString();
    }
    /**
     * Replaces each whitespace sequence with a single space.
     * 
     * @param in input string
     * @param checkQuotes  if to throws a parse exception in the case of a
     * runaway double quote.
     * @param map if not null, indexed with output string's position,
     * returns <code>in</code>'s line + column pair, a contracted whitespace
     * refers to its beginning; the array, if not null, should have a sufficient
     * size, typically <code>in.size()*2</code> for safety
     * @return processed string
     */
    public static String compactWhitespace(String in, boolean checkQuotes,
            int[] map) throws ParseException {
        StringBuilder out = new StringBuilder();
        int line = 0;
        int col = 0;
        boolean white = false;
        boolean quotes = false;
        for(int i = 0; i < in.length(); ++i) {
            char c = in.charAt(i);
            if(Character.isWhitespace(c) && !quotes) {
                if(white)
                    ;
                else {
                    white = true;
                    if(map != null) {
                        map[out.length()*2 + 0] = line;
                        map[out.length()*2 + 1] = col;
                    }
                    out.append(" ");
                }
            } else {
                white = false;
                if(c == '"')
                    quotes = !quotes;
                if(checkQuotes && c == '\n' && quotes)
                    throw new ParseException(new StreamPos(null, line, col),
                            ParseException.Code.PARSE, "runaway quoted string");
                if(map != null) {
                    map[out.length()*2 + 0] = line;
                    map[out.length()*2 + 1] = col;
                }
                out.append(c);
            }
            if(c == '\n') {
                ++line;
                col = 0;
            } else
                ++col;
        }
        return out.toString();
    }
    /**
     * Removes all substrings having given left and right delimiters,
     * including these delimiters. No nesting of the dewlimiter pairs
     * allowed.
     * 
     * @param in input string
     * @param left left delimiter
     * @param right right dleimiter
     * @return output string
     */
    public static String removeWithin(String in, String left, String right) {
        StringBuilder out = new StringBuilder();
        int pos = 0;
        int prevPos = 0;
        while((pos = in.indexOf(left, pos)) != -1) {
            out.append(in.substring(prevPos, pos));
            pos = in.indexOf(right, pos);
            if(pos == -1)
                throw new RuntimeException("unexpected");
            pos += right.length();
            prevPos = pos;
        }
        out.append(in.substring(prevPos, in.length()));
        return out.toString();
    }
    /**
     * Return file extension.
     * 
     * @param fileName file or path
     * @param last if true, all after the last dot is returned; if false, all
     * after the first dot is returned
     * @return extension without the leading dot or null if no extension
     */
    public static String getExtension(String fileName, boolean last) {
        int pos;
        if(last)
            pos = fileName.lastIndexOf(".");
        else
            pos = fileName.indexOf(".");
        if(pos == -1 || pos == fileName.length() - 1)
            throw null;
        return fileName.substring(pos + 1);
    }
    /**
     * Replaces an existing extension with another.
     * 
     * @param fileName file name
     * @param newExtension new extension
     * @return a file name with the new extension
     */
    public static String replaceExtension(String fileName, String newExtension)
            throws IOException {
        int pos = fileName.lastIndexOf(".");
        if(pos == -1 || pos == fileName.length() - 1)
            throw new IOException("extension not found in " + fileName + "'");
        return fileName.substring(0, pos + 1) + newExtension;
    }
    /**
     * Removes all whitespace suffixed to a string.
     * 
     * @param in input string
     * @return a string which does not end with a whitespace
     */
    public static String removeTrailingSpace(String in) {
        for(int pos = in.length() - 1; pos >= 0; --pos)
            if(!Character.isWhitespace(in.charAt(pos)))
                return in.substring(0, pos + 1);
        return "";
    }
    public static void main(String[] args) {
        test("");
        test("a");
        test("(a)");
        test("(a");
        test("a)");
        test("(a)(b)");
        test("(a(a))");
        test("a(b)");
        test("(a(a)a");
        test("a(a(a))a)");
        test("(a)b");
        test("((a)((b)(c)))");
    }
}
