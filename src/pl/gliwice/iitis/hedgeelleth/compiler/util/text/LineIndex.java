/*
 * LineIndex.java
 *
 * Created on Nov 18, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.text;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An index of line positions. Indexes a string, and then can be used for
 * a fast look-up of line or column number of a given position in the
 * string.
 * 
 * @author Artur Rataj
 */
public class LineIndex {
    /**
     * Index, key is position of a first character in a line, value is the
     * line's number starting at 0.
     */
    NavigableMap<Integer, Integer> indexLine;
    /**
     * Index, key is line number starting at 0, value is the position of a first
     * character in that line.
     */
    NavigableMap<Integer, Integer> indexPos;
    /**
     * Length og the indexed string.
     */
    int length;
    
    /**
     * Creates a new instance of LineIndex. 
     * 
     * @param indexed                   string to index
     */
    public LineIndex(String indexed) {
        indexLine = new TreeMap<>();
        indexPos = new TreeMap<>();
        int pos = 0;
        int line = 0;
        indexLine.put(pos, line);
        indexPos.put(line, pos);
        while(pos < indexed.length()) {
            pos = indexed.indexOf('\n', pos);
            if(pos == -1)
                break;
            ++pos;
            ++line;
            indexLine.put(pos, line);
            indexPos.put(line, pos);
        }
        length = indexed.length();
    }
    /**
     * Returns the line of a character at a given position.
     * 
     * @param pos                       position in the indexed string
     * @return                          line number, starting at 0
     */
    public int getLine(int pos) {
        return indexLine.get(indexLine.floorKey(pos));
    }
    /**
     * Returns the column of a character at a given position.
     * 
     * @param pos                       position in the indexed string
     * @return                          column number, starting at 0
     */
    public int getColumn(int pos) {
        return pos - indexLine.floorKey(pos);
    }
    /**
     * Returns the position of a character at a line/column.
     * 
     * @param line line, starting at 0
     * @param column column, starting at 0
     * @return index
     */
    public int getPos(int line, int column) {
// if(indexPos.get(line) == null)
//     line = line;
        return indexPos.get(line) + column;
    }
    /**
     * Returns the position of a character at a line/column. This is a convenience
     * method.
     * 
     * @param pos position
     */
    public int getPos(StreamPos pos) {
        return getPos(pos.line - 1, pos.column - 1);
    }
    /**
     * Returns a source position description.
     * 
     * @param streamName                name of the stream
     * @param pos                       position in the indexed string
     * @param firstLineNum              value to add to the line number,
     *                                  in a typical case it is 1
     * @param firstColumnNum            value to add to the column number,
     *                                  in a typical case it is 1
     */
    public StreamPos getDescription(String streamName, int pos,
            int firstLineNum, int firstColumnNum) {
        if(streamName == null)
            streamName = "";
        StreamPos p = new StreamPos(streamName);
        p.line = getLine(pos) + firstLineNum;
        p.column = getColumn(pos) + firstColumnNum;
        return p;
    }
    /**
     * Selects a line from an indexed string.
     * 
     * @param indexed a string indexed by this object
     * @param lineNum a line number to extract, starting at 0
     * @return a line selected from <code>indexed</p>
     */
    public String selectLine(String indexed, int lineNum) {
        int beg = getPos(lineNum, 0);
        int end;
        if(indexPos.containsKey(lineNum + 1))
            end = getPos(lineNum + 1, 0);
        else
            end = length;
        return indexed.substring(beg, end);
    }
}
