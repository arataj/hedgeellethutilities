/*
 * StringEscape.java
 *
 * Created on Nov 12, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.text;

/**
 * Processes escape characters into Unicode characters.
 * 
 * @author Artur Rataj
 */
public class StringEscape {
    /**
     * Two--character escape sequences.
     */
    private static final char[] ESCAPE = {
        't', '\t',
        'n', '\n',
        'r', '\r',
        'f', '\f',
        'b', '\b',
        '0', '\0',
        '\'', '\'',
        '"', '"',
        '\\', '\\',
    };

    /**
     * Return the next unicode character from a string, that can contain
     * Java--style escape sequences.
     * 
     * @param s string to analyse, beginning at a given position
     * @param pos reads current position from pos[0] and then writes
     * the next one there
     * @return the next unicode character, null for unknown escape sequence
     */
    public static Character next(String s, int[] pos) {
        int p = pos[0];
        if(s.length() < p + 1)
            return null;
        char c = 0;
        if(s.charAt(p) == '\\') {
            if(s.length() < p + 2)
                return null;
            if(s.charAt(p + 1) == 'u') {
                if(s.length() < p + 5)
                    return null;
                c = (char)Integer.parseInt(s.substring(p + 1, p + 5), 0x10);
                p = p + 5;
            } else {
                if(s.length() < p + 2)
                    return null;
                boolean found = false;
                for(int i = 0; i < ESCAPE.length/2; ++i)
                    if(s.charAt(p + 1) == ESCAPE[i*2 + 0]) {
                        c = ESCAPE[i*2 + 1];
                        p = p + 2;
                        found = true;
                        break;
                    }
                if(!found)
                    return null;
            }
        } else {
            c = s.charAt(p);
            p = p + 1;
        }
        pos[0] = p;
        return c;
    }
    /**
     * Converts a string that containins Java--style escape sequences
     * into a plain unicode string.
     * 
     * @param s input string
     * @param error if null is returned, and this parameter is not null, then
     * then error[0] contains an index of the invalid escape sequence
     * @return a string, null for an invalid escape sequence
     */
    public static String convert(String s, int[] error) {
        StringBuilder out = new StringBuilder();
        int[] pos = new int[1];
        while(pos[0] < s.length()) {
            Character c = next(s, pos);
            if(c == null) {
                if(error != null)
                    error[0] = pos[0];
                return null;
            } else
                out.append(c);
        }
        return out.toString();
    }
}
