/*
 * NumStream.java
 *
 * Created on May 10, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.text;

import java.io.*;
import java.util.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;

/**
 * Conversion betwwen a stream with numerical values and a nested
 * array [row][colum]. Also converts such an array to a grayscale buffered
 * image.
 * 
 * @author Artur Rataj
 */
public class NumStream {
    /**
     * Converts numeric data from a text stream to a nested array
     * having coordinates [row][colum].
     * 
     * @param in input stream, is not closed by this method; number
     * of columns must be the same in each row
     * @return a nested array
     */
    @SuppressWarnings("empty-statement")
    public static double[][] toArray(InputStream in) {
        double[][] out;
        int cols = -1;
        int rows = 0;
        double[] aCol = null;
        List<double[]> lRow = new ArrayList<>();
        Scanner scLine = CompilerUtils.newScanner(in);
        while(scLine.hasNextLine()) {
            String line = scLine.nextLine();
            List<Double> l;
            if(cols == -1)
                l = new ArrayList<>();
            else {
                l = null;
                aCol = new double[cols];
            }
            int count = 0;
            final boolean FAST = true;
            if(FAST) {
                line = line.trim();
                if(!line.isEmpty()) {
                    line += " ";
                    int beg = 0;
                    for(int i = 0; i < line.length(); ++i)
                        if(Character.isWhitespace(line.charAt(i))) {
                            double d = NumParser.getDouble(line, beg, i);
                            if(l != null)
                                l.add(d);
                            else
                                aCol[count++] = d;
                            while(i < line.length() - 1 && Character.isWhitespace(line.charAt(i + 1)))
                                ++i;
                            beg = i + 1;
                        }
                }
            } else {
                Scanner scNumbers = CompilerUtils.newScanner(line);
                while(scNumbers.hasNextDouble()) {
                    double d = scNumbers.nextDouble();
                    if(l != null)
                        l.add(d);
                    else
                        aCol[count++] = d;
                }
            }
            if(count != 0 || (l != null && !l.isEmpty())) {
                if(cols == -1) {
                    cols = l.size();
                    aCol = new double[cols];
                    Iterator<Double> it = l.iterator();
                    for(int i = 0; i < cols; ++i)
                        aCol[i] = it.next();
                    count = cols;
                }
                if(count != cols)
                    throw new RuntimeException("expected " + cols + " columns, found " + count);
                lRow.add(aCol);
            }
        }
        if(cols == -1)
            throw new RuntimeException("data not found");
        return lRow.toArray(new double[lRow.size()][]);
    }
    /**
     * Converts numeric data from a nested array having coordinates [row][colum]
     * to a text stream.
     * 
     * @param in a nested array
     * @param out output stream, is not closed by this method; number
     * of columns can be different in each row
     */
    public static void fromArray(double[][] in, OutputStream out) throws IOException {
        PrintWriter writer = new PrintWriter(out);
        int rows = in.length;
        for(int r = 0; r < rows; ++r) {
            StringBuilder line = new StringBuilder();
            int cols = in[r].length;
            for(int c = 0; c < cols; ++c) {
                if(c != 0)
                    line.append("\t");
                line.append(in[r][c]);
            }
            line.append("\n");
            writer.print(line.toString());
        }
        if(writer.checkError())
            throw new IOException("error writing array file");
        writer.close();
    }
    /**
     * Converts a two--dimensional array of doubles to a grayscale image.
     * 
     * @param in array, indexed with y and x
     * @param maxValue an input value which is translated to maximum
     * vaule in the output image, i.e. 0xffff
     * @return 
     */
    public static BufferedImage toImage(double[][] in, double maxValue) {
        int H = in.length;
        int W;
        if(H > 0)
            W = in[0].length;
        else
            W = 0;
        BufferedImage out = new BufferedImage(W, H, BufferedImage.TYPE_USHORT_GRAY);
        WritableRaster raster = out.getRaster();
        int MAX_GRAY = 0xffff;
        for(int y = 0; y < H; ++y)
            for(int x = 0; x < W; ++x) {
                int v = Math.max(0, Math.min(MAX_GRAY,
                        (int)Math.round(in[y][x]*MAX_GRAY/maxValue)));
                raster.setSample(x, y, 0, v);
            }
        return out;
    }
}
