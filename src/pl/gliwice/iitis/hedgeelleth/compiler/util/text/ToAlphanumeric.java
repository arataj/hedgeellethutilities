/*
 * ToAlphanumeric.java
 *
 * Created on Oct 31, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.text;

/**
 * Converts a string to one containing only alphanumeric characters and
 * underscore. Preserves uniqueness.
 * 
 * @author Artur Rataj
 */
public class ToAlphanumeric {
    /**
     * Converts a string.
     *
     * @param in                        input name
     * @param underscoreAllowed         do not convert underscored
     * @return                          converted name
     */
    public static String get(String in, boolean underscoreAllowed) {
        StringBuilder out = new StringBuilder();
        for(int i = 0; i < in.length(); ++i) {
            char c = in.charAt(i);
            String s;
            switch(c) {
                case '_':
                    if(underscoreAllowed)
                        s = "_";
                    else
                        s = "__";
                    break;

                case '#':
                    s = "_H";
                    break;

                case '$':
                    s = "_S";
                    break;

                case '.':
                    s = "_D";
                    break;

                case ',':
                    s = "_C";
                    break;

                case '<':
                    s = "_L";
                    break;

                case '>':
                    s = "_G";
                    break;

                case '(':
                    s = "_l";
                    break;

                case ')':
                    s = "_r";
                    break;

                case '+':
                    s = "_a";
                    break;

                case '-':
                    s = "_s";
                    break;

                case '*':
                    s = "_m";
                    break;

                case '/':
                    s = "_d";
                    break;

                case '%':
                    s = "_o";
                    break;

                case '&':
                    s = "_A";
                    break;

                case '|':
                    s = "_O";
                    break;

                case '^':
                    s = "_x";
                    break;

                case ' ':
                    s = "_w";
                    break;

                default:
                    if(!Character.isLetterOrDigit(c)) {
                        String n = Integer.toHexString(c);
                        while(n.length() < 4)
                            n = "0" + n;
                        s = "_u" + n;
                    } else
                        s = "" + c;
                    break;
            }
            out.append(s);
        }
        return out.toString();
    }
    /**
     * If a given string is alphanumeric.
     * 
     * @param in string to test
     * @param underscoreAllowed treat an underscore as if it
     * were an alphanumeric character
     * @return if contains only alphanumeric characters,
     * and optionally underscored
     */
    public static boolean is(String in, boolean underscoreAllowed) {
        return in.equals(get(in, underscoreAllowed));
    }
}
