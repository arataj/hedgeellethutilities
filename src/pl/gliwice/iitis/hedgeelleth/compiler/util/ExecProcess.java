/*
 * ExecProcess.java
 *
 * Created on Jun 9, 2016
 *
 * Copyright (c) 2016  Artur Rataj. Contains code from
 * <code>http://stackoverflow.com/questions/14288185/detecting-windows-or-linux</code>.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.util.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * How to prefix a command, so that it runs even if it is a script.
 * 
 * @author Artur Rataj
 */
public abstract class ExecProcess {
    /**
     * If to redirect the error stream to the output stream
     */
    protected final boolean REDIRECT_ERROR;
    /**
     * Creates a new object.
     * 
     * @param redirectError if to redirect the error stream to the output stream
     */
    public ExecProcess(boolean redirectError) {
        REDIRECT_ERROR = redirectError;
    }
    /**
     * Runs a process. Override <code>readOut<code> to process its
     * output. If it returns normally, it means that the process' exit
     * code was 0.
     * 
     * @param cliArgs complete command line; the first element can be a binary or
     * a script
     */
    public void run(List<String> cliArgs) throws CompilerException {
        BufferedReader br = null;
        if(cliArgs.isEmpty())
            throw new CompilerException(null, CompilerException.Code.IO,
                    "no command to run");
        String command = cliArgs.get(0);
if(false) {
    StringBuilder message = new StringBuilder();
    for(String s : cliArgs) {
        message.append(s);
        message.append(" ");
    }
    message.toString();
    if(true) return;
}
        try {
            List<String> cliArgsCopy = new LinkedList<>(cliArgs);
            if(!cliArgsCopy.get(cliArgsCopy.size() - 1).equals(";"))
                cliArgsCopy.add(";");
            List<List<String>> series = new LinkedList<>();
            List<String> part = new LinkedList<>();
            for(String s : cliArgsCopy) {
                if(s.equals(";")) {
                    series.add(part);
                    part = new LinkedList<>();
                } else
                    part.add(s);
            }
            for(List<String> args : series) {
                ProcessBuilder builder = new ProcessBuilder(args);
                builder.redirectErrorStream(REDIRECT_ERROR);
                Process process = builder.start();
                Thread closeChildThread = new Thread() {
                    public void run() {
                        process.destroy();
//                        try {
//                            Thread.sleep(2000);
//                        } catch (InterruptedException ex) {
//                        }
//                        process.destroyForcibly();
                    }
                };
                Runtime.getRuntime().addShutdownHook(closeChildThread);                 
                InputStream is = process.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                try {
                    process.waitFor();
                } catch(InterruptedException e) {
                    throw new CompilerException(null, CompilerException.Code.IO,
                            command + " interrupted: " + e.getMessage());
                }
                if(process.exitValue() != 0) {
                    StringBuilder message = new StringBuilder();
                    for(String s : cliArgs) {
                        message.append(s);
                        message.append(" ");
                    }
                    message.append("did not execute properly:");
                    String line;
                    while((line = br.readLine()) != null) {
                        message.append("\n\t");
                        message.append(line);
                    }
                    throw new CompilerException(null, CompilerException.Code.IO,
                            message.toString());
                } else {
                    processOut(br);
                }
            }
        } catch(IOException e) {
            throw new CompilerException(null, CompilerException.Code.IO,
                    "can not run " + command + ": " + e.getMessage());
        } finally {
            if(br != null)
                try {
                    br.close();
                } catch(IOException e) {
                    throw new CompilerException(null, CompilerException.Code.IO,
                            "can not close " + command + "'s output: " + e.getMessage());
                }
        }
    }
    /**
     * An abstract method for processing the standard output.
     * 
     * @param br a reader, not closed by this method
     */
    abstract protected void processOut(BufferedReader br) throws CompilerException;
    private static String OS = System.getProperty("os.name").toLowerCase();
    static {
        // System.out.println("PATH = " + System.getenv("PATH"));
    }
    public static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }
    public static boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
    }
    public static boolean isSolaris() {
        return (OS.indexOf("sunos") >= 0);
    }
    public static boolean isMac() {
        return (OS.indexOf("mac") >= 0);
    }
    public static boolean useBash() {
        return isUnix() || isSolaris() || isMac();
    }
    public static boolean useCmd() {
        return isWindows();
    }
    final static String[] bash = { "bash", };
    final static String[] cmd = { "cmd.exe", "/C", };
    /**
     * Returns a prefix of a command.
     * 
     * @return array of strings which tells how to run a shell
     */
    public static List<String> getShellPrefix() {
        String[] a;
        if(useBash())
            a = bash;
        else if(useCmd())
            a = cmd;
        else
            throw new RuntimeException("can not run a command: unknown os id = " +
                    OS);
        return Arrays.asList(a);
    }
}
