/*
 * For.java
 *
 * Created on May 20, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.util.Collection;
import java.util.Iterator;

/**
 * <p>A foreach with a counter.</p>
 * 
 * <p>Based on an answer by James Scriven at
 * https://stackoverflow.com/questions/477550/is-there-a-way-to-access-an-iteration-counter-in-javas-for-each-loop
 * </p>
 * @author Artur Rataj
 */
public class For {
   @FunctionalInterface
   public interface LoopWithIndexConsumer<T> {
       void accept(T t, int i);
   }
   @FunctionalInterface
   public interface LoopWithIndexConsumerDouble {
       void accept(double t, int i);
   }
   @FunctionalInterface
   public interface LoopWithIndexConsumer2<T, R> {
       void accept(T t, R r, int i);
   }
   @FunctionalInterface
   public interface LoopWithIndexConsumerDouble2 {
       void accept(double t, double r, int i);
   }
   @FunctionalInterface
   public interface LoopWithIndexConsumer3<T, R, S> {
       void accept(T t, R r, S s, int i);
   }
   @FunctionalInterface
   public interface LoopWithIndexConsumerDouble3 {
       void accept(double t, double r, double s, int i);
   }
   @FunctionalInterface
   public interface LoopWithIndexConsumer4<T, R, S, U> {
       void accept(T t, R r, S s, U u, int i);
   }
   @FunctionalInterface
   public interface LoopWithIndexConsumerDouble4 {
       void accept(double t, double r, double s, double u, int i);
   }
   @FunctionalInterface
   public interface LoopWithIndexConsumerE<T, E extends Exception> {
       void accept(T t, int i) throws E;
   }
   public static <T> void each(Collection<T> collection,
            LoopWithIndexConsumer<T> consumer) {
      int index = 0;
      for (T object : collection) {
         consumer.accept(object, index++);
      }
   }
   public static void each(double[] array,
            LoopWithIndexConsumerDouble consumer) {
      int num = array.length;
      for(int index = 0; index < num; ++index)
         consumer.accept(array[index], index);
   }
   public static <T, R> void each(Collection<T> collection1, Collection<R> collection2,
            LoopWithIndexConsumer2<T, R> consumer) {
      int num = collection1.size();
      if(num != collection2.size())
          throw new RuntimeException("collections must be of the same size");
      Iterator<T> it1 = collection1.iterator();
      Iterator<R> it2 = collection2.iterator();
      for(int index = 0; index < num; ++index) {
         T o1 = it1.next();
         R o2 = it2.next();
         consumer.accept(o1, o2, index);
      }
   }
   public static void each(double[] array1, double[] array2,
            LoopWithIndexConsumerDouble2 consumer) {
      int num = array1.length;
      if(num != array2.length)
          throw new RuntimeException("arrays must be of the same size");
      for(int index = 0; index < num; ++index) {
         double d1 = array1[index];
         double d2 = array2[index];
         consumer.accept(d1, d2, index);
      }
   }
   public static <T, R, S> void each(Collection<T> collection1, Collection<R> collection2, Collection<S> collection3,
            LoopWithIndexConsumer3<T, R, S> consumer) {
      int num = collection1.size();
      if(num != collection2.size() || num != collection3.size())
          throw new RuntimeException("collections must be of the same size");
      Iterator<T> it1 = collection1.iterator();
      Iterator<R> it2 = collection2.iterator();
      Iterator<S> it3 = collection3.iterator();
      for(int index = 0; index < num; ++index) {
         T o1 = it1.next();
         R o2 = it2.next();
         S o3 = it3.next();
         consumer.accept(o1, o2, o3, index);
      }
   }
   public static void each(double[] array1, double[] array2, double[] array3,
            LoopWithIndexConsumerDouble3 consumer) {
      int num = array1.length;
      if(num != array2.length || num != array3.length)
          throw new RuntimeException("arrays must be of the same size");
      for(int index = 0; index < num; ++index) {
         double d1 = array1[index];
         double d2 = array2[index];
         double d3 = array3[index];
         consumer.accept(d1, d2, d3, index);
      }
   }
   public static <T, R, S, U> void each(Collection<T> collection1, Collection<R> collection2,
           Collection<S> collection3, Collection<U> collection4,
           LoopWithIndexConsumer4<T, R, S, U> consumer) {
      int num = collection1.size();
      if(num != collection2.size() || num != collection3.size() || num != collection4.size())
          throw new RuntimeException("collections must be of the same size");
      Iterator<T> it1 = collection1.iterator();
      Iterator<R> it2 = collection2.iterator();
      Iterator<S> it3 = collection3.iterator();
      Iterator<U> it4 = collection4.iterator();
      for(int index = 0; index < num; ++index) {
         T o1 = it1.next();
         R o2 = it2.next();
         S o3 = it3.next();
         U o4 = it4.next();
         consumer.accept(o1, o2, o3, o4, index);
      }
   }
   public static void each(double[] array1, double[] array2,
           double[] array3, double[] array4,
           LoopWithIndexConsumerDouble4 consumer) {
      int num = array1.length;
      if(num != array2.length || num != array3.length || num != array4.length)
          throw new RuntimeException("arrays must be of the same size");
      for(int index = 0; index < num; ++index) {
         double d1 = array1[index];
         double d2 = array2[index];
         double d3 = array3[index];
         double d4 = array4[index];
         consumer.accept(d1, d2, d3, d4, index);
      }
   }
   public static <T> void eachIO(Collection<T> collection,
            LoopWithIndexConsumerE<T, java.io.IOException> consumer) throws java.io.IOException {
      int index = 0;
      for (T object : collection) {
         consumer.accept(object, index++);
      }
   }
   @FunctionalInterface
   public interface LoopWithIndexAndSizeConsumer<T> {
       void accept(T t, int i, int n);
   }
   public static <T> void eachSize(Collection<T> collection,
            LoopWithIndexAndSizeConsumer<T> consumer) {
      int index = 0;
      for(T object : collection) {
         consumer.accept(object, index++, collection.size());
      }
   }
}
