/*
 * PSpace.java
 *
 * Created on Dec 2, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.util.*;

/**
 * Definition of a P space.
 * 
 * @author Artur Rataj
 */
public class PSpace {
    /**
     * Type constants.
     */
    public static enum Type {
        // standard probabilistic space
        PROB,
        // hybrid quantum 
        HQ,
    };
    /**
     * Type of this space.
     */
    public final Type TYPE;
    /**
     * Base vectors, "0" for a plain probabilistic space.
     */
    public final List<String> BV;
    
    /**
     * Creates a p-space.
     * 
     * @param type type of the space
     * @param baseVectors names of base vectors
     */
    public PSpace(Type type, List<String> baseVectors) {
        TYPE = type;
        BV = new LinkedList<>(baseVectors);
    }
    /**
     * Creates a plain probabilistic p-space. This is a convenience constructor.
     */
    public PSpace() {
        this(Type.PROB, getProbBV());
    }
    private static List<String> getProbBV() {
        List<String> out = new LinkedList<>();
        out.add("0");
        return out;
    }
    public boolean isProb() {
        return TYPE == Type.PROB;
    }
}
