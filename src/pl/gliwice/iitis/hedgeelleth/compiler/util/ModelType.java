/*
 * ModelType.java
 *
 * Created on Jun 11, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

/**
 * Types of a PTA system, all are discrete--time.
 */
public enum ModelType {
    /**
     * Markov chain.
     */
    DTMC,
    /**
     * Markov decision process.
     */
    MDP,
    /**
     * Probabilistic timed automaton, that is, a Markov decision process
     * with clocks.
     */
    PTA,
    /**
     * A game, that is, a Markov decision process with agents.
     */
    SMG,
    /**
     * A timed game, that is, a probabilistic timed automaton with agents.
     */
    TSMG;

    /**
     * Returns a header of a serialized PTA system, that identifies this
     * type.
     * 
     * @return name of this constant put to lower case
     */
    public String getHeader() {
        return name().toLowerCase();
    }
    /**
     * Reverse of <code>getHeader()</code>.
     * 
     * @param header a header string
     * @return type or null for unrecognized type
     */
    public static ModelType getType(String header) {
        for(ModelType t : ModelType.values())
            if(header.equals(t.getHeader()))
                return t;
        return null;
    }
    /**
     * If this type allows for definition of players.
     * 
     * @return if agents allowed
     */
    public boolean allowsPlayers() {
        switch(this) {
            case DTMC:
            case MDP:
            case PTA:
                return false;

            case SMG:
            case TSMG:
                return true;

            default:
                throw new RuntimeException("unknown type");
        }
    }
    /**
     * If this type allows for clock variables.
     * 
     * @return if clocks allowed
     */
    public boolean allowsClocks() {
        switch(this) {
            case DTMC:
            case MDP:
            case SMG:
                return false;

            case PTA:
            case TSMG:
                return true;

            default:
                throw new RuntimeException("unknown type");
        }
    }
}
