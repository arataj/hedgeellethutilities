/*
 * Utilities.java
 *
 * Created on May 6, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * Contains code from the JavaCC project and The Java Developers Almanac 1.4.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.util.*;
import java.io.*;

import com.esotericsoftware.wildcard.Paths;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * Various tools not directly connected with the translator.
 * 
 * @author Artur Rataj
 */
public class CompilerUtils {
    /**
     * Avoid instantation.
     */
    private CompilerUtils() {
        /* empty */
    }
    /**
     * Returns all files in a given directory structure, including
     * the subdirectories.
     *
     * @param directory                 top directory in the
     *                                  hierarchy
     * @param pattern                   pattern to filter the file names,
     *                                  exluding directory names
     * @return                          list of files
     */
    public static List<String> getAllFiles(File directory, Pattern pattern) throws IOException {
// System.out.println("** at dir = " + directory.toString());
        List<String> list = new LinkedList<>();
        if(!directory.exists())
            throw new IOException("file " + directory + " not found");
        if(!directory.isDirectory())
            throw new IOException(directory + " is not a directory");
        File[] files = directory.listFiles();
        for(int i = 0; i < files.length; ++i) {
            File sub = files[i];
            if(sub.isDirectory())
                list.addAll(getAllFiles(sub, pattern));
            else {
                String name = sub.getName();
                Matcher m = pattern.matcher(name);
// System.out.println("** found = " + name);
                if(m.matches())
                    list.add(sub.getAbsolutePath());
            }
        }
        return list;
    }
    /**
     * Returns the extension of a given file name.
     *
     * @param fileName                  file name
     * @param numParts                  number of dot--separated parts
     *                                  in the extension, typically 1
     * @return                          extracted extension, empty string
     *                                  if no extension was found, the
     *                                  extension does not include the
     *                                  leading dot
     */
    public static String getFileNameExtension(String fileName, int numParts) {
        int pos = -1;
        String s = fileName;
        while(numParts > 0) {
            pos = s.lastIndexOf('.');
            if(pos != -1)
                s = s.substring(0, pos);
            else
                break;
            --numParts;
        }
        if(pos != -1)
            return fileName.substring(pos + 1);
        else
            return "";
    }
    /**
     * Deletes recursively a directory.
     * 
     * @param dir                       directory to delete
     * @return                          if successfull
     */
    public static boolean deleteDirectoryRecursively(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDirectoryRecursively(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
    /**
     * Searches for paths with wilcards, expands them into
     * individual files found in the filesystem.
     * 
     * @param in input list, with paths possibly containing
     * wildcards
     * @return output list, no paths with wildcards; empty for
     * no files
     */
    public static List<String> expandFiles(List<String> in) {
        List<String> out = new LinkedList<>();
        for(String path : in) {
            Paths paths = new Paths();
            paths.glob(".", path);
            List<String> expanded = paths.getPaths();
            if(expanded.isEmpty())
                // to report an error
                out.add(path);
            else
                out.addAll(expanded);
        }
        return out;
    }
    /**
     * Returns a file as it is named in its directory, i.e. without
     * any possible preceeding path.
     * 
     * @param path path or name
     * @return name
     */
    public static String getFileNameFromPath(String path) {
        String n;
        int pos = path.lastIndexOf(File.separatorChar);
        if(pos == -1)
            n = path;
        else
            n = path.substring(pos + 1);
        return n;
    }
    /**
     * Returns an indented string. Adds a new line character after the
     * text, if there is not any.
     * 
     * @param indentLevel               indent level
     * @param s                         text to indent
     * @param indent                    indent unit, like for example
     *                                  four spaces
     * @return                          indented text
     */
    public static String indent(int indentLevel, String s, String indent) {
        String indentString = "";
        for(int i = 0; i < indentLevel; ++i)
            indentString += indent;
        String out = "";
        if(s.length() != 0 && s.charAt(s.length() - 1) != '\n')
            s += "\n";
        Scanner sc = newScanner(s).useDelimiter("\\n");
        while(sc.hasNext()) {
            out += indentString;
            out += sc.next() + "\n";
        }
        return out;
    }
    /**
     * Returns an indented string. Adds a new line character after the
     * text, if there is not any. Indent unit is two spaces.<br>
     * 
     * This is a convenience method.
     * 
     * @param indentLevel               indent level, unit is
     *                                  two space characters
     * @param s                         text to indent
     * @return                          indented text
     */
    public static String indent(int indentLevel, String s) {
        return indent(indentLevel, s, "  ");
    }
    /**
     * If there are newlines in the comment, then, after any newline,
     * an indent is replaced with a given one.
     * 
     * @param indentLevel               indent level, unit is
     *                                  two space characters
     * @param indentAsteriskMiscSpace   additional space added
     *                                  to the indent if this is
     *                                  a * or ** comment;
     *                                  null for the default
     *                                  of a single space
     * @param comment                   comment contents, with
     *                                  decorations
     * @param indent                    indent unit
     * @return                          comment contents with
     *                                  decorations and corrected
     *                                      
     */
    public static String correctCommentIndent(int indentLevel,
            String indentAsteriskMiscSpace, String comment,
            String indent) {
        String indentString = "";
        boolean asterisk = comment.startsWith("/*");
        if(asterisk) {
            if(indentAsteriskMiscSpace == null)
                indentAsteriskMiscSpace = " ";
            indentString = indentAsteriskMiscSpace;
        } else
            indentString = "";
        for(int i = 0; i < indentLevel; ++i)
            indentString += indent;
        String out = "";
        Scanner sc = newScanner(comment);
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            int pos = 0;
            while(Character.isWhitespace(line.charAt(pos)))
                ++pos;
            if(!out.isEmpty())
                line = indentString + line.substring(pos);
            out += line + "\n";
        }
        return out.trim();
    }
    /**
     * Prints a string to standard output, indenting it and
     * prefixing.
     * 
     * @param prefix                    prefix, added before
     *                                  the indentation
     * @param indentLevel               indent level, unit is
     *                                  two space characters
     * @param s                         text to indent
     */
    public static void log(String prefix, int indentLevel, String s) {
        if(s.charAt(s.length() - 1) == '\n')
            s += "\n";
        Scanner sc = newScanner(s).useDelimiter("\\n");
        while(sc.hasNext()) {
            System.out.print(prefix + " ");
            for(int i = 0; i < indentLevel; ++i)
                System.out.print("  ");
            System.out.println(sc.next());
        }
    }
  /**
   * Used to convert raw characters to their escaped version
   * when these raw version cannot be used as part of a
   * string or character literal.<br>
   *
   * Based on the code from JavaCC.
   *
   * @param str                         input string
   * @return                            processed string
   */
  public static String addEscapes(String in) {
      StringBuilder out = new StringBuilder();
      char ch;
      for(int i = 0; i < in.length(); i++) {
          switch (in.charAt(i)) {
              case 0:
                  continue;

              case '\b':
                  out.append("\\b");
                  continue;

              case '\t':
                  out.append("\\t");
                  continue;

              case '\n':
                  out.append("\\n");
                  continue;

              case '\f':
                  out.append("\\f");
                  continue;

              case '\r':
                  out.append("\\r");
                  continue;

              case '\"':
                  out.append("\\\"");
                  continue;

              case '\'':
                  out.append("\\\'");
                  continue;

              case '\\':
                  out.append("\\\\");
                  continue;

              default:
                  if((ch = in.charAt(i)) < 0x20 || ch > 0x7e) {
                      String s = "0000" + Integer.toString(ch, 16);
                      out.append("\\u" + s.substring(s.length() - 4, s.length()));
                  } else {
                      out.append(ch);
                  }
                  continue;

          }
      }
      return out.toString();
   }
    /**
     * Puts backslashes before any occurence of either a single or a
     * double quote, or a backslash.
     *
     * @param in                        input string
     * @param single                    true if to escape single quotes,
     *                                  false if to escape double quotes
     * @param map if not null, indexed with an output string position,
     * returns the respective input string position
     * @return                          processed string
     */
    public static String escapeQuotesSlashes(String in, boolean single,
            List<Integer> map) {
        if(map != null)
            map.clear();
        StringBuilder out = new StringBuilder();
        for(int i = 0; i < in.length(); ++i) {
            char c = in.charAt(i);
            String tC;
            switch(c) {
                case '\'':
                    if(single)
                        tC = "\\\'";
                    else
                        tC = "" + c;
                    break;

                case '"':
                    if(!single)
                        tC = "\\\"";
                    else
                        tC = "" + c;
                    break;

                case '\\':
                    tC = "\\\\";
                    break;
                    
                default:
                    tC = "" + c;
            }
            out.append(tC);
            if(map != null)
                for(int j = 0; j < tC.length(); ++j)
                    map.add(i);
        }
        return out.toString();
    }
    /**
     * Returns a new, locale--neutral scanner.
     * 
     * @param in input stream to scan, not affected by this method
     * @return a new instance of <code>Scanner<code>, with
     * <code>Locale.ROOT</code>
     */
    public static Scanner newScanner(InputStream in) {
        return new Scanner(in).useLocale(Locale.ROOT);
    }
    /**
     * Returns a new, locale--neutral scanner.
     * 
     * @param file file to scan
     * @return a new instance of <code>Scanner<code>, with
     * <code>Locale.ROOT</code>
     */
    public static Scanner newScanner(File file) throws IOException {
        return new Scanner(file).useLocale(Locale.ROOT);
    }
    /**
     * Returns a new, locale--neutral scanner.
     * 
     * @param s string to scan
     * @return a new instance of <code>Scanner<code>, with
     * <code>Locale.ROOT</code>
     */
    public static Scanner newScanner(String s) {
        return new Scanner(s).useLocale(Locale.ROOT);
    }
    /**
     * Reads a stream into a string, neutral locale.
     * 
     * @param in input stream, closed after the file is read
     * @return contents of the whole file
     */
    public static String readStream(InputStream in) throws IOException {
        try(Scanner sc = newScanner(in).useDelimiter("\\A")) {
            String out;
            if(sc.hasNext())
                out = sc.next();
            else
                out = "";
            return out;
        }
    }
    /**
     * Reads a file into a string, neutral locale.
     * 
     * @param in input file
     * @return contents of the whole file
     */
    public static String readFile(File in) throws IOException {
        InputStream s = new FileInputStream(in);
        return readStream(s);
    }
    /**
     * Reads a resource file into a string, neutral locale.
     * 
     * @param clazz                     a class whose loader to use
     * @param name                      resource name
     * @return                          contents of the whole file
     */
    public static String readResourceFile(Class clazz, String name)
            throws IOException {
        InputStream in = clazz.getResourceAsStream(name);
        if(in == null)
            throw new IOException("resource not found: " + name);
        return readStream(in);
    }
    /**
     * Returns a file name with its extension replaces. If the original
     * name did not had any extension, a new extension is only appended.
     * 
     * @param fileName file name with the original extension
     * @param newExtension the replacing extension; null for none
     * @return file name with the new extension
     */
    public static String replaceExtension(String fileName, String newExtension) {
        int pos = fileName.lastIndexOf(".");
        if(pos == -1)
            pos = fileName.length();
        return fileName.substring(0, pos) +
                (newExtension != null ? "." + newExtension : "");
    }
    /**
     * Saves to a stream, the stream is closed afterwards by this method.
     * 
     * @param out output stream
     * @param text text to save
     */
    public static void save(OutputStream outStream, String text)
            throws IOException {
        try(PrintWriter out = new PrintWriter(outStream)) {
            out.print(text);
            if(out.checkError())
                throw new IOException("could not save file `" + outStream + "'");
        }
    }
    /**
     * Saves text to a file.
     * 
     * @param outFileName name of the file
     * @param text text to save
     */
    public static void save(String outFileName, String text)
            throws IOException {
        try(PrintWriter out = new PrintWriter(outFileName)) {
            out.print(text);
            if(out.checkError())
                throw new IOException("could not save file `" + outFileName + "'");
        }
    }
    /**
     * If the argument is within the range of <code>int</code>,
     * then return it. Otherwise, throws a runtime exception.
     * 
     * @param l 64--bit value
     * @return 32--bit value
     */
    public static int toInt(long l) {
        if(l >= Integer.MIN_VALUE && l <= Integer.MAX_VALUE)
            return (int)l;
        else
            throw new RuntimeException("value outside the range of a 32--bit integer: " +
                    l);
    }
    /**
     * <p>Checks if a constant name does not contain a lower--case letter,
     * and if a variable name is not capitalized. Otherwise, throws a parse
     * exception.</p>
     * 
     * <p>Not to be used with the Java parser.</p>
     * 
     * @param pos source position of the symbol name
     * @param name name
     * @param constant if the symbol is a constant or a variable
     */
    public static void checkSymbolName(StreamPos pos, String name,
            boolean constant) throws ParseException {
        if(true)
            return;
        if(constant) {
            for(int i = 0; i < name.length(); ++i) {
                char c = name.charAt(i);
                if(Character.isLowerCase(c))
                      throw new ParseException(pos,
                          ParseException.Code.ILLEGAL,
                          "lower case letter in the name of a constant");
           }
        } else {
           if(Character.isUpperCase(name.charAt(0)))
                 throw new ParseException(pos,
                     ParseException.Code.ILLEGAL,
                     "capitalized name of a variable");
        }
    }
}
