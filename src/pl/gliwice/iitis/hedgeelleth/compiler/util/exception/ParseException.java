package pl.gliwice.iitis.hedgeelleth.compiler.util.exception;

import org.antlr.v4.runtime.RecognitionException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * This exception is thrown when parse errors are encountered.
 * You can explicitly create objects of this exception type by
 * calling the method generateParseException in the generated
 * parser.<br>
 *
 * You can modify this class to customize your error reporting
 * mechanisms so long as you retain the public fields.
 */
public class ParseException extends CompilerException {
    /**
     * Not null if this is a paraphrase wrapper for Antlr4's
     * recognition exception.
     */
    RecognitionException recognitionException = null;
    /**
     * Creates a new instance of CompilerException, with an empty set of
     * reports.
     */
    public ParseException() {
        super();
    }
//    /**
//     * Creates a new instance of ParseException, with one report,
//     * and a reference to to Antlr'4 parse exception instead of
//     * position.
//     *
//     * @param recognitionException the original exception to wrap
//     * @param code                      error code, null for Code.UNKNOWN
//     * @param message                   message, without stream
//     *                                  position description
//     */
//    public ParseException(Code code, String message,
//            RecognitionException recognitionException) {
//        super(null, code, message);
//        this.recognitionException = recognitionException;
//    }
    /**
     * Creates a new instance of ParseException, with one report.
     *
     * @param pos                       position in the stream
     * @param code                      error code, null for Code.UNKNOWN
     * @param message                   message, without stream
     *                                  position description
     */
    public ParseException(StreamPos pos, Code code, String message) {
        super(pos, code, message);
    }
    /**
     * Creates a new instance of ParseException. This constructor
     * just calls
     * <code>CompilerException(StreamPos, Code, CompilerException)</code>.
     * See that superconstructor documentation for description
     * how the parameters are treated.
     *
     * @param pos                       position in the stream,
     *                                  applied only if not
     *                                  already defined in
     *                                  a report
     * @param code                      error code, null for unspecified,
     *                                  applied only if not
     *                                  already not equal to Code.UNKNOWN
     *                                  in a report
     * @param e                         exception, whose reports, if any,
     *                                  to reuse in this exception
     * @see CompilerException(StreamPos, Code, CompilerException)
     */
    public ParseException(StreamPos pos, Code code, ParseException e) {
        super(pos, code, e);
    }
  /**
   * This constructor is used by the method "generateParseException"
   * in the generated parser.
   */
  public ParseException(Token currentToken,
                        int[][] expectedTokenSequences,
                        String[] tokenImage) {
    // the position is unknown when this constructor is called,
    // as no stream name is passed, thus, determining of
    // the position is deferred up to parsing of this exception
    super(retrievePosAfter(currentToken), Code.PARSE,
            retrieveMessage(currentToken, expectedTokenSequences,
                tokenImage));
  }
  /**
   * Retrieves stream position from a token after a given token.
   * The position does not contain the stream
   * name, because the token does not contain a stream name.
   * Thus, exceptions with positions taken from this method should
   * later have the stream names completed, for example using
   * <code>completeStreamName(String)</code>.
   *
   * @param token                       token
   * @return                            stream position, without stream
   *                                    name
   */
  private static StreamPos retrievePosAfter(Token token) {
      return new StreamPos(null, token.next);
  }
  /**
   * The end of line string for this machine.
   */
  protected static String eol = System.getProperty("line.separator", "\n");
   /**
    * Retrieves parse error message from objects passed by the parser.
    *
    * @return                            a textual parse error description
    */
   private static String retrieveMessage(Token currentToken,
            int[][] expectedTokenSequences,
            String[] tokenImage) {
        StringBuilder expected = new StringBuilder();
        int maxSize = 0;
        for(int i = 0; i < expectedTokenSequences.length; i++) {
            if (maxSize < expectedTokenSequences[i].length) {
                maxSize = expectedTokenSequences[i].length;
            }
            for (int j = 0; j < expectedTokenSequences[i].length; j++) {
                expected.append(tokenImage[expectedTokenSequences[i][j]]).append(' ');
            }
            if (expectedTokenSequences[i][expectedTokenSequences[i].length - 1] != 0) {
                expected.append("...");
            }
            expected.append(eol).append("    ");
        }
        String retval = "unexpected \"";
        Token tok = currentToken.next;
        for(int i = 0; i < maxSize; i++) {
            if(i != 0)
                retval += " ";
            if(tok.kind == 0) {
                retval += tokenImage[0];
                break;
            }
            retval += CompilerUtils.addEscapes(tok.image);
            tok = tok.next;
        }
        retval += "\"";
        /*
        retval += ", was expecting";
        if (expectedTokenSequences.length == 1)
            retval += eol + "    ";
        else
            retval += " one of:" + eol + "    ";
        retval += expected.toString();
         */
        return retval;
    }
}
