/*
 * TranslatorException.java
 *
 * Created on Mar 7, 2009, 6:21:50 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.exception;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An abstract base class of this translator checked exceptions.
 *
 * @author Artur Rataj
 */
public abstract class TranslatorException extends Exception implements SourcePosition {
    /**
     * Position in the stream, null for none.
     */
    protected StreamPos pos;

    /**
     * Creates a new instance of TranslatorException.
     *
     * @param pos                       position, can be null
     * @param message                   message
     */
    public TranslatorException(StreamPos pos, String message) {
        super(message);
        if(pos != null)
            setStreamPos(pos);
    }
//    /**
//     * Creates a new instance of TranslatorException. This constructor
//     * copies only position and message, ignores possible other parameters
//     * in subclasses.
//     *
//     * @param e translator exception
//     */
//    public TranslatorException(TranslatorException e) {
//        super(e.getRawMessage());
//        if(e.pos != null)
//            setStreamPos(e.pos);
//    }
    /**
     * Creates a new instance of TranslatorException. Merges all reports
     * into a single message.
     *
     * @param e compiler exception
     */
    public TranslatorException(CompilerException e) {
        super(e.getMessage());
        setStreamPos(null);
    }
    @Override
    public void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public StreamPos getStreamPos() {
        return pos;
    }
    /**
     * Sets the position of this exception, if none was set so far.
     * If the position has already been set, this method does nothing.<br>
     * 
     * Also works with exceptions that have multiple positions. In such a
     * case, sets only the missing positions.
     *
     * @param pos                       position, can be null
     */
    public void completePos(StreamPos pos) {
        if(getStreamPos() == null)
            setStreamPos(pos);
    }
    /**
     * Returns the message without stream position data.
     * 
     * @return message without the prefixing position
     */
    public String getRawMessage() {
        return super.getMessage();
    }
    @Override
    public String getMessage() {
        return (pos != null ? pos.toString() + ": " : "") + getRawMessage();
    }
    @Override
    public String toString() {
        return (pos != null ? pos.toString() + ": " : "") + super.toString();
    }
}
