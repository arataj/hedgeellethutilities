/*
 * CompilerException.java
 *
 * Created on Jan 29, 2008, 1:18:21 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.exception;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A compiler exception. Contains a set of error reports, sorted by
 * file name and position within a file. The set, instead of a single
 * error report, is needed in the case of multiple reported translation
 * errors.<br>
 *
 * The set of errors is sorted, for clarity and repetitiveness of the
 * form of errors reported by the translator.<br>
 *
 * This exception, as opposed to the superclass, can have multiple
 * positions, one per report. Thus, usage of the original position
 * completion mechanism of <code>TranslatorException</code> would cause
 * a runtime exception.
 * 
 * @author Artur Rataj
 */
public class CompilerException extends TranslatorException {
    /**
     * A mark of error position within the text of a line. Can be used to
     * mark errors e.g. in a line modified by a preprocessor.
     */
    public static final String LINE_POSITION_MARK = "⚑";
    /**
     * General error categories, used usually only by regression tests.
     */
    public enum Code {
        /**
         * Default error code.
         */
        UNKNOWN,
        /**
         * Input/output error.
         */
        IO,
        /**
         * Parse error.
         */
        PARSE,
        /**
         * Invalid identifier or value.
         */
        INVALID,
        /**
         * Missing identifier or value.
         */
        MISSING,
        /**
         * Dupplicate identifier or value.
         */
        DUPLICATE,
        /**
         * Ambiguous identifier or value.
         */
        AMBIGUOUS,
        /**
         * Identifier could not be looked up.
         */
        LOOK_UP,
        /**
         * An identifier, value or some operation that is recognized,
         * but illegal in the given context.
         */
        ILLEGAL,
        /**
         * An arithmetic exception like an integer overflow.
         */
        ARITHMETIC,
        /**
         * A not implemented feature.
         */
        NOT_IMPLEMENTED,
    };
    /**
     * A single error report.
     */
    public static class Report implements Comparable<Report> {
        /**
         * Position within stream, can be null if not yet determined.
         */
        public StreamPos pos;
        /**
         * Error code.
         */
        public Code code;
        /**
         * Textual report description, does not contain stream position
         * description.
         */
        public String message;

        /**
         * Creates a new instance of Report.
         *
         * @param pos                       position in the stream,
         *                                  null for not yet determined
         * @param code                      error code
         * @param message                   message, without stream
         *                                  position description
         */
        public Report(StreamPos pos, Code code, String message) {
/*
if(pos != null && pos.line == 28 && pos.column == 15)
    pos = pos;
    */
            this.pos = pos;
            this.code = code;
            this.message = message;
        }
        /**
         * Creates a new instance of Report. This is a copying
         * constructor. The copy of stream position is deep.
         *
         * @param report                    report to copy
         *
         */
        public Report(Report r) {
            if(r.pos == null)
                pos = null;
            else
                pos = new StreamPos(r.pos);
            code = r.code;
            message = r.message;
        }
        /**
         * Sets the position of this exception, if none was set so far.
         * If the position has already been set, this method does nothing.<br>
         * 
         * @param streamPos position to set, if none already
         */
        public void completePos(StreamPos streamPos) {
            if(pos == null)
                pos = streamPos;
        }
        @Override
        public int compareTo(Report r) {
            int order;
            if(pos != null && r.pos != null) {
                order = pos.compareTo(r.pos);
                if(order == 0) {
                    int p = message.indexOf(LINE_POSITION_MARK);
                    int rP = r.message.indexOf(LINE_POSITION_MARK);
                    if(p != -1 && rP != -1)
                        return (int)Math.signum(p - rP);
                }
            } else
                // if at least one position is not yet determined,
                // do not use the positions for sorting
                order = 0;
            if(order != 0)
                return order;
            order = code.compareTo(r.code);
            if(order != 0)
                return order;
            return message.compareTo(r.message);
        }
        /**
         * A textual description of this report, to be used in
         * compiler messages. Works like
         * <code>toString</code>, but omits the error code.
         *
         * @return                      a textual description
         */
        public String getDescription() {
            String s;
            if(pos == null)
                s = "";
            else
                s = pos.toString();
            if(!message.isEmpty()) {
                if(!s.isEmpty())
                    s += ": ";
                s += message;
            }
            return s;
        }
        @Override
        public String toString() {
            String s;
            if(pos == null)
                s = "";
            else
                s = pos.toString() + ": ";
            s += code.toString();
            if(!message.isEmpty())
                s += ": " + message;
            return s;
        }
    }
    /**
     * Sorted reports.
     */
    protected SortedSet<Report> reports;

    /**
     * Creates a new instance of CompilerException, with an empty set of
     * reports.
     */
    public CompilerException() {
        super(null, "<unused>");
        reports = new TreeSet<>();
    }
    /**
     * Creates a new instance of CompilerException, with one report.
     *
     * @param pos                       position in the stream
     * @param code                      error code, null for Code.UNKNOWN
     * @param message                   message, without stream
     *                                  position description
     */
    public CompilerException(StreamPos pos, Code code, String message) {
        this();
        if(code == null)
            code = Code.UNKNOWN;
        Report r = new Report(pos, code, message);
        reports.add(r);
    }
    /**
     * Creates a new instance of CompilerException, that
     * copies all reports from another exception, possibly completing
     * some of their properties in  the copies.<br>
     *
     * For each report, the argument of this constructor
     * <code>pos</code> is ignored if source code
     * position is already set in the report. In such a case,
     * the position in the report is copied into
     * the report copy instead. This handling of <code>pos</code>
     * is needed in the case of nested primary expressions.
     * The value <code>code</code> is handled in a similar manner,
     * with the exception that a code being set means a code
     * different from <code>Code.UNKNOWN</code>.
     *
     * @param pos                       position in the stream,
     *                                  applied only if not
     *                                  already defined in
     *                                  a report
     * @param code                      error code, null for unspecified,
     *                                  applied only if already not equal to
     *                                  Code.UNKNOWN
     *                                  in a report
     * @param e                         exception, whose reports, if any,
     *                                  to reuse in this exception
     */
    public CompilerException(StreamPos pos, Code code, CompilerException e) {
        this();
        for(Report r : e.reports) {
            Report copy = new Report(r);
            if(copy.pos == null)
                copy.pos = pos;
            if(copy.code == Code.UNKNOWN) {
                if(code == null)
                    code = Code.UNKNOWN;
                copy.code = code;
            }
            reports.add(copy);
        }
    }
    /**
     * Adds a report to this exception.
     *
     * @param report                    report to add
     */
    public void addReport(Report report) {
        reports.add(report);
    }
    /**
     * Adds all reports from a given exception to this exception.
     *
     * @param report                    an exception with reports to add
     */
    public void addAllReports(CompilerException exception) {
        for(Report report : exception.getReports())
            addReport(report);
    }
    /**
     * Returns the reports within this exception.
     *
     * @return                          a sorted set of reports
     */
    public SortedSet<Report> getReports() {
        return reports;
    }
    /**
     * Removes all reports.
     */
    public void clearReports() {
        reports.clear();
    }
    /**
     * If there is one or more reports within this exception.
     * 
     * @return                          if any reports exist
     */
    public boolean reportsExist() {
        return getReports().size() != 0;
    }
    @Override
    public void setStreamPos(StreamPos pos) {
        throw new RuntimeException("this exception has support for multiple positions");
    }
    @Override
    public StreamPos getStreamPos() {
        throw new RuntimeException("this exception has support for multiple positions");
    }
    @Override
    public void completePos(StreamPos pos) {
        for(Report r : getReports())
            r.completePos(pos);
    }
    /**
     * Completes stream names in all reports and then, as the sort
     * order might have changed after adding the names, sorts the
     * reports again.<br>
     *
     * If some report already has a known stream name, it is not
     * replaced.
     *
     * @param name                      stream name to add to the reports
     */
    public void completeStreamName(String name) {
        for(Report r : getReports()) {
            StreamPos pos = r.pos;
            if(pos != null && pos.name == null)
                pos.name = name;
        }
        reports = new TreeSet<Report>(getReports());
    }
    /**
     * Prefixes messages in all reports.
     *
     * @param prefix                    prefix
     */
    public void prefixMessages(String prefix) {
        for(Report r : getReports())
            r.message = prefix + r.message;
    }
    @Override
    public String getRawMessage() {
        throw new RuntimeException("not valid in exception classes with separate reports");
    }
    /**
     * Returns the message, each report prefixed.
     * 
     * @param prefix prefix, null for none
     * @return message in this exception
     */
    public String getMessage(String prefix) {
        String s = "";
        for(Report r : getReports()) {
            if(!s.isEmpty())
                s += "\n";
            s += (prefix != null ? prefix + ": " : "") + r.getDescription();
        }
        return s;
    }
    @Override
    public String getMessage() {
        return getMessage(null);
    }
    /**
     * Returns a Java code compatible with the methods in
     * <code>CompilerTest</code>, that would generate the exact
     * reports in this exception.
     * 
     * @return                          a string with a Java code
     */
    public String toCodeString() {
        /*
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Initialization.java", 6, 7),
                CompilerException.Code.PARSE,
                "parse error"));
         */
        String s = "";
        for(Report r : getReports()) {
            String posString;
            if(r.pos == null)
                posString = "null";
            else
                posString =
                        "new StreamPos(\"" + r.pos.name + "\", " +
                        r.pos.line + ", " + r.pos.column + ")";
            s +=
                    "pattern.addReport(new CompilerException.Report(\n" +
                    "        " + posString + ",\n" +
                    "        CompilerException.Code." + r.code.toString() + ",\n" +
                    "        \"" + CompilerUtils.escapeQuotesSlashes(r.message, false, null) +
                    "\"));\n";
        }
        return s;
    }
    @Override
    public String toString() {
        String s = "";
        for(Report r : getReports())
            s += r.toString() + "\n";
        return s;
    }
}
