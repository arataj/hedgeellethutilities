/*
 * ModelException.java
 *
 * Created on Nov 24, 2011
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.exception;

/**
 * An exception thrown when there is a problem with the model or with the command
 * line options.
 * 
 * @author Artur Rataj
 */
public class ModelException extends Exception {
    /**
     * Creates a new instance of <code>ModelException</code>.
     */
    public ModelException(String message) {
        super(message);
    }
}
