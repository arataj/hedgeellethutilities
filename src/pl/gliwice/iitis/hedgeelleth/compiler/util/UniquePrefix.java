/*
 * UniquePrefix.java
 *
 * Created on Sep 11, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.util.*;

/**
 * A set of static methods for providing unique string prefixes, for objects
 * like labels.<br>
 * 
 * If a prefix is checked for the first time, its exact form is returned. Subsequent
 * calls about the same prefix returns the prefix with a suffix added, that
 * ensured, that never two the same prefixes are returned.<br>
 * 
 * This class can be reset, i. e. for the needs of a subsequent, independent compilation.
 * This class is reentrant except for the reset, which is global.
 * 
 * @author Artur Rataj
 */
public class UniquePrefix {
    /**
     * Counts occurences of a given prefix so far.
     */
    static Map<String, Integer> occurences = new HashMap<>();
        
    /**
     * Globally resets this class, clearing all prefixes stored in it.
     */
    public static void reset() {
        occurences.clear();
    }
    /**
     * Returns an unique form of a given prefix. If the prefix is already
     * unique, i. e. was not asked already after the last <code>reset()</code>,
     * it is returned in its exact form.
     * 
     * @param prefix prefix tested
     * @return unique variant of the prefix, may have a typically short suffix
     * attached
     */
    public static String get(String prefix) {
        Integer num = occurences.get(prefix);
        if(num == null)
            num = 0;
         occurences.put(prefix, num + 1);
         if(num == 0)
             return prefix;
         else
             return get(prefix + Integer.toHexString(num));
    }
}
