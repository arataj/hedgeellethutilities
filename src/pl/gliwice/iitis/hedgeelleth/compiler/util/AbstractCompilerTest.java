/*
 * AbstractCompilerTest.java
 *
 * Created on Jun 26, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.util.*;
import java.io.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * An abstract test class of a compiler, using JUnit.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractCompilerTest {
    /**
     * Name of the extension of the regression files.
     */
    final static protected String REGRESSION_STRING = ".regression";
    /**
     * Set in <code>test()</code> with the number of tests performed.
     */
    protected int TESTS_PERFORMED = -1;
    /**
     * Set in <code>test()</code> with the number of compilations performed
     * and comparisons outside of <code>compile()</code> and in
     * <code>compare()</code>.
     */
    protected int COMPILATIONS_PERFORMED = -1;
    
    /**
     * Number of performed tests. Used to check if all available
     * tests were executed.
     */
    protected int testsPerformed = 0;
    /**
     * Number of performed compilation attempts. Used to check if all available
     * compilations were attempted.
     */
    protected int compilationsPerformed = 0;

    /**
     * Creates a new instance of <code>AbstractCompilerTest</code>.
     */
    protected AbstractCompilerTest() {
    }
    /**
     * Registers that a test has been perfomed.
     */
    protected void testPerformed() {
        ++testsPerformed;
    }
    @Before
    public void  setUp() {
    }
    /**
     * Checks if position, code and description
     * of a given output error is
     * equal to respective trait of some pattern error.
     * If the pattern error is null, then the output error
     * should be null as well to be equal.<br>
     * 
     * If the equality does not hold, the test fails with
     * an appropriate message.
     * 
     * @param output                    output error, can be null
     * @param pattern                   pattern error, can be null
     */
    protected void errorEquals(CompilerException output,
            CompilerException pattern) {
        String error = null;
        if(pattern == null) {
            if(output != null)
                error = "unexpected compilation failure:\n" +
                        CompilerUtils.indent(1, output.toString(), "\t");
        } else {
            if(output == null)
                error = "unexpected compilation success";
            else  {
                Iterator<CompilerException.Report> o = output.getReports().iterator();
                Iterator<CompilerException.Report> p = pattern.getReports().iterator();
                while(o.hasNext() || p.hasNext()) {
                    CompilerException.Report oR;
                    if(o.hasNext())
                        oR = o.next();
                    else
                        oR = null;
                    CompilerException.Report pR;
                    if(p.hasNext())
                        pR = p.next();
                    else
                        pR = null;
                    if(oR == null || pR == null) {
                        error = "error number mismatch, dangling error \"";
                        if(oR != null)
                            error += oR.getDescription() +
                                    "\" found in compiler output";
                        else
                            error += pR.getDescription() +
                                    "\" found in pattern";
                    } else {
                        if(!oR.getDescription().equals(pR.getDescription()))
                            error = "unexpected error message: was " +
                                    "\"" + oR.getDescription() + "\", expected \"" +
                                    pR.getDescription() + "\"";
                        else if(oR.code != pR.code)
                            error = "unexpected error code: was " +
                                    oR.code + ", expected " + pR.code +
                                    ", in error \"" + oR.getDescription() + "\"";
                    }
                    if(error != null)
                        break;
                }
            }
        }
        if(error != null) {
            System.out.println("COMPILER ERROR: " + error);
            System.out.println("END");
            throw new AssertionError(error);
        }
    }
    /**
     * Removes variable range comments. See <code>compare()<code>
     * for details.
     * 
     * @param in input string
     * @return string with variable range comments removed
     */
    String removeVariableRangeComments(String in) {
        StringBuilder out = new StringBuilder();
        Scanner sc = CompilerUtils.newScanner(in);
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            if(line.trim().startsWith("global")) {
                int pos = line.indexOf(";");
                if(pos == -1)
                    throw new RuntimeException("unexpected format of variable declaration");
                line = line.substring(0, pos + 1);
            }
            out.append(line + "\n");
        }
        return out.toString();
    }
    /**
     * Compares a file to <code>&lt;file&gt;.regression.</code>. For special cases
     * not covered by <code>compile()</code>.
     * 
     * @param directory top directory of the compilation
     * @param testedFile file to test for being equal
     * @param regressionFile regression file, null for one with the default
     * extension;  as <code>PTAIO</code> ignores variable's range comments,
     * then if this parameter ends with `_c', then these comments are
     * ignored in <code>testedFile</code> by  filtering the tested file through
     * <code>removeVariableRangeComments()</code>
     */
    protected void compare(PWD directory, String testedFile, String regressionFile) {
        System.out.println("TEST:");
        if(regressionFile == null)
            regressionFile = testedFile + ".regression";
        System.out.println("\tin " + directory.toString() + "\n\tfile " +
                testedFile + "\n\tpattern " + regressionFile);
        try {
            String currentString = CompilerUtils.readFile(directory.getFile(
                testedFile));
            if(regressionFile.endsWith("_c"))
                currentString = removeVariableRangeComments(currentString);
            String patternString = CompilerUtils.readFile(directory.getFile(
                regressionFile));
            if(!currentString.equals(patternString)) {
                String s = "files differ: " +
                            testedFile + " " + regressionFile;
                System.out.println("\t" + s);
                throw new AssertionError(s);
            }
        } catch(IOException e) {
            System.out.println("\t" + e.getMessage());
            throw new AssertionError(e.getMessage());
        }
        System.out.println("OK.");
        ++compilationsPerformed;
    }
    /**
     * <p>Tests how the compiler behaves when compiling a given set of files,
     * against an error pattern.</p>
     * 
     * <p>Put <code>++compilationsPerformed;</code> after each successfull
     * test in this method.</p>
     *
     * @param directory top directory of the compilation,
     * @param files files to compile
     * @param pattern error pattern, as defined in <code>errorEquals</code>, or null
     * if the compilation is expected to be successful
     * @param patternFileName name of a pattern output file to compare, null if the
     * compilation is expected to fail
     */
    abstract protected void compile(PWD directory, String[] files,
            CompilerException pattern, String patternFileName);
    /**
     * Implement tests here.
     */
    abstract protected void test();
    @Test
    public void testCompiler() {
        System.out.println("Regression tests...");
        do {
            testsPerformed = 0;
            test();
            if(testsPerformed != TESTS_PERFORMED)
                System.out.println("*\n* WARNING: Not all regression tests were " +
                        "executed: " + testsPerformed +
                        " vs expected " + TESTS_PERFORMED + "\n*");
            if(compilationsPerformed != COMPILATIONS_PERFORMED)
                System.out.println("*\n* WARNING: Not all compilations within the regression tests were " +
                        "attempted: " + compilationsPerformed +
                        " vs expected " + COMPILATIONS_PERFORMED + "\n*");
        } while(false);
        System.out.println("Regression tests passed.");
    }
    @After
    public void tearDown() {
    }
}
