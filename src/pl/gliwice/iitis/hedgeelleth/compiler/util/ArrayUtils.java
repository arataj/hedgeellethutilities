/*
 * ArrayUtils.java
 *
 * Created on Dec 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.util.*;

/**
 * Some array conversion utilities.
 * 
 * @author Artur Rataj
 */
public class ArrayUtils {
    /**
     * Converts a list of integers into an array of <code>int</code>.<br>
     * 
     * Based on
     * <a href="http://stackoverflow.com/questions/718554/how-to-convert-an-arraylist-containing-integers-to-primitive-int-array">Marek Sebera's code</a>.
     * 
     * @param integers list of <code>Integer</code>
     * @return array of <code>int</code>s
     */
    public static int[] convertInteger(List<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        return ret;
    }
    /**
     * Converts a list of doubles into an array of <code>int</code>.<br>
     * 
     * Based on
     * <a href="http://stackoverflow.com/questions/718554/how-to-convert-an-arraylist-containing-integers-to-primitive-int-array">Marek Sebera's code</a>.
     * 
     * @param doubles list of <code>Double</code>
     * @return array of <code>double</code>s
     */
    public static double[] convertDouble(List<Double> doubles) {
        double[] ret = new double[doubles.size()];
        Iterator<Double> iterator = doubles.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().doubleValue();
        }
        return ret;
    }
    /**
     * Converts an integer value into an array of four bytes.
     * 
     * @param value input value
     * @param out a preallocated array of the size of 4; if null,
     * a new array is returned
     * @return new byte array
     */
    public static final byte[] intToByteArray(int value, byte[] out) {
        if(out != null) {
            out[0] = (byte)(value >>> 24);
            out[1] = (byte)(value >>> 16);
            out[2] = (byte)(value >>> 8);
            out[3] = (byte)(value);
            return out;
        } else
            return new byte[] {
                    (byte)(value >>> 24),
                    (byte)(value >>> 16),
                    (byte)(value >>> 8),
                    (byte)value};
    }
    /**
     * Converts a long value into an array of a given
     * number of bytes. Throws a runtime exception if
     * the value does not fit into the output array.
     * 
     * @param value input value
     * @param arraySize size of the array to create
     * @param out a preallocated array of the size
     * of at least <code>arraySize</code>; null if to
     * create  ewn array
     * @return new byte array
     */
    public static final byte[] longToByteArray(long value,
            int arraySize, byte[] out) {
        if(out == null)
            out = new byte[arraySize];
        else if(out.length < arraySize)
            throw new RuntimeException("output array too small");
        int shift = (arraySize - 1)*8;
        if(arraySize < 8) {
            int b = (byte)(value >>> (shift + 8));
            if(b != 0 && b != -1)
                throw new RuntimeException("value " + value +
                        " too large to fit into " + (shift + 8) + " bits");
        }
        for(int i = 0; i < arraySize; ++i) {
            out[i] = (byte)(value >>> shift);
            shift -= 8;
        }
        if(arraySize < 8) {
            if(value > 0 && (out[0]&0x80) != 0)
                throw new RuntimeException("value " + value +
                        " too large to fit into " + arraySize*8 + " bits");
            if(value < 0 && (out[0]&0x80) == 0)
                throw new RuntimeException("value " + value +
                        " too large to fit into " + arraySize*8 + " bits");
        }
        return out;
    }
    /**
     * COnvefrts an array of bytes to long value.
     * 
     * @param in input buffer
     * @return long value
     */
    public static long byteArrayToLong(byte[] in) {
        long out = 0;
        for(int i = 0; i < in.length; ++i)
            out = (out << 8) + (in[i]&0xff);
        if((in[0]&0x80) != 0)
            for(int i = in.length; i < 8; ++i)
                out += 0xffL << (i << 3);
        return out;
    }
    /**
     * A test routine.
     * 
     * @param args
     */
    public static void main(String[] args) {
        long l = 50000000000000000L;
        // l = 500;
        System.out.println(" in = " + l);
        byte[] b = longToByteArray(l, 3, null);
        for(int i = 0; i < b.length; ++i)
            System.out.print(b[i] + " ");
        System.out.println();
        long m = byteArrayToLong(b);
        System.out.println("out = " + m);
        b = longToByteArray(m, 8, null);
        for(int i = 0; i < b.length; ++i)
            System.out.print(b[i] + " ");
        System.out.println();
    }
}
