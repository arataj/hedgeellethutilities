/*
 * Array2D.java
 *
 * Created on Jun 28, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.image;

import java.util.*;
import java.io.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.NumStream;

/**
 *
 * @author Artur Rataj
 */
public class Array2d {
    enum FileType {
        TEXT,
        PNG,
    };
    final int PNG_MAX = 65535;
    
    public final int WIDTH;
    public final int HEIGHT;
    public final double[][] CONTENTS;

    /**
     * A new image initialised with zeroes.
     * 
     * @param width width
     * @param height height
     */
    public Array2d(int width, int height) {
        WIDTH = width;
        HEIGHT = height;
        CONTENTS = allocContents();
    }
    /**
     * A new image containing a given data.
     * 
     * @param data data to be used as contents, not copied
     */
    public Array2d(double[][] data) {
        WIDTH = data[0].length;
        HEIGHT = data.length;
        CONTENTS = data;
    }
    /**
     * Loads a new array from a text file.
     * 
     * @param fileName input text file
     */
    public Array2d(String fileName) throws IOException {
        try(InputStream in = new FileInputStream(fileName)) {
            CONTENTS = NumStream.toArray(in);
            HEIGHT = CONTENTS.length;
            if(HEIGHT == 0)
                throw new IOException("no input data");
            WIDTH = CONTENTS[0].length;
        }
    }
    /**
     * Loads a new array from a PNG file.
     * 
     * @param fileName input text file
     * @param plane plane to read, must be 0 for monochrome images
     * @param min input minimum possible integer 0 is mapped to this contents value
     * @param max input maximum possible integer 65535 is mapped to this contents value
     */
    public Array2d(String fileName, int plane, double min, double max) throws IOException {
        BufferedImage image = ImageIO.read(new File(fileName));
        WritableRaster raster = image.getRaster();
        WIDTH = raster.getWidth();
        HEIGHT = raster.getHeight();
        CONTENTS = allocContents();
        double diff = max - min;
        for (int y = 0; y < HEIGHT; ++y) {
            for (int x = 0; x < WIDTH; ++x) {
                int value = raster.getSample(x, y, plane);
                double scaled = min + value*diff/PNG_MAX;
                CONTENTS[y][x] = scaled;
            }
        }
    }
    private double[][] allocContents() {
        double[][] contents = new double[HEIGHT][];
        for(int y = 0; y < HEIGHT; ++y)
            contents[y] = new double[WIDTH];
        return contents;
    }
    /**
     * Quantises this array to a 16--bit unsigned monochrome buffered image.
     * 
     * @param min contents value represented as minimum possible integer 0;
     * @param max contents value represented as as maximum possible integer 65535;
     * @return buffered image
     */
    public BufferedImage toImage(double min, double max) {
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT,
                BufferedImage.TYPE_USHORT_GRAY);
        WritableRaster raster = image.getRaster();
        double diff = max - min;
        for (int y = 0; y < HEIGHT; ++y) {
            for (int x = 0; x < WIDTH; ++x) {
                double scaled = Math.min(1.0, Math.max(0.0,
                        (CONTENTS[y][x] - min)/diff));
                raster.setSample(x, y, 0, (int)Math.round(scaled*PNG_MAX));
            }
        }
        return image;
    }
    /**
     * A save to unsigned 16--bit PNG, lossy due to quantisation and clipping.
     * 
     * @param fileName name of the output file
     * @param min contents value represented as minimum possible integer 0;
     * @param max contents value represented as as maximum possible integer 65535;
     */
    public void savePNG(String fileName, double min, double max) throws IOException {
        BufferedImage image = toImage(min, max);
        ImageIO.write(image, "PNG", new File(fileName));
    }
    /**
     * Combines up to three arrays into a single RGB image, 8 bit unsigned per plane.
     * 
     * @param fileName output file name
     * @param min this input value is pmapped to output's minimum value of 0
     * @param max this input value is pmapped to output's maximum value of 255
     * @param arrays up to three arrays
     */
    public static void savePNGColor(String fileName, double min, double max,
            Array2d[] arrays) throws IOException {
        int planes = arrays.length;
        int width = arrays[0].WIDTH;
        int height = arrays[0].HEIGHT;
        BufferedImage colour = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        WritableRaster rasterOut = colour.getRaster();
        double diff = max - min;
        final int MAX = 255;
        for(int p = 0; p < planes; ++p) {
            double[][] c = arrays[p].CONTENTS;
            for(int y = 0; y < height; ++y) {
                for(int x = 0; x < width; ++x) {
                    rasterOut.setSample(x, y, p,
                            Math.max(0, Math.min(MAX,
                                    (int)Math.round((c[y][x] - min)*MAX/diff))));
                }
            }
        }
        ImageIO.write(colour, "PNG", new File(fileName));
    }
    /**
     * Saves this array.
     * 
     * @param fileName otuput file name; PNG is lossy due to 16--bit quantisation
     * @param type type of the output file
     */
    public void save(String fileName, FileType type) throws IOException {
        switch(type) {
            case TEXT:
                try(OutputStream out = new FileOutputStream(fileName)) {
                    NumStream.fromArray(CONTENTS, out);
                }
                break;
                
            case PNG:
                double min = findMin();
                double max = findMax();
                savePNG(fileName, min, max);
                break;
                
            default:
                throw new IOException("saving to " + type.toString() + " unimplemented");
        }
    }
    public void save(String fileName) throws IOException {
        save(fileName, FileType.TEXT);
    }
    /**
     * Finds global minimum in the contents.
     */
    public double findMin() {
        double min = Double.MAX_VALUE;
        for(int y = 0; y < HEIGHT; ++y)
            for(int x = 0; x < WIDTH; ++x) {
                double v = CONTENTS[y][x];
                if(min > v)
                    min = v;
            }
        return min;
    }
    /**
     * Finds global maximum in the contents.
     */
    public double findMax() {
        double max = -Double.MAX_VALUE;
        for(int y = 0; y < HEIGHT; ++y)
            for(int x = 0; x < WIDTH; ++x) {
                double v = CONTENTS[y][x];
                if(max < v)
                    max = v;
            }
        return max;
    }
}
