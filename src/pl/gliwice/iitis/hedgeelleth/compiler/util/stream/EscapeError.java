/*
 * EscapeError.java
 *
 * Created on Nov 12, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.stream;

/**
 * JavaCC by default throws <code>Error</code> when an invalid unicode sequence
 * is found. This class replaces <code>Error</code>, in oder to explicitly store the stream
 * position.<br>
 * 
 * No further parsing is performed after this error is thrown.
 * 
 * @author Artur Rataj
 */
public class EscapeError extends Error {
    public StreamPos pos;
    
    public EscapeError(StreamPos pos, String message) {
        super(message);
        this.pos = pos;
    }
}
