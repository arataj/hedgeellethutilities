/*
 * StreamPos.java
 *
 * Created on Jan 23, 2008, 10:21:39 PM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.stream;

import java.util.List;

/**
 * Position in the compiled stream.<br>
 *
 * Exceptions throw by parsers sometimes do not have the stream
 * name included, which in such cases is completed later.
 * 
 * @author Artur Rataj
 */
public class StreamPos implements Comparable<StreamPos> {
    /**
     * Stream name or null if unknown.
     */
    public String name;
    /**
     * Line number, starting at 1, or -1 for no position.
     */
    public int line;
    /**
     * Column number, starting at 1. Valid only if <code>line != -1</code>.
     * If 0, it means that only the line number is known.
     */
    public int column;
    
    /**
     * Creates a new instance of StreamPos, without any position data.<br>
     *
     * Only this constructor allows usage of <code>set(StreamPos)</code>.
     */
    public StreamPos() {
        this.name = null;
// this.name = "";
// StackTraceElement[] s = Thread.getAllStackTraces().get(Thread.currentThread());
// for(int i = 0; i < s.length; ++i)
//     name += s[i].toString() + "\n";
        line = -1;
    }
    /**
     * <p>Creates a new instance of <code>StreamPos</code>.</p>
     *
     * @param name stream name, can be null if not known yet
     * @param line line number, starting at 1, or -1 for no position
     * @param column column number, starting at 1, used only if
     * <code>line != -1</code>; 0 if only line is to be known
     */
    public StreamPos(String name, int line, int column) {
        this.name = name;
        this.line = line;
        this.column = column;
/*if(line == 7 && column == 9)
    line = line;*/
    }
    /**
     * Creates a new instance of StreamPos, taking position from a given token.
     *
     * @param name                      stream name, can be null if not known
     *                                  yet
     * @param token                     parsed token, whose position is copied
     *                                  to this object
     */
    public StreamPos(String name, Token token) {
        this(name, token.beginLine, token.beginColumn);
    }
    /**
     * Creates a new instance of StreamPos, taking position from a given Antlr4's
     * token.
     *
     * @param name                      stream name, can be null if not known
     *                                  yet
     * @param token                     parsed token, whose position is copied
     *                                  to this object
     */
    public StreamPos(String name, org.antlr.v4.runtime.Token token) {
        this(name, token.getLine(), token.getCharPositionInLine() + 1);
    }
    /**
     * Creates a new instance of StreamPos, with only stream's name
     * but without position within that stream.<br>
     *
     * @param name                      name of stream or other name,
     *                                  can be null if not known yet
     */
    public StreamPos(String name) {
        this();
        this.name = name;
    }
    /**
     * Creates a new instance of StreamPos. This is a copying constructor.
     *
     * @param pos                       stream to copy
     */
    public StreamPos(StreamPos pos) {
        this();
        if(pos == null)
            throw new RuntimeException("null position");
        set(pos);
    }
    /**
     * Converts a textual representation into a new stream position.
     * A stream name can not contain `:' for this method to work properly.
     * 
     * @param p string to parse, in the format as returned by <code>toString()</code>
     * @return new stream position, or null if the parsing was unsuccesfull
     */
    public static StreamPos parse(String p) {
        int colon = p.indexOf(':');
        if(colon == -1)
            return new StreamPos(p);
        String name = p.substring(0, colon);
        if(name.isEmpty())
            return null;
        if(colon >= p.length() - 1)
            return null;
        String location = p.substring(colon + 1);
        colon = location.indexOf(':');
        if(colon == -1)
            return null;
        try {
            int line = Integer.parseInt(location.substring(0, colon));
            int column = Integer.parseInt(location.substring(colon + 1));
            return new StreamPos(name, line, column);
        } catch(NumberFormatException e) {
            return null;
        }
    }
    /**
     * Copies data from a given position to this position.<br>
     * 
     * This method can be used only if <code>StreamPos(null)</code>
     * have been used to construct this object, otherwise a runtime
     * exception is thrown.
     * 
     * @param source                    stream to copy
     */
    public void set(StreamPos source) {
        if(name == null && line == -1) {
            name = source.name;
            line = source.line;
            column = source.column;
        } else
            throw new RuntimeException("position not empty");

    }
    /**
     * Advances this position by a given number of characters in the stream.
     *
     * @param offset                    offset by which to move the position
     *                                  must be &gt;= 0, otherwise a runtime
     *                                  exception is thrown
     * @param text                      text in the input stream, that begins
     *                                  at this position before the change,
     *                                  and ends at least just before this
     *                                  position after the change
     */
    public void advancePos(int offset, String text) {
        if(offset < 0)
            throw new RuntimeException("invalid offset = " + offset);
        for(int i = 0; i < offset; ++i) {
            char c = text.charAt(i);
            switch(c) {
                case '\n':
                    ++line;
                    column = 1;
                    break;

                case '\r':
                    break;

                default:
                    ++column;
            }
        }
    }
    /**
     * Reconstructs a stream, on basis of images and positions
     * of tokens in a list. The reconstructed stream may differ
     * from the original parsed stream, but the tokens' positions
     * will match.
     *
     * @param beginPos                  the first character of the
     *                                  reconstructed stream is assumed
     *                                  to have this position
     * @param tokens                    tokens that resulted from
     *                                  parsing some stream
     * @return                          heuristically reconstructed
     *                                  stream, assuming first
     *                                  character is at this
     *                                  position
     */
    public static String reconstructStream(StreamPos beginPos,
            List<Token> tokens) {
        String out = "";
        StreamPos currPos = new StreamPos(beginPos);
        for(Token u : tokens) {
            int currL = currPos.line;
            int currC = currPos.column;
            int targetL = u.beginLine;
            int targetC = u.beginColumn;
            while(currL < targetL) {
                out += "\n";
                ++currL;
                currC = 0;
            }
            while(currC < targetC) {
                out += " ";
                ++currC;
            }
            currPos.line = currL;
            currPos.column = currC;
            currPos.advancePos(u.image.length(), u.image);
            out += u.image;
        }
        return out;
    }
    /**
     * Get the offset of this stream position, given the stream's
     * whole contents.
     * 
     * @param contents                  contents of the stream
     * @return                          offset within the stream,
     *                                  -1 if not found
     */
    public int getOffset(String contents) {
        int offset = -1;
        int lineCount = 0;
        boolean newLine = true;
        for(int i = 0; i < contents.length(); ++i) {
            if(newLine) {
                if(lineCount == line - 1) {
                    offset = i + (column - 1);
                    break;
                }
                newLine = false;
            }
            if(contents.charAt(i) == '\n') {
                ++lineCount;
                newLine = true;
            }
        }
        return offset;
    }
    /**
     * Adds another position to this position. If <code>pos.line = 1</code>,
     * then <code>this.column += pos.column - 1</code>, otherwise
     * <code>this.column = pos.column</code>; in both cases
     * <code>this.line += pos.line - 1</code>
     * 
     * @param pos the other position
     */
    public void add(StreamPos pos) {
        line += pos.line - 1;
        if(pos.line < 1)
            throw new RuntimeException("line numbering starts at 1");
        if(column < 1 || pos.column < 1)
            throw new RuntimeException("column numbering starts at 1");
        else if(pos.line == 1)
            column += pos.column - 1;
        else
            column = pos.column;
    }
    @Override
    public int compareTo(StreamPos s) {
        int order;
        if(name != null && s.name != null) {
            order = name.compareTo(s.name);
            if(order != 0)
                return order;
        } else
            // at least one name unknown, do not assume
            // any order depending on the names
            order = 0;
        if(line != -1) {
            if(line < s.line)
                return -1;
            else if(line > s.line)
                return 1;
            if(column == 0 || s.column == 0)
                // at least one column is unknown 
                return 0;
            if(column < s.column)
                return -1;
            else if(column > s.column)
                return 1;
        } else
            return 0;
        return 0;
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof StreamPos)
            return compareTo((StreamPos)o) == 0;
        else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 97 * hash + this.line;
        hash = 97 * hash + this.column;
        return hash;
    }
    @Override
    public String toString() {
        String s = name != null ? name +
                (line != -1 || column != 0 ? ":" : "") : "";
        if(line != -1) {
            s = s + line;
            if(column != 0)
                s = s + ":" + column;
        }/* else
            s += "<no stream position>"; */
        return s;
    }
}
