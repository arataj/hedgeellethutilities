/*
 * NdFile.java
 *
 * Created on Jun 17, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.stream;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;

/**
 * Reads information from an Nd file.
 * 
 * @author Artur Rataj
 */
public class NdFile {
    /**
     * A suffix which denotes, that a value is double and not an integer.
     */
    public static final String DOUBLE_SUFFIX = "f";
    /**
     * A branch within a choice.
     */
    public static class Branch {
        /**
         * Owner choice of this branch.
         */
        public Choice owner;
        /**
         * Numbers within this branch, i.e. the values returned by <code>nextInt()</code>.
         */
        public final List<Integer> num;
        /**
         * A copy of <code>num</code> as an array. Needed only for faster computations.
         */
        public int[] numAsArray;
        /**
         * Name of the step variable. If null, it means that this branch holds merely
         * return values. Such branches occur in merged models, where choice states are already
         * found.
         */
        public String stepVariable;
        /**
         * State, denoted by the step variable, where the choice is located.
         */
        public final int source;
        /**
         * Target state, denoted by the step variable,  of this particular branch.
         */
        public final int target;
        
        /**
         * Creates a branch out of state filters of the form as defined
         * in <code>BackendPTACompilation.xStateFilter</code>.
         * 
         * @param onwer owner of this branch
         * @param num numbers within this branch, i.e. the values returned by
         * <code>nextInt()</code>
         * @param b source state filter
         * @param e target state filter
         */
        public Branch(Choice owner, List<Integer> num, String b, String e) throws IOException {
            this.owner = owner;
            this.num = num;
            makeNumAsArray();
            source = decode(b);
            target = decode(e);
        }
        private void makeNumAsArray() {
            numAsArray = new int[this.num.size()];
            for(int i = 0; i < this.num.size(); ++i)
                numAsArray[i] = this.num.get(i);
        }
        /**
         * Creates a new branch. This is a copying constructor.
         * 
         * @param source branch to copy
         */
        public Branch(Branch source) {
            this.owner = source.owner;
            this.num = new LinkedList<>(source.num);
            makeNumAsArray();
            this.stepVariable = source.stepVariable;
            this.source = source.source;
            this.target = source.target;
        }
        private int decode(String filter) throws IOException {
            int pos = filter.indexOf("=");
            if(pos == -1)
                throw new IOException("invalid state filter format");
            String name = filter.substring(0, pos);
            try {
                int value = Integer.parseInt(filter.substring(pos + 1));
                if(stepVariable == null)
                    stepVariable = name;
                else if(!stepVariable.equals(name))
                    throw new RuntimeException("conflicting variables in state filter");
                return value;
            } catch(NumberFormatException e) {
                throw new IOException("expected integer value in state filter");
            }
        }
        @Override
        public String toString() {
            StringBuilder numStr = new StringBuilder();
            boolean first = true;
            for(int i : num) {
                if(!first)
                    numStr.append(",");
                numStr.append(i);
                first = false;
            }
            String s = "#" + numStr.toString() + ":";
            if(stepVariable == null)
                s += "<no step variable>";
            else
                s += stepVariable + "(" + source + "->" + target + ")";
            return s;
        }
    }
    /**
     * A single non--deterministic choice.
     */
    public static class Choice {
        /**
         * Position of a substring in the source code, which produced this
         * choice.
         */
        public final TextRange range;
        /**
         * <p>Names of subsequent arguments, as in the output model.</p>
         */
        public final List<String> argName;
        /**
         * <p>Values of respective arguments in <code>argsName</code>.</p>
         * 
         * <p>If a respective value is null then an argument is a state variable, otherwise
         * it is a parameter and also one of model constants. Element type can be <code>int</code>
         * or <code>double</code>, depending on the type of such a constant, or <code>Double.NaN</code>
         * if to be copied from a current define value.</p>
         * 
         * <p>Number's subclass can be only Integer or Double.</p>
         */
        public final List<Number> argValue;
        /**
         * Source of subsequent arguments.
         */
        public final List<String> argSource;
        /**
         * Branches within this choice. Two branches can not share the same number.
         */
        public final List<Branch> xs;
        /**
         * Minimum number in branches.
         */
        public int NUM_MIN;
        /**
         * Maximum number in branches.
         */
        public int NUM_MAX;
        /**
         * If the numbers in branches are continuous, i.e. spaced by 1.
         */
        public boolean NUM_CONTINUOUS;
        /**
         * Set of numbers in the branches.
         */
        public SortedSet<Integer> NUM_SET;
        
        /**
         * After modification to <code>xs</code>, a call
         * to <code>computeNumSet()</code> is required.

         * @param range substring placement in the source code
         */
        public Choice(TextRange range) {
            this.range = range;
            argName = new LinkedList<>();
            argValue = new LinkedList<>();
            argSource = new LinkedList<>();
            xs = new LinkedList<>();
        }
        public Choice(Choice choice) {
            this.range = choice.range;
            argName = new LinkedList<>(choice.argName);
            argValue = new LinkedList<>(choice.argValue);
            argSource = new LinkedList<>(choice.argSource);
            xs = new LinkedList<>();
            for(Branch b : choice.xs) {
                Branch bCopy = new Branch(b);
                bCopy.owner = this;
                xs.add(bCopy);
            }
            computeNumSet();
        }
        /**
         * Computes the set of numbers in the branches, extreme values,
         * continuity.
         */
        public void computeNumSet() {
            NUM_MIN = Integer.MAX_VALUE;
            NUM_MAX = Integer.MIN_VALUE;
            NUM_SET = new TreeSet<>();
            for(Branch b : xs)
                for(int n : b.num) {
                    if(NUM_MIN > n)
                        NUM_MIN = n;
                    if(NUM_MAX < n)
                        NUM_MAX = n;
                    if(NUM_SET.contains(n))
                        throw new RuntimeException("branches share a number");
                    NUM_SET.add(n);
                }
            NUM_CONTINUOUS = true;
            for(int i = NUM_MIN; i <= NUM_MAX; ++i)
                if(!NUM_SET.contains(i)) {
                    NUM_CONTINUOUS = false;
                    break;
                }
        }
        /**
         * Returns the number of parameters in this choice.
         * 
         * @return number of parameters of a respective  nd function
         */
        public int getNumParameters() {
            int parameterNum = 0;
            for(Number n : argValue)
                if(n != null)
                    ++parameterNum;
            return parameterNum;
        }
        /**
         * Returns the number of state variables in this choice.
         * 
         * @return number of variable arguments of a respective  nd function
         */
        public int getNumSV() {
            int svNum = 0;
            for(Number n : argValue)
                if(n == null)
                    ++svNum;
            return svNum;
        }
        /**
         * If the call definition in the source file is identical. This is implied by the same source position
         * and the same set of return values possible. The bound must have the same numerical value, the other
         * arguments must have the same names in the output models. This method does not also take into
         * account the context, and in effect, the semantics of output values of the call may be different for
         * source--equivalent calls.
         * 
         * @param other choice being compared to this one
         * @return if two choices can be associated inter--model
         */
        public boolean sourceEquivalent(Choice other) {
            if(!(range.samePosition(other.range) == 0))
                return false;
            if(!NUM_SET.containsAll(other.NUM_SET) ||
                    !other.NUM_SET.containsAll(NUM_SET))
                return false;
            Iterator<String> nI = other.argName.iterator();
            for(String name : argName) {
                String otherName = nI.next();
                if(!name.equals(otherName))
                    return false;
            }
            return true;
        }
    }
    /**
     * Choices.
     */
    public final List<Choice> CHOICES;
    /**
     * Reads information from an Nd file.
     * 
     * @param inFilename input file name
     */
    public NdFile(String inFilename) throws IOException {
        CHOICES = new LinkedList<>();
        Scanner lineSc = CompilerUtils.newScanner(new File(inFilename));
        Choice currChoice = null;
        while(lineSc.hasNextLine()) {
            String line = lineSc.nextLine().trim();
            if(!line.startsWith("#") && !line.startsWith("\"")) {
                if(currChoice != null)
                    CHOICES.add(currChoice);
                currChoice = new Choice(new TextRange(line));
            } else if(line.startsWith("\"")) {
                line = line.substring(1);
                int pos = line.indexOf("\"");
                String source = line.substring(0, pos);
                currChoice.argSource.add(source);
                line = line.substring(pos + 1);
                pos = line.indexOf(",");
                if(pos == -1)
                    pos = line.length();
                String name = line.substring(0, pos);
                currChoice.argName.add(name);
                String valueStr = line.substring(Math.min(
                        line.length(), pos + 1));
                Number value;
                if(valueStr.isEmpty())
                    value = null;
                else if(valueStr.equals("?"))
                    value = Double.NaN;
                else try {
                    if(valueStr.endsWith(DOUBLE_SUFFIX))
                        value = Double.parseDouble(valueStr.substring(0, valueStr.length() - 1));
                    else
                        value = Integer.parseInt(valueStr);
                } catch(NumberFormatException e) {
                    throw new IOException("invalid parameter value");
                }
                currChoice.argValue.add(value);
            } else {
                int pos = line.indexOf(",");
                List<Integer> num = new LinkedList<>();
                try {
                    Scanner sc = CompilerUtils.newScanner(line.substring(1, pos)).
                            useDelimiter("#");
                    while(sc.hasNext())
                        num.add(Integer.parseInt(sc.next()));
                } catch(NumberFormatException e) {
                    throw new IOException("branch value is not integer");
                }
                if(num.isEmpty())
                    throw new IOException("no values in a branch");
                line = line.substring(pos + 1);
                int posSource = line.indexOf(",");
                int posTarget = line.lastIndexOf(",");
                String b = line.substring(0, posSource);
                String e = line.substring(posTarget + 1);
                Branch branch = new Branch(currChoice, num, b, e);
                if(currChoice == null)
                    throw new IOException("branch but no preceding choice");
                currChoice.xs.add(branch);
                currChoice.computeNumSet();
            }
        }
        if(currChoice != null)
            CHOICES.add(currChoice);
        lineSc.close();
    }
    /**
     * Creates a new Nd file. This is a copying constructor.
     * 
     * @param orig nd file to copy
     */
    public NdFile(NdFile orig) {
        CHOICES = new LinkedList<>();
        for(Choice ch : orig.CHOICES)
            CHOICES.add(new Choice(ch));
        
    }
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for(Choice ch : CHOICES) {
            out.append("choice " + ch.range.toString() + "\n");
            Iterator<String> nameI = ch.argName.iterator();
            Iterator<Number> valueI = ch.argValue.iterator();
            Iterator<String> sourceI = ch.argSource.iterator();
            while(nameI.hasNext()) {
                String name = nameI.next();
                Number value = valueI.next();
                String source = sourceI.next();
                out.append("\t");
                if(value == null) {
                    out.append("state var: ");
                    out.append(name);
                } else {
                    out.append("parameter: ");
                    if(value instanceof Integer)
                        out.append("int ");
                    else if(value instanceof Double)
                        out.append("double ");
                    else
                        throw new RuntimeException("unknown type");
                    out.append(name + "=");
                    out.append(value);
                }
                out.append("\n");
            }
            for(Branch b : ch.xs) {
                out.append("\tbranch");
                for(int i : b.num) {
                    out.append(" #");
                    out.append(i);
                }
                if(b.stepVariable == null)
                    out.append("<no step variable>");
                else {
                    out.append(", ");
                    out.append(b.stepVariable + "=" + b.source + " -> " +
                            b.stepVariable + "=" + b.target);
                }
                out.append("\n");
            }
        }
        return out.toString();
    }
}
