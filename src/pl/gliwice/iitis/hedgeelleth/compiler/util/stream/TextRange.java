/*
 * TextRange.java
 *
 * Created on Jun 17, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.stream;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.LineIndex;

/**
 * Beginning and end of a substring of source code.
 * 
 * @author Artur Rataj
 */
public class TextRange {
    /**
     * <p>The first character in the parsed stream. For long prefixes, it may be sometimes
     * less informatory than <code>pos</code>. Null if not specified.</p>
     * 
     * <p>Together with <code>textEnd</code>, these fields are frequently unset. They are
     * required only in some special cases, like a call to the @RANDOM method.</p>
     */
    public final StreamPos BEG;
    /**
     * The last character in the parsed stream, inclusive. Null if not specified.
     */
    public final StreamPos END;
    
    /**
     * Creates a new text range.
     * 
     * @param beg beginning, deeply copied
     * @param end end, deeply copied
     */
    public TextRange(StreamPos beg, StreamPos end) {
        BEG = new StreamPos(beg);
        END = new StreamPos(end);
    }
    /**
     * Creates a new text range.
     *
     * @param text encoded text range, as defined by <code>encode()</code>
     */
    public TextRange(String text) throws IOException {
        int nameEnd = text.indexOf("#");
        if(nameEnd == -1)
            throw new IOException("invalid format; no name found");
        String name = text.substring(0, nameEnd);
        text = text.substring(nameEnd + 1);
        int posSplit = text.indexOf("#");
        if(posSplit == -1)
            throw new IOException("invalid format; no positions found");
        BEG = getStreamPos(text.substring(0, posSplit));
        END = getStreamPos(text.substring(posSplit + 1));
        BEG.name = name;
        END.name = name;
    }
    /**
     * COnstruct a position from line and column, leaves name null.
     * 
     * @param text constans name and column, separated by a colon
     * @return a stream pos without name
     */
    private StreamPos getStreamPos(String text) throws IOException {
        int colon = text.indexOf(":");
        if(colon == -1)
            throw new IOException("invalid format; no line and column found");
        try {
            int line = Integer.parseInt(text.substring(0, colon));
            int column = Integer.parseInt(text.substring(colon + 1));
            return new StreamPos(null, line, column);
        } catch(NumberFormatException e) {
            throw new IOException("invalid format; line or column not integer");
        }
    }
    /**
     * Creates a new text range. This is a copying constructor.
     *
     * @param range range to deep copy
     */
    public TextRange(TextRange range) {
        this(range.BEG, range.END);
    }
    /**
     * Encodes this text range; to decode, use a respective constructor.
     * 
     * @param topDirectory if not null, relative path to this directory is used
     * @return a textual form of this text range
     */
    public String encode(PWD topDirectory) {
        String name;
        if(topDirectory == null)
            name = BEG.name;
        else
            name = topDirectory.getRelativePath(BEG.name);
        return name + "#" + BEG.line + ":" + BEG.column + "#" +
                END.line + ":" + END.column;
    }
    /**
     * Finds a respective fragment in the source file.
     * 
     * @param topDirectory if not null, relative path to this directory is used
     * @return source code fragment specified by this range
     */
    public String extract(PWD topDirectory) throws IOException {
        File file;
        if(topDirectory == null)
            file = new File(BEG.name);
        else
            file = topDirectory.getFile(BEG.name);
        String source = CompilerUtils.readFile(file);
        LineIndex li = new LineIndex(source);
        return source.substring(
                li.getPos(BEG), li.getPos(END) + 1);
    }
    /**
     * Compares positions. The ranges must be in the same file and can not overlap.
     * 
     * @param other a range to compare to
     * @return order, encoded like <code>Comparable.compareTo()</code>
     */
    public int samePosition(TextRange other) {
        int name = BEG.name.compareTo(other.BEG.name);
        if(name != 0)
            throw new RuntimeException("can not compare: different file");
        int beg = BEG.compareTo(other.BEG);
        int end = END.compareTo(other.END);
        if(beg != end)
            throw new RuntimeException("can not compare: overlap");
        return beg;
    }
    @Override
    public String toString() {
        return BEG.name + "< " + BEG.line + ":" + BEG.column + " ... " +
                END.line + ":" + END.column + " >";
    }
}
