/*
 * SourcePosition.java
 *
 * Created on Feb 6, 2008.
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util.stream;

/**
 * An interface of objects with determined position in the source
 * text stream or indication that they do not exists in the source
 * stream.
 * 
 * @author Artur Rataj
 */
public interface SourcePosition {
    /**
     * Sets the position in the parsed stream or null if none.
     * 
     * @param pos                       position in text stream or null
     */
    public void setStreamPos(StreamPos pos);
    /**
     * Returns the position in the parsed stream or null if none.
     * 
     * @return                          position in text stream or null
     */
    public StreamPos getStreamPos();
}
