/*
 * PrismExec.java
 *
 * Created on Jun 10, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.io.*;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * Executes Prism, processes its output.
 * 
 * @author Artur Rataj
 */
public class PrismExec extends ExecProcess {
    /**
     * Results of a single model checking.
     */
    public static class Result {
        /**
         * Number of states. If not detected, -1.
         */
        public int stateNum = -1;
        /**
         * Number of transitions. If not detected, -1.
         */
        public int transitionNum = -1;
        /**
         * Model checking results. If not detected, an empty list.
         */
        public List<Double> value = new LinkedList<>();
        /**
         * A warning, empty for none. Includes any warning, also minor. A single
         * warning per line.
         */
        public String warning = "";
    }
    /**
     * If to fail on a major warning. If false, a possible warning is written
     * to the result.
     */
    private final boolean FAIL_ON_WARNING;
    /**
     * Last result. Null if Prism never ran or the last run was a failure.
     */
    protected Result lastResult = null;
    
    /**
     * Creates a new object.
     * 
     * @param failOnWarning if to fail on an important warning (regarding usually an
     * internal consistency of the model or an invalid property)
     */
    public PrismExec(boolean failOnWarning) {
        super(true);
        FAIL_ON_WARNING = failOnWarning;
    }
    @Override
    protected void processOut(BufferedReader br) throws CompilerException {
        lastResult = new Result();
        String line;
        try {
            while((line = br.readLine()) != null) {
                String l = line.toLowerCase();
                if(l.contains("error") ||
                        (l.contains("warning") && FAIL_ON_WARNING &&
                                !l.contains("warning: disabling") && !l.contains("warning during"))) {
                    lastResult = null;
                    throw new CompilerException(null, CompilerException.Code.IO,
                            "while running Prism: " + line);
                }
                if(l.contains("warning"))
                    lastResult.warning += l + "\n";
                int pos = l.indexOf(":");
                if(pos != -1) {
                    String value = l.substring(pos + 1).trim();
                    for(int i = 0; i < value.length(); ++i)
                        if(Character.isWhitespace(value.charAt(i))) {
                            value = value.substring(0, i);
                            break;
                        }
                    if(l.startsWith("states:")) {
                        if(lastResult.stateNum != -1)
                            throw new CompilerException(null, CompilerException.Code.IO,
                                    "non-unique state number declaration in Prism output");
                        lastResult.stateNum = Integer.parseInt(value);
                    } else if(l.startsWith("transitions:")) {
                        if(lastResult.transitionNum != -1)
                            throw new CompilerException(null, CompilerException.Code.IO,
                                    "non-unique transition number declaration in Prism output");
                        lastResult.transitionNum = Integer.parseInt(value);
                    } else if(l.startsWith("result:")) {
                        lastResult.value.add(Double.parseDouble(value));
                    }
                }
            }
        } catch(IOException e) {
            throw new CompilerException(null, CompilerException.Code.IO,
                    "while reading process' output: " + e.toString());
        }
    }
    /**
     * Returns the result of the latest <code>processOut()</code>.
     * 
     * @return model checking result, null if <code>processOut()</code> was
     * never run of the execution failed
     */
    public Result getResult() {
        return lastResult;
    }
}

