/*
 * WorkingDirectory.java
 *
 * Created on Feb 10, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.util;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
// import java.util.regex.Pattern;
// import org.apache.commons.io.FilenameUtils;

/**
 * A class that resolves file names as if it were a program
 * working directory.<br>
 *
 * Used by the compiler to specify the compilation top directory.
 *
 * @author Artur Rataj
 */
public class PWD {
    /**
     * The working directory.
     */
    String directory;

    /**
     * Default working directory.
     */
    public PWD() {
        this.directory = System.getProperty("user.dir");
    }
    /**
     * Creates a new instance of PWD.
     *
     * @param directory                 name of the working directory
     */
    public PWD(String directory) {
        this.directory = directory;
    }
    /**
     * Creates a new instance of PWD. This is a copying constructor.
     *
     * @param source                    PWD to copy
     */
    public PWD(PWD source) {
        this(source.directory);
    }
    /**
     * Appends a path to this path.
     *
     * @param s                         path, optionally preceeded with
     *                                  path separator char
     */
    public void appendPath(String s) {
        if(s.charAt(0) != File.separatorChar &&
                !(s.length() >= 2 && s.charAt(1) == ':'))
            s = File.separatorChar + s;
        directory = directory + s;
    }
    /**
     * Resolves the file name, and returns its <code>File</code> object.
     * 
     * @param fileName                  either a relative or an absolute
     *                                  file name
     * @return
     */
    public File getFile(String fileName) {
        if(directory == null || new File(fileName).isAbsolute())
            return new File(fileName);
        else
            return new File(Paths.get(directory, fileName).normalize().toString());
    }
    /**
     * Get the relative path from one file to another.
     * 
     * @param basePath base path
     * @param targetPath target path
     * @return relative path if possible, otherwise <code>targetPath</code>
     * 
     * // author Milan Aleksic, patched by Erhannis, Artur Rataj;
     * // <a href ="http://stackoverflow.com/questions/204784/how-to-construct-a-relative-path-in-java-from-two-absolute-paths-or-urls">source</a>
     */
    public static String getRelativePath(String basePath, String targetPath) {
        Path pathAbsolute = Paths.get(targetPath);
        Path pathBase = Paths.get(basePath);
        Path pathRelative;
        if(pathAbsolute.getFileName().toString().equals(targetPath))
            pathRelative = pathAbsolute;
        else
            try {
                pathRelative = pathBase.relativize(pathAbsolute);
            } catch(IllegalArgumentException e) {
                pathRelative = pathAbsolute;
            }
        String s = pathRelative.toString();
        if(s.isEmpty())
            s = ".";
        return s;
        /*
        String pathSeparator = File.separator;
        // Normalize the paths
        String normalizedTargetPath = FilenameUtils.normalizeNoEndSeparator(targetPath);
        String normalizedBasePath = FilenameUtils.normalizeNoEndSeparator(basePath);
        // Undo the changes to the separators made by normalization
        switch (pathSeparator) {
            case "/":
                normalizedTargetPath = FilenameUtils.separatorsToUnix(normalizedTargetPath);
                normalizedBasePath = FilenameUtils.separatorsToUnix(normalizedBasePath);
                break;
                
            case "\\":
                normalizedTargetPath = FilenameUtils.separatorsToWindows(normalizedTargetPath);
                normalizedBasePath = FilenameUtils.separatorsToWindows(normalizedBasePath);
                break;
                
            default:
                throw new IllegalArgumentException("Unrecognised dir separator '" + pathSeparator + "'");
        }
        String[] base = normalizedBasePath.split(Pattern.quote(pathSeparator));
        String[] target = normalizedTargetPath.split(Pattern.quote(pathSeparator));
        // First get all the common elements. Store them as a string,
        // and also count how many of them there are.
        StringBuilder common = new StringBuilder();
        int commonIndex = 0;
        while (commonIndex < target.length && commonIndex < base.length
                && target[commonIndex].equals(base[commonIndex])) {
            common.append(target[commonIndex] + pathSeparator);
            commonIndex++;
        }
        if (commonIndex == 0) {
            // No single common path element. This most
            // likely indicates differing drive letters, like C: and D:.
            // These paths cannot be relativized.
            throw new RuntimeException("no common path element found for '"
                    + normalizedTargetPath + "' and '" + normalizedBasePath + "'");
        }   
        // The number of directories we have to backtrack depends on whether the base is a file or a dir
        // For example, the relative path from
        //
        // /foo/bar/baz/gg/ff to /foo/bar/baz
        // 
        // ".." if ff is a file
        // "../.." if ff is a directory
        //
        // The following is a heuristic to figure out if the base refers to a file or dir. It's not perfect, because
        // the resource referred to by this path may not actually exist, but it's the best I can do
        boolean baseIsFile = true;
        File baseResource = new File(normalizedBasePath);
        if (baseResource.exists()) {
            baseIsFile = baseResource.isFile();
        } else if (basePath.endsWith(pathSeparator)) {
            baseIsFile = false;
        }
        StringBuilder relative = new StringBuilder();
        if (base.length != commonIndex) {
            int numDirsUp = baseIsFile ? base.length - commonIndex - 1 : base.length - commonIndex;
            for (int i = 0; i < numDirsUp; i++) {
                relative.append(".." + pathSeparator);
            }
        }
        if (common.length() >= normalizedTargetPath.length())
            return ".";
        else {
            relative.append(normalizedTargetPath.substring(common.length()));
            return relative.toString();
        }
        */
    }
    /**
     * Get the relative path, using this directory as a base.
     * 
     * @param targetPath target path
     * @return relative path, if possible
     */
    public String getRelativePath(String targetPath) {
        String s = directory;
        if(s == null)
            s = System.getProperty("user.dir");
        return getRelativePath(s, targetPath);
    }
    @Override
    public String toString() {
        if(directory == null)
            return "";
        else
            return directory;
    }
}
