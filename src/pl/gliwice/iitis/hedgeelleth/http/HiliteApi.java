/*
 * HiliteApi.java
 *
 * Created on Jul 4, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.http;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;

/**
 * Uses the <code>Hilite.me</code> api to convert Java surces to HTML.
 * 
 * @author Artur Rataj
 */
public class HiliteApi {
    protected static URL HILITE_URL;
    static {
        try {
            HILITE_URL = new URL("http://hilite.me/api");
        } catch (MalformedURLException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Converts a Java code to a highghted HTML div.
     * 
     * @param style style, null for <code>colorful</code>
     * @param lineNumbers if to print line numbers
     * @param code code to convert
     * @return HTML code
     */
    public static String convert(String style, boolean lineNumbers, String code) throws IOException {
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("code", code);
        params.put("lexer", "java");
        params.put("style", style != null ? style : "friendly");
        if(lineNumbers)
            params.put("linenos", "true");
        params.put("divstyles", "border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;");
        return Http.post(HILITE_URL, params);
    }
    public static void main(String[] args) throws Exception {
        String[] args_ = { "/home/art/projects/svn/archive/doc/articles/hc-taai" };
        args = args_;
        List<String> files = CompilerUtils.getAllFiles(new File(args[0]), Pattern.compile(".*\\.java"));
        for(String fileName : files) {
            String response = convert(null, true,
                    CompilerUtils.readFile(new File(fileName)));
            CompilerUtils.save(fileName + ".html.inc", response);
        }
    }
}
