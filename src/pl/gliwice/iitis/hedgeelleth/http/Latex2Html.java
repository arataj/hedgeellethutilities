/*
 * Latex2Html.java
 *
 * Created on Jul 6, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.http;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;

/**
 * Basic conversion of some Latex text formatters into a HTML equivalent.
 * 
 * @author Artur Rataj
 */
public class Latex2Html {
    static enum Mode {
        PLAIN,
        TOKEN,
    }
    static enum Modifier {
        NONE,
        MATH,
        EMPH,
        SUPERSCRIPT,
        CODE,
        VERBATIM,
        BEGIN,
        END,
    }
    public static String convert(String in) throws IOException {
        StringBuilder out = new StringBuilder();
        int lineNum = 0;
        int colNum = 0;
        Mode mode = Mode.PLAIN;
        Stack<Modifier> mod = new Stack<>();
        mod.push(Modifier.NONE);
        boolean mathItalic = false;
        char verbatimEnding = '\0';
        boolean bracketed = false;
        String token = "";
        try {
            for(int p = 0; p < in.length(); ++p) {
                char c = in.charAt(p);
if(c == '^')
    c = c;
                String s = null;
                if(bracketed) {
                    if(mod.peek() != Modifier.VERBATIM && c != '{')
                        throw new IOException("'{' expected");
                    else if(mod.peek() == Modifier.VERBATIM)
                        verbatimEnding = c;
                    bracketed = false;
                    switch(mod.peek()) {
                        case MATH:
                            break;
                            
                        case EMPH:
                            s = "<emph>";
                            break;
                            
                        case CODE:
                            s = "<code>";
                            break;
                            
                        case SUPERSCRIPT:
                            s = "<sup>";
                            break;
                            
                        case VERBATIM:
                            s = "<code>";
                            break;
                            
                        default:
                            throw new RuntimeException("unexpected: mod = " + mod.peek());
                    }
                    if(s == null)
                        continue;
                }
                if(s == null) {
                    boolean endVerbatim = mod.peek() == Modifier.VERBATIM && c == verbatimEnding;
                    if(mod.peek() != Modifier.VERBATIM && c == '{') {
                        // a bracket not paired with a modifier
                        token = "";
                        mode = Mode.PLAIN;
                        mod.push(Modifier.NONE);
                    } else if((mod.peek() != Modifier.VERBATIM && c == '}') || endVerbatim) {
                        if(endVerbatim)
                            verbatimEnding = '\0';
                        if(mod.isEmpty())
                            throw new IOException("not paired '}'");
                        switch(mod.pop()) {
                            case MATH:
                                break;

                            case EMPH:
                                s = "</emph>";
                                break;

                            case CODE:
                                s = "</code>";
                                break;

                            case SUPERSCRIPT:
                                s = "</sup>";
                                break;

                            case VERBATIM:
                                s = "</code>";
                                break;

                            default:
                                throw new RuntimeException("unexpected: mod = " + mod.peek());
                        }
                        if(s == null)
                            continue;
                    } else if(mod.peek() != Modifier.VERBATIM && c == '^')
                        // this one is not prepended by the slash
                        mode = Mode.TOKEN;
                }
                if(mode == Mode.TOKEN) {
                    if(!Character.isWhitespace(c) && c != '~') {
                        token = token + c;
                        switch(token) {
                            case "(":
                                mod.push(Modifier.MATH);
                                mode = Mode.PLAIN;
                                break;

                            case ")":
                                if(mod.pop() != Modifier.MATH)
                                    throw new IOException("expected end of math mode");
                                if(mathItalic)
                                    s = "</i>";
                                mode = Mode.PLAIN;
                                break;

                            case "emph":
                                mod.push(Modifier.EMPH);
                                bracketed = true;
                                mode = Mode.PLAIN;
                                break;

                            case "code":
                                mod.push(Modifier.CODE);
                                bracketed = true;
                                mode = Mode.PLAIN;
                                break;

                            case "^":
                                mod.push(Modifier.SUPERSCRIPT);
                                bracketed = true;
                                mode = Mode.PLAIN;
                                break;
                                
                            case "verb":
                                mod.push(Modifier.VERBATIM);
                                bracketed = true;
                                mode = Mode.PLAIN;
                                break;
                                
                            case "begin":
                                mod.push(Modifier.BEGIN);
                                bracketed = true;
                                mode = Mode.PLAIN;
                                break;
                                
                            case "end":
                                mod.push(Modifier.END);
                                bracketed = true;
                                mode = Mode.PLAIN;
                                break;
                                
                            case "cdot":
                                s = "&middot;";
                                mode = Mode.PLAIN;
                                break;

                            case ",":
                                if(!mod.contains(Modifier.MATH))
                                    throw new IOException("math space not in math mode");
                                s = " ";
                                mode = Mode.PLAIN;
                                break;
                                
                         

                            default:
                                // continue reading the token
                                break;
                        }
                        if(mode == Mode.PLAIN) {
                            // token eaten
                            token = "";
//                            if(s != null)
//                                s = s;
                        }
                    } else {
                        mode = Mode.PLAIN;
                        token = "";
                    }
                }
                if(s != null) {
                    out.append(s);
                    s = null;
                } else if(mod.peek() != Modifier.VERBATIM && c == '\\')
                    mode = Mode.TOKEN;
                else if(mode == Mode.PLAIN) {
                    if(mod.contains(Modifier.MATH)) {
                        boolean newMathItalic;
                        if(Character.isDigit(c))
                            newMathItalic = false;
                        else if(Character.isLetter(c))
                            newMathItalic = true;
                        else
                            newMathItalic = false;
                        if(mathItalic && !newMathItalic)
                            s = "</i>";
                        else if(!mathItalic && newMathItalic)
                            s = "<i>";
                        mathItalic = newMathItalic;
                    }
                    if(s != null) {
                        out.append(s);
                        s = null;
                    }
                    out.append(c);
                }
                if(c == '\n') {
                    colNum = 0;
                    ++lineNum;
                } else
                    ++colNum;
            }
        } catch(IOException e) {
            throw new IOException((lineNum + 1) + ":" + (colNum + 1) + ": " + e.getMessage());
        }
        return out.toString();
    }
    public static void main(String[] args) throws IOException {
        String[] args_ = {
            "/home/art/projects/ngreen/HedgeellethCompiler/doc/html-src/0.Home/5.Tutorial/6.High level/3.Clocks.txt",
            "/home/art/projects/ngreen/HedgeellethCompiler/doc/html-src/0.Home/5.Tutorial/6.High level/3.Clocks.txt.html",
        };
        args = args_;
        String out = convert(CompilerUtils.readFile(new File(args[0])));
        CompilerUtils.save(args[1], out);
    }
}
